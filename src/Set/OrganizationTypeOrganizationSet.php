<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * @property ORM_PkField                $id
 * @property OrganizationTypeSet        $organizationType
 * @property OrganizationSet            $organization
 *
 * @method OrganizationTypeOrganization[]   select()
 * @method OrganizationTypeOrganization     get()
 * @method OrganizationTypeOrganization     request()
 * @method OrganizationTypeOrganization     newRecord()
 * @method Func_App                         App()
 */
class OrganizationTypeOrganizationSet extends \app_RecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'OrganizationTypeOrganization');
        
        $this->setDescription('Organization type organization');
        
        $this->setPrimaryKey('id');

        $this->hasOne('organizationType', $App->OrganizationTypeSetClassName());
        $this->hasOne('organization', $App->OrganizationSetClassName());
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new OrganizationTypeOrganizationBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new OrganizationTypeOrganizationAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
}

class OrganizationTypeOrganizationBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class OrganizationTypeOrganizationAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}