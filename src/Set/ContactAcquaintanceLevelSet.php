<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * @property \ORM_StringField   $name
 * @property \ORM_IntField      $rank
 * @property \ORM_StringField   $color
 */
class ContactAcquaintanceLevelSet extends \app_RecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ContactAcquaintanceLevel');
        
        $this->setDescription('Contact acquaintance level');
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('name')->setDescription($App->translate('Name')),
            ORM_IntField('rank')->setDescription($App->translate('Rank')),
            ORM_StringField('color')->setDescription($App->translate('color'))
        );
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ContactAcquaintanceLevelBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ContactAcquaintanceLevelAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class ContactAcquaintanceLevelBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ContactAcquaintanceLevelAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}