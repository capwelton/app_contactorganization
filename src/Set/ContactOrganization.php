<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * A ContactOrganization is a contact in an organization
 * one contact can have multiple organizations
 *
 *
 * @property int                        $id
 * @property string                     $position
 * @property int                        $contactRank
 * @property int                        $organizationRank
 * @property string                     $start
 * @property string                     $end
 * @property Contact                    $contact
 * @property Organization               $organization
 * @property ContactOrganizationType    $type
 *
 * @method ContactOrganizationSet       getParentSet()
 * @method Func_App                     App()
 */
class ContactOrganization extends \app_TraceableRecord
{
    /**
     * {@inheritDoc}
     * @see \ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->contact()->getRecordTitle() . ', ' . $this->position . ' - ' . $this->organization()->getRecordTitle();
    }

    /**
     *
     * {@inheritDoc}
     * @see \app_Record::delete()
     */
    public function delete()
    {
        $contactId = $this->getScalarValue('contact');
        $result = parent::delete();

        $App = $this->App();

        $contactSet = $App->contactSet();
        $contact = $contactSet->get($contactId);
        if ($contact->resetMainPosition()) {
            $contact->save();
        }

        return $result;
    }

    /**
     *
     * {@inheritDoc}
     * @see \ORM_Record::save()
     */
    public function save()
    {
        $App = $this->App();

        $contactOrganizationSet = $App->ContactOrganizationSet();
        if ($this->contactRank == 0) {
            $records = $contactOrganizationSet->select(
                $contactOrganizationSet->contact->is($this->contact)
            );
            $records->orderDesc($contactOrganizationSet->contactRank);

            foreach ($records as $record) {
                $this->contactRank = $record->contactRank + 1;
                break;
            }
        }
        if ($this->organizationRank == 0) {
            $records = $contactOrganizationSet->select(
                $contactOrganizationSet->organization->is($this->organization)
            );
            $records->orderDesc($contactOrganizationSet->organizationRank);

            foreach ($records as $record) {
                $this->organizationRank = $record->organizationRank + 1;
                break;
            }
        }

        $result = parent::save();

        $contactSet = $App->contactSet();
        $contact = $contactSet->get($this->getScalarValue('contact'));
        if (isset($contact) && $contact->resetMainPosition()) {
            $contact->save();
        }

        return $result;
    }
    
    /**
     * Position in organization
     * @return string
     */
    public function getMainPosition() {
        
        return $this->position;
    }
    
    /**
     * @return Organization
     */
    public function getMainOrganization()
    {
        $this->App()->includeOrganizationSet();
        return $this->organization();
    }
    
    /**
     * @return Contact
     */
    public function getContact()
    {
        $this->App()->includeContactSet();
        return $this->contact();
    }
    
    
    /**
     * @return ContactPhone[]
     */
    public function getPhoneRecords()
    {
        $App = $this->App();
        $contactPhoneSet = $App->ContactPhoneSet();
        
        $contactPhones = $contactPhoneSet->select(
            $contactPhoneSet->contactOrganization->is($this->id)
        );
        
        $contactPhones->orderAsc($contactPhoneSet->rank);
        $contactPhones->orderAsc($contactPhoneSet->id);
        
        return $contactPhones;
    }
    
    
    /**
     * @return ContactPhone|NULL
     */
    public function getMainPhoneRecord()
    {
        $contactPhones = $this->getPhoneRecords();
        
        foreach ($contactPhones as $contactPhone) {
            return $contactPhone;
        }
        
        return null;
    }
    
    public function getMainPhone()
    {
        $mainPhoneRecord = $this->getMainPhoneRecord();
        if (isset($mainPhoneRecord)) {
            return $mainPhoneRecord->phone;
        }
        
        return null;
    }
}
