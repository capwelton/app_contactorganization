<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * @property    ORM_PkField     $id
 * 
 * @method LegalStatus     get()
 * @method LegalStatus     newRecord()
 * @method LegalStatus[]   select()
 */
class LegalStatusSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'LegalStatus');
        
        $this->setDescription('Legal status');
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('name')->setDescription($App->translate('Name')),
            ORM_StringField('code')->setDescription($App->translate('Code'))
        );
    }
    
    public function onUpdate()
    {
        $this->instanciateDefaultStatuses();
    }
    
    protected function instanciateDefaultStatuses()
    {
        $App = $this->App();
        $set = $App->LegalStatusSet();
        if ($set->select()->count() == 0) {
            $defaultLegalStatuses = array(
                'SNC' => $App->translate('SNC'),
                'SCI' => $App->translate('SCI'),
                'SA' => $App->translate('SA'),
                'SAS' => $App->translate('S.A.S.'),
                'SARL' => $App->translate('SARL'),
                'ASSO' => $App->translate('Association'),
                'SEM' => $App->translate('SEM')
            );
            
            foreach ($defaultLegalStatuses as $code => $name){
                $record = $set->newRecord();
                $record->code = $code;
                $record->name = $name;
                $record->save();
            }
            
            $message = "<span style='color:green;'>{$App->translate('Default legal statuses instanciated')}</span>";
            \bab_installWindow::message($message);
        }
    }
    
    public static $legalStatuses =	array(
        'SNC'   => 'SNC',
        'SCI'   => 'SCI',
        'SA'    => 'SA',
        'SAS'   => 'S.A.S.',
        'SARL'  => 'SARL',
        'ASSO'  => 'Association',
        'SEM'   => 'SEM'
    );
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new LegalStatusBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new LegalStatusAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function isReadable(){
        return $this->all();
        
    }
    
    public function isDeletable(){
        return $this->all();
    }
    
    public function isCreatable()
    {
        return $this->all();
    }
    
    public function isUpdatable()
    {
        return $this->all();
    }
}

class LegalStatusBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class LegalStatusAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}