<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * @method \Func_App             App()
 * @method OrganizationTypeSet  getParentSet()
 */
class OrganizationType extends \app_Record
{
    CONST PROFILE_PURCHASE = 1;
    CONST PROFILE_SALE = 2;
    CONST PROFILE_OTHER = 3;
    CONST PROFILE_INTERNAL = 4;
    
    public static function getProfiles(\Func_App $App)
    {
        $component = $App->getComponentByName('ContactOrganization');
        return array(
            self::PROFILE_PURCHASE => $component->translate('Purchase'),
            self::PROFILE_SALE => $component->translate('Sale'),
            self::PROFILE_OTHER => $component->translate('Other'),
            self::PROFILE_INTERNAL => $component->translate('Internal')
        );
    }
    
    /**
     * @return bool
     */
    public function isReadable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * @return bool
     */
    public function isUpdatable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
   
    /**
     * @return string
     */
    public function getProfileName()
    {
        $App = $this->App();
        $rc = new \ReflectionClass($this);
        
        $shortName = $rc->getShortName();
        $includeClassName = 'include'.$shortName.'Set';
        if(method_exists($App, $includeClassName)){
            //Tries to get the profile from the $App if this class has been overriden
            $shortClassName = $shortName.'ClassName';
            $App->$includeClassName();
            $name = $App->classPrefix.$shortName;
            $profiles = $name::getProfiles($App);
        }
        else{
            $profiles = self::getProfiles($App);
        }
        return isset($profiles[$this->profile]) ? $profiles[$this->profile] : '';
    }
}
