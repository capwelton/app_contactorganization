<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

use Capwelton\App\Address\Set;
use Capwelton\App\Address\Set\Address;

/**
 * Organization object
 *
 * @property string                     $name
 * @property string                     $description
 * @property string                     $activity
 * @property string                     $email
 * @property string                     $phone
 * @property string                     $fax
 * @property string                     $website
 *
 * @property Organization       $parent
 * @property OrganizationType   $type
 * @property Address            $address
 *
 * @property Organization       $responsibleOrganization
 * @property Contact            $responsible
 * 
 * @property Contact            $commercialReferralContact
 *
 * @method OrganizationSet  getParentSet()
 * @method Address          address()
 * @method \Func_App         App()
 */
class Organization extends \app_TraceableRecord
{

    const SUBFOLDER = 'organizations';
    const PHOTOSUBFOLDER = 'logo';
    const ATTACHMENTSSUBFOLDER = 'attachments';

    const TYPE_CUSTOMER = 'customer';
    const TYPE_SUPPLIER = 'supplier';
    const TYPE_INSTUTIONNAL_PARTNER = 'institutionnal partner';
    
    /**
     * Types of orders
     * @return array
     */
    public static function getTypes()
    {
        return array(
            self::TYPE_CUSTOMER                 => "Customer",
            self::TYPE_SUPPLIER                 => "Supplier",
            self::TYPE_INSTUTIONNAL_PARTNER     => "Institutionnal partner"
        );
    }
    
    public static $legalStatuses =	array(
        'SNC'   => 'SNC',
        'SCI'   => 'SCI',
        'SA'    => 'SA',
        'SAS'   => 'S.A.S.',
        'SARL'  => 'SARL',
        'ASSO'  => 'Association',
        'SEM'   => 'SEM'
    );

    public function getAllChildrenOrganization(){
        $childs = $this->getFirstChildOrganization();
        $value = array();
        foreach($childs as $c){
            $value[$c->id]["name"] = $c->name;
            $value[$c->id]["id"] = $c->id;
            $value[$c->id]["children"] = $c->getAllChildrenOrganization();
        }
        return $value;
    }

    public function getFirstChildOrganization(){
        $App = $this->App();
        $set = $App->OrganizationSet();
        $orgs = $set->select(
            $set->parent->is($this->id)
        );
        return $orgs;
    }

    public function getFullName(){
        return $this->getName();
    }
    /**
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get main website http URL or null if no website
     *
     * @return string | null
     */
    public function getMainWebsite()
    {
        return $this->website;
    }

    /**
     * Get main organization email or null if no email
     *
     * @return string | null
     */
    public function getMainEmail()
    {
        return $this->email;
    }

    /**
     * Get main organization phone number or null if no phone
     *
     * @return string | null
     */
    public function getMainPhone()
    {
        return $this->phone;
    }

    /**
     * Get main organization mobile phone number or null if no phone
     *
     * @return string | null
     */
    public function getMainMobile()
    {
        return null;
    }

    /**
     * Get main organization fax number or null if no fax
     *
     * @return string | null
     */
    public function getMainFax()
    {
        return $this->fax;
    }

    /**
     * Get additional information for organization displayed in a widget organization suggest as a second line after informations from the getName() method
     *
     * @see Organization::getName()
     * @return string
     */
    public function getSuggestInfos()
    {
        if (! $this->address || $this->address->isEmpty()) {
            return null;
        }

        return $this->address->postalCode . ' ' . $this->address->city;
    }

    public function getChildren()
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        return $children;
    }

    public function getListOfChildren(){
        $childs = $this->getChildren();
        $return = array($this->id);
        foreach($childs as $child){
            $return = array_merge($return, $child->getListOfChildren());
        }
        return $return;
    }

    /**
     * Get the upload path for files related to this organization.
     *
     * @return \bab_Path
     */
    public function uploadPath()
    {
        if (! isset($this->id)) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'] . 'utilit/path.class.php';

        $path = $this->App()->getUploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }


    /**
     * Get the upload path for attached files.
     *
     * @return \bab_Path
     */
    public function getAttachmentsUploadPath()
    {
        if (! isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::ATTACHMENTSSUBFOLDER);
        return $path;
    }

    /**
     * Get the upload path for the logo file
     * this method can be used with the image picker widget
     *
     * @return \bab_Path
     */
    public function getLogoUploadPath()
    {
        $path = $this->uploadPath();
        if (! isset($path)) {
            return null;
        }

        $path->push(self::PHOTOSUBFOLDER);
        return $path;
    }

    /**
     * Return the full path of the photo file
     * this method return null if there is no logo to display
     *
     * @return \bab_Path | null
     */
    public function getLogoPath()
    {
        $uploadpath = $this->getLogoUploadPath();

        if (! isset($uploadpath)) {
            return null;
        }

        $W = bab_Widgets();
        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

        if (! isset($uploaded)) {
            return null;
        }

        foreach ($uploaded as $file) {
            return $file->getFilePath();
        }

        return null;
    }

    /**
     * Move logo to logo upload path
     * this method is used to import an image from a temporary directory of the filePicker widget or another file
     * warning, the default behavior remove the source file
     *
     * @param \bab_Path $sourcefile
     * @param bool $temporary
     *            default is true, if the sourcefile is a filePicker temporary file
     * @return bool
     */
    public function importLogo(\bab_Path $sourcefile, $temporary = true)
    {
        $uploadpath = $this->getLogoUploadPath();

        if (!isset($uploadpath)) {
            return false;
        }

        $uploadpath->createDir();

        if (! $temporary) {
            $W = bab_Widgets();
            return $W->imagePicker()
                ->setFolder($uploadpath)
                ->importFile($sourcefile);
        }

        $original = $sourcefile->toString();
        $uploadpath->push(basename($original));

        return rename($original, $uploadpath->toString());
    }

    /**
     * Method used to get user info for the bab_updateUserById function
     * only the organization part
     *
     * @see Contact::getUserInfos()
     *
     *
     * @return array
     */
    public function getUserOrganizationInfos()
    {
        $output = array();

        $output['btel'] = $this->getMainPhone();
        $output['bfax'] = $this->getMainFax();
        $output['organisationname'] = $this->getName();

        if ($orgaddress = $this->address()) {
            /* @var $orgaddress Address */

            $output['bstreetaddress'] = $orgaddress->street;
            $output['bcity'] = $orgaddress->city;

            if ($orgaddress->cityComplement) {
                $output['bcity'] .= ' ' . $orgaddress->cityComplement;
            }

            $output['bpostalcode'] = $orgaddress->postalCode;
            $output['bstate'] = $orgaddress->state;

            if ($orgcountry = $orgaddress->getCountry()) {
                /* @var $orgcountry Country */
                $output['bcountry'] = $orgcountry->getName();
            } 
            else {
                $output['bcountry'] = '';
            }
        } else {
            $output['bstreetaddress'] = '';
            $output['bcity'] = '';
            $output['bpostalcode'] = '';
            $output['bstate'] = '';
            $output['bcountry'] = '';
        }

        return $output;
    }

    /**
     * Checks whether the organization is a descendant of the specified organization.
     *
     * @param int|Organization $organization
     *
     * @return bool
     */
    public function isDescendantOf($organization)
    {
        $organizationId = (($organization instanceof Organization) ? $organization->id : $organization);

        $org = $this;
        while ($org->parent != 0) {
            if ($org->parent == $organizationId) {
                return true;
            }
            $org = $org->parent();
        }
        return false;
    }

    /**
     *
     * @return array of org id => org.
     */
    public function getAscendants()
    {
        $ancestors = array();
        $org = $this;
        while ($org) {
            if (isset($ancestors[$org->id])) {
                break;
            }
            $ancestors[$org->id] = $org;
            $org = $org->parent();
        }

        return $ancestors;
    }

    /**
     *
     * @return bool
     */
    public function hasDescendants()
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        $hasDescendants = ($children->count() > 0);
        return $hasDescendants;
    }

    /**
     *
     * @param array $descendants
     *            Contains the result of descendants ids.
     */
    public function getDescendants(&$descendants)
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        foreach ($children as $child) {
            if ($child->id != $this->id && ! isset($descendants[$child->id])) {
                $descendants[$child->id] = $child->id;
                $child->getDescendants($descendants);
            }
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see \app_TraceableRecord::linkTo($source, $linkType)
     */
    public function linkTo(\app_Record $source, $linkType = 'hasOrganization')
    {
        return parent::linkTo($source, $linkType);
    }

    /**
     * Reassociates all data associated to this organization to another
     * specified one.
     *
     * @param int $id   The id of the organization that will replace this one.
     *
     * @return Organization
     */
    public function replaceWith($id)
    {
        parent::replaceWith($id);

        $App = $this->App();
        
        $contactOrganizationSet = $App->ContactOrganizationSet();

        $contactOrganizations = $contactOrganizationSet->select($contactOrganizationSet->organization->is($this->id));
        foreach ($contactOrganizations as $contactOrganization) {
            $contactOrganization->organization = $id;
            $contactOrganization->save();
        }
        
        // Keep track of the replacement.
        $this->replacedBy = $id;

        $this->save();

        return $this;
    }
    
    /**
     * Returns the real paying organization.
     * @return Organization
     */
    public function getPayingOrganization()
    {
        $payingOrganization = $this->payingOrganization();
        if ($payingOrganization && $payingOrganization->id != 0) {
            return $this->payingOrganization();
        }
        return $this;
    }

    /**
     * Returns the real responsible contact.
     * @return Contact
     */
    public function getResponsibleContact()
    {
        $this->App()->includeContactSet();
        $responsibleContact = $this->responsible();
        return $responsibleContact;
    }
    
    /**
     * Returns the real commercial referral contact.
     * @return Contact
     */
    public function getCommercialReferralContact()
    {
        $this->App()->includeContactSet();
        $commercialReferralContact = $this->commercialReferralContact();
        return $commercialReferralContact;
    }
    /**
     * @return \Widget_Color
     */
    public function getColor()
    {
        $W = bab_Widgets();
        $color = $W->Color();
        $color->setHueFromString($this->name, 0.8, 0.71);

        return $color;
    }


    /**
     * Main organization address
     * @return Address
     */
    public function getMainAddress()
    {
        return $this->address();
    }

    public function updateMainAddress()
    {
        $App = $this->App();
        $addressSet = $App->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this);

        if ($this->address instanceof Address) {
            $thisAddressId = $this->address->id;
        } else {
            $thisAddressId = $this->address;
        }

        $firstAddressId = null;
        $firstAddressRecord = null;
        $thisAddressOk = false;
        foreach ($addressLinks as $addressLink) {
            if ($addressLink->targetId->id == $thisAddressId) {
                // We found the current main address in the linked addresses.
                $thisAddressOk = true;
                break;
            }
            if (!isset($firstAddressId)) {
                $firstAddressId = $addressLink->targetId->id;
                $firstAddressRecord = $addressLink->targetId;
            }
        }

        if (!$thisAddressOk) {
            // If the current main address is not part of the linked addresses,
            // we replace it with the first linked address.
            if ($this->address instanceof Address) {
                $this->address = $firstAddressRecord;
            } else {
                $this->address = $firstAddressId;
            }
            $this->save();
        }
    }

    /**
     * Adds a typed address to the contact.
     *
     * @param Address $address
     * @param string $type
     */
    public function addAddress(Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }
        if (!$address->isLinkedTo($this, 'hasAddressType:' . $type)) {
            $address->linkTo($this, 'hasAddressType:' . $type);
            $this->updateMainAddress();
        }

        return $this;
    }

    /**
     * Removes a typed address from the organization.
     *
     * @param Address $address
     * @param string $type
     *
     * @return Organization
     */
    public function removeAddress(Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }
        $address->unlinkFrom($this, 'hasAddressType:' . $type);

        $this->updateMainAddress();

        return $this;
    }

    /**
     * Update the type of a linked address for the organization.
     *
     * @param Address $address
     * @param string $previousAddressType
     * @param string $newAddressType
     *
     * @return Organization
     */
    public function updateAddress(Address $address, $previousAddressType, $newAddressType)
    {
        if (!isset($this->id)) {
            return;
        }

        $App = $this->App();
        $addressSet = $App->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this, 'hasAddressType:' . $previousAddressType);
        foreach ($addressLinks as $addressLink) {
            $addressLink->type = 'hasAddressType:' . $newAddressType;
            $addressLink->save();
        }

        return $this;
    }

    /**
     *
     * @param string $type
     *
     * @return array  array('Type 1' => array(address1), 'Type 2' => array(address2, address3 ), ...)
     */
    public function getAddresses($type = null)
    {
        $App = $this->App();

        $addressSet = $App->AddressSet();

        if (isset($type)) {
            $type = 'hasAddressType:' . $type;
        }

        $addresses = $addressSet->selectLinkedTo($this, $type);

        $typedAddresses = array();

        foreach ($addresses as $address) {
            $type = $address->type;
            if (substr($type, 0, strlen('hasAddressType:')) === 'hasAddressType:') {
                $type = substr($type, strlen('hasAddressType:'));
            }
            if (!isset($typedAddresses[$type])) {
                $typedAddresses['' . $type] = array();
            }
            $typedAddresses['' . $type][] = $address->targetId;
        }

        return $typedAddresses;
    }

    public function getFirstDegreeRelatedContactIds(){
        $App = $this->App();
        $contactOrg = $App->ContactOrganizationSet();
        $contactsOrganizations = $contactOrg->select(
            $contactOrg->organization->is($this->id)
        );
        $contactId = array();
        foreach($contactsOrganizations as $CO){
            $contactId[] = $CO->contact;
        }
        return $contactId;
    }

    public function getOpennedFactures(){
        return array();
    }

    public function get3MFactures(){
        return array();
    }

    /**
     * @return Deal[]|[]
     */
    public function getDeals()
    {
        $deals = array();
        $dealOrderCmp = $this->App()->getComponentByName('DealOrder');
        if($dealOrderCmp){
            $dealSet = $this->App()->DealSet();
            $deals = $dealSet->select($dealSet->customer->is($this->id));
        }
        return $deals;
    }

    /**
     * Update properties with CSV row
     *
     * @return 	int
     */
    public function import(\Widget_CsvRow $row)
    {
        $App = $this->App();
        $up_prop = 0;

        static $contactSet = null;
        if (!isset($contactSet)) {
            $contactSet = $App->ContactSet();
        }
        static $organizationSet = null;
        if (!isset($organizationSet)) {
            $organizationSet = $App->OrganizationSet();
        }
        static $addressSet = null;
        if (!isset($addressSet)) {
            $addressSet = $App->AddressSet();
        }

        if ($this->code !== $row->code) {
            $up_prop++;
            $this->code = $organizationSet->code->input($row->code);
        }
        if ($this->initials !== $row->initials) {
            $up_prop++;
            $this->initials = $organizationSet->initials->input($row->initials);
        }
        if ($this->name !== $row->name) {
            $up_prop++;
            $this->name = $organizationSet->name->input($row->name);
        }
        if ($this->name2 !== $row->name2) {
            $up_prop++;
            $this->name2 = $organizationSet->name2->input($row->name2);
        }
        if ($this->description !== $row->description) {
            $up_prop++;
            $this->description = $organizationSet->description->input($row->description);
        }

        if ($this->types !== $row->types) {
            switch ($row->types){
                case 'C' :
                    $this->types = self::TYPE_CUSTOMER;
                    break;
                case 'F' :
                    $this->types = self::TYPE_SUPPLIER;
                    break;
            }
        }

        if ($this->legalStatus !== $row->legalStatus) {
            switch ($row->legalStatus){
                case 'C' :
                    $this->legalStatus = self::TYPE_CUSTOMER;
                    break;
                case 'F' :
                    $this->legalStatus = self::TYPE_SUPPLIER;
                    break;
            }
        }
        if ($this->accountNumber !== $row->accountNumber) {
            $up_prop++;
            $this->accountNumber = $row->accountNumber;
        }
        if ($this->email !== $row->email) {
            $up_prop++;
            $this->email = $row->email;
        }
        if ($this->phone !== $row->phone) {
            $up_prop++;
            $this->phone = $row->phone;
        }
        if ($this->phone2 !== $row->phone2) {
            $up_prop++;
            $this->phone2 = $row->phone2;
        }
        if ($this->fax !== $row->fax) {
            $up_prop++;
            $this->fax = $row->fax;
        }
        if ($this->website !== $row->website) {
            $up_prop++;
            $this->website = $row->website;
        }
        if ($this->siret !== $row->siret) {
            $up_prop++;
            $this->siret = $row->siret;
        }
        if ($this->siren !== $row->siren) {
            $up_prop++;
            $this->siren = $row->siren;
        }
        if ($this->rcs !== $row->rcs) {
            $up_prop++;
            $this->rcs = $row->rcs;
        }
        if ($this->ape !== $row->ape) {
            $up_prop++;
            $this->ape = $row->ape;
        }
        if ($this->intracommunityVat !== $row->intracommunityVat) {
            $up_prop++;
            $this->intracommunityVat = $row->intracommunityVat;
        }
        if ($this->iban !== $row->iban) {
            $up_prop++;
            $this->iban = $row->iban;
        }
        if ($this->ESS !== $row->ESS) {
            $up_prop++;
            $this->ESS = $row->ESS;
        }
        if ($this->account !== $row->account) {
            $up_prop++;
            $this->account = $row->account;
        }
        if ($this->paymentCode !== $row->paymentCode) {
            $up_prop++;
            $this->paymentCode = $row->paymentCode;
        }
        if ($this->accountNature !== $row->accountNature) {
            $up_prop++;
            $this->accountNature = $row->accountNature;
        }
        return $up_prop;
    }


    /**
     * Update properties of linked elements with CSV row
     *
     * @return 	int
     */
    public function importLinked(\Widget_CsvRow $row)
    {
        $App = $this->App();
        $organizationSet = $App->OrganizationSet();
        $up_prop = 0;

        if ($row->parent) {
            $parent = $organizationSet->get($organizationSet->code->like(trim($row->parent)));
            if ($parent) {
                $this->parent = $parent->id;
            }
            $up_prop++;
        }
        if ($row->payeur) {
            $parent = $organizationSet->get($organizationSet->code->like(trim($row->payeur)));
            if ($parent) {
                $this->payingOrganization = $parent->id;
            }
            $up_prop++;
        }

        if ($row->address) {
            $addressSet = $App->AddressSet();
            $address = $addressSet->newRecord();
            $address->street = $addressSet->street->input($row->address->street);
            $address->postalCode = $addressSet->postalCode->input($row->address->postalCode);
            $address->city = $addressSet->city->input($row->address->city);
            $address->state = $addressSet->state->input($row->address->state);
            $address->cityComplement = $addressSet->cityComplement->input($row->address->cityComplement);
            $address->country = $addressSet->country->input($row->address->country);
            $address->save();
            $this->addAddress($address, '');
            if($this->address instanceof Address){
                $this->address = $address;
            }
            else{
                $this->address = $address->id;
            }
            $this->save();
        }

        return $up_prop;
    }


    /**
     *
     * @return number
     */
    public function getPaymentBalance()
    {
        $dealOrderCmp = $this->App()->getComponentByName('DealOrder');
        if($dealOrderCmp){
            $App = $this->App();
            $orderSet = $App->OrderSet();
            $orderSet->deal();
            $orders = $orderSet->select(
                $orderSet->all(
                    $orderSet->type->is(Order::TYPE_INVOICE),
                    $orderSet->deal->customer->is($this->id)
                )
            );
            $paymentBalance = 0;
            foreach ($orders as $order) {
                $paymentBalance += $order->paymentBalance;
            }
            return $paymentBalance;
        }
        return 0;
    }


    /**
     * Returns the last commercial document date.
     *
     * @return string|null
     */
    public function getLastCommercialActivity()
    {
        $dealOrderCmp = $this->App()->getComponentByName('DealOrder');
        if($dealOrderCmp){
            $App = $this->App();
            $orderSet = $App->OrderSet();
            $orderSet->deal();
            $orders = $orderSet->select($orderSet->deal->customer->is($this->id));
            $orders->orderDesc($orderSet->requestDate);
    
            foreach ($orders as $order) {
                return $order->requestDate;
            }
        }

        return null;
    }

    /**
     * Select contacts linked to this organization.
     *
     * @return \ORM_Iterator	<ContactOrganization>
     */
    public function selectContacts()
    {
        $App = $this->App();
        $App->includeContactSet();
        $set = $App->ContactOrganizationSet();
        $set->contact();

        $contacts = $set->select(
            $set->organization->is($this->id)->_AND_($set->contact->deleted->is(0))
        );

        $contacts->orderAsc($set->contact->lastname);

        return $contacts;
    }  
    
    /**
     * Select organization types linked to this organization.
     *
     * @return OrganizationType[]|\ORM_Iterator
     */
    public function getOrganizationTypes()
    {
        $App = $this->App();
        $organizationTypeSet = $App->OrganizationTypeSet();
        $organizationTypeOrganizationSet = $App->OrganizationTypeOrganizationSet();
        
        return $organizationTypeSet->select(
            $organizationTypeSet->id->in(
                $organizationTypeOrganizationSet->organization->is($this->id),
                'organizationType'
            )
        )->orderAsc($organizationTypeSet->name);
    }
    
    public function getFormOutputValues()
    {
        $values = parent::getFormOutputValues();
        $organizationTypes = $this->getOrganizationTypes();
        $types = array();
        foreach($organizationTypes as $organizationType){
            $types[] = $organizationType->id;
        }
        $values['types'] = $types;
        return $values;
    }
    
    public function addType(OrganizationType $type)
    {
        $App = $this->App();
        $organizationTypeOrganizationSet = $App->OrganizationTypeOrganizationSet();
        
        $organizationType = $organizationTypeOrganizationSet->get(
            $organizationTypeOrganizationSet->all(
                array(
                    $organizationTypeOrganizationSet->organization->is($this->id),
                    $organizationTypeOrganizationSet->organizationType->is($type->id)
                )
            )
        );
        
        if(!isset($organizationType)){
            $organizationType = $organizationTypeOrganizationSet->newRecord();
            $organizationType->organization = $this->id;
            $organizationType->organizationType = $type->id;
            $organizationType->save();
        }
        
        return true;
    }
}