<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * A ContactPhone defines a list of phone for a contact
 *
 * @property ORM_PhoneField                 $phone
 * @property ORM_StringField                $title
 * @property ORM_IntField                   $rank
 * @property ContactSet                     $contact
 * @property ContactOrganizationSet         $contactOrganization
 *
 * @method ContactPhone                     get()
 * @method ContactPhone                     request()
 * @method ContactPhone[]|\ORM_Iterator     select()
 * @method ContactPhone                     newRecord()
 * @method Func_App                         App()
 */
class ContactPhoneSet extends \app_RecordSet
{

    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ContactPhone');
        
        $this->setDescription('Contact phone');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_PhoneField('phone')->setDescription($App->translate('Phone')),
            ORM_StringField('title'),
            ORM_IntField('rank')
        );

        $this->hasOne('contact', $App->ContactSetClassName());
        $this->hasOne('contactOrganization', $App->ContactOrganizationSetClassName());
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ContactPhoneBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ContactPhoneAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return true;
    }
    
    
    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    
    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->all();
    }
    
    /**
     * Returns a criterion matching personal records
     * @return \ORM_Criteria
     */
    public function isPersonal()
    {
        return $this->contact->isNot(0)->_AND_($this->contactOrganization->is(0));
    }
}

class ContactPhoneBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ContactPhoneAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}