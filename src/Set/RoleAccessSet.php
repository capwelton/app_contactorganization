<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * @property ORM_StringField        $object
 * @property ORM_StringField        $action
 * @property ORM_StringField        $criterion
 * @property RoleSet                $role
 * 
 * @method RoleSet      role()
 * @method RoleAccess   get(mixed $criteria)
 * @method RoleAccess   request(mixed $criteria)
 * @method RoleAccess[] select(\ORM_Criteria $criteria = null)
 * @method RoleAccess   newRecord()
 */
class RoleAccessSet extends \app_RecordSet
{
    /**
     *
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'RoleAccess');
        
        $this->setDescription('Role access');
        
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('object')
                ->setDescription('Object'),
            ORM_StringField('action')
                ->setDescription('Action'),
            ORM_StringField('criterion')
                ->setDescription('Criterion')
        );

        $this->hasOne('role', $App->RoleSetClassName());
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new RoleAccessBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new RoleAccessAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }

    /**
     * @param \app_RecordSet $recordSet
     * @return \ORM_Criterion
     */
    public function hasObject(\app_RecordSet $recordSet)
    {
        $classname = substr($recordSet->getRecordClassName(), strlen($recordSet->App()->classPrefix));
        return $this->object->is($classname);
    }
    /**
     *
     * {@inheritdoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return bab_isUserAdministrator();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        if (bab_isUserAdministrator()) {
            return $this->all();
        }
        return $this->none;
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        if (bab_isUserAdministrator()) {
            return $this->all();
        }
        return $this->none;
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class RoleAccessBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class RoleAccessAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}