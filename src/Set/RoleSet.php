<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * @property	 ORM_StringField	$name             The name of the role
 * @property	 ORM_TextField		$description      A longer description of the role
 *
 * @method Role         get(mixed $criteria)
 * @method Role         request(mixed $criteria)
 * @method Role[]       select(\ORM_Criteria $criteria = null)
 * @method Role         newRecord()
 * @method Func_App     App()
 */
class RoleSet extends \app_RecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Role');
        
        $this->setDescription('Role');
        
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription($App->translate('Name')),
			ORM_TextField('description')
                ->setDescription($App->translate('Description'))
        );

        $this->hasMany('contactRoles', $App->ContactRoleSetClassName(), 'role');
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new RoleBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new RoleAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }


    /**
     *
     * {@inheritdoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return bab_isUserAdministrator();
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        if (bab_isUserAdministrator()) {
            return $this->all();
        }
        return $this->none;
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        if (bab_isUserAdministrator()) {
            return $this->all();
        }
        return $this->none;
    }

    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class RoleBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class RoleAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}