<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';


/**
 * A ContactOrganization is a contact in an organization
 * this is the set object definitition
 *
 * @property ORM_StringField                $position
 * @property ORM_IntField                   $contactRank
 * @property ORM_IntField                   $organizationRank
 * @property ORM_DateField                  $start
 * @property ORM_DateField                  $end
 * @property ContactSet                     $contact
 * @property OrganizationSet                $organization
 * @property ContactSet                     $lineManager        // Superieur hierarchique
 * @property ContactOrganizationType        $type
 *
 * @method ContactOrganization                  get()
 * @method ContactOrganization                  request()
 * @method ContactOrganization[]|\ORM_Iterator  select()
 * @method ContactOrganization                  newRecord()
 * @method \Func_App                             App()
 */
class ContactOrganizationSet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ContactOrganization');
        
        $this->setDescription('Contact organization');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('service')
                ->setDescription('Service'),
            ORM_StringField('position')
                ->setDescription($App->translate('Position')),
            ORM_IntField('contactRank')
                ->setDescription($App->translate('Rank for contact')),
            ORM_IntField('organizationRank')
                ->setDescription($App->translate('Rank for organization')),
            ORM_DateField('start')
                ->setDescription($App->translate('Association date beetween contact and organization')),
            ORM_DateField('end')
                ->setDescription($App->translate('End date of association beetween contact and organization')),
            ORM_PhoneField('phone')
                ->setDescription($App->translate('Phone')),
            ORM_PhoneField('mobile')
                ->setDescription($App->translate('Mobile')),
            ORM_PhoneField('fax')
                ->setDescription($App->translate('Fax')),
            ORM_EmailField('email')
                ->setDescription($App->translate('Email'))
        );

        $this->hasOne('contact', $App->ContactSetClassName())
            ->setDescription($App->translatable('Contact'));
        $this->hasOne('organization', $App->OrganizationSetClassName())
            ->setDescription($App->translate('Organization'));
        $this->hasOne('type', $App->ContactOrganizationTypeSetClassName())
            ->setDescription($App->translate('Type'));
        
        $this->hasOne('lineManager', $App->ContactSetClassName())
            ->setDescription($App->translate('Line manager'));
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ContactOrganizationBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ContactOrganizationAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function getRequiredComponents()
    {
        return array(
            'ENTRY'
        );
    }

    /**
     * Matches contact-organization links active at the specified date or today
     * if no date is specified.
     *
     * @since 0.9.13
     * @param string|null $date
     *            ISO formatted date. null = today
     *
     * @return \ORM_Criteria
     */
    public function isActive($date = null)
    {
        if (! isset($date)) {
            $date = date('Y-m-d');
        }

        return $this->all(
            $this->end->is('0000-00-00')->_OR_($this->end->greaterThan($date)),
            $this->start->is('0000-00-00')->_OR_($this->start->lessThanOrEqual($date))
        );
    }


    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->all();
    }
}

class ContactOrganizationBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ContactOrganizationAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}