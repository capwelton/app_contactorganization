<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set\RecordViews;

class OrganizationPreviewRecordView
{
    /**
     * @var \Func_App
     */
    private $app;
    public function __construct(\Func_App $App)
    {
        $this->app = $App;
    }
    
    public function generate($force = false)
    {
        $App = $this->app;
        $customSectionSet = $App->CustomSectionSet();
        $object = $App->classPrefix.'Organization';
        $view = 'preview';
        
        $criteria = $customSectionSet->all(
            $customSectionSet->object->is($object)
            ->_AND_($customSectionSet->view->is($view))
        );
        
        $customSections = $customSectionSet->select(
            $criteria
        );
        
        if($customSections->count() > 0 && !$force)
        {
            return $this;
        }
        
        $customContainerSet = $App->CustomContainerSet();
        
        $containerCriteria = $customContainerSet->all(
            $customContainerSet->object->is($object)
            ->_AND_($customContainerSet->view->is($view))
        );
        
        $customContainerSet->delete($containerCriteria);
        $customSectionSet->delete($criteria);
        
        $rank = 0;
        
        $container = $customContainerSet->newRecord();
        $container->view = $view;
        $container->object = $object;
        $container->sizePolicy = 'col-md-12';
        $container->layout = 'vbox';
        $container->save();
        
        //Logo
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'verticalLabel';
        $customSection->object = $object;
        $customSection->fields = 'logo';
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
        
        //Organization
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'verticalLabel';
        $customSection->object = $object;
        $customSection->fields = 'name:type=title1;label=__;classname=;sizePolicy=,description,parent,children,types,commercialReferralContact,responsible,name2,address';
        $customSection->classname = 'box yellow';
        $customSection->editable = true;
        $customSection->name = $App->translate('Organization');
        $customSection->rank = $rank++;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
        
        //Invoice information
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'veryWideHorizontalLabel';
        $customSection->object = $object;
        $customSection->fields = 'payingOrganization,iban,bic,rib,intracommunityVat,status';
        $customSection->classname = 'box red';
        $customSection->editable = true;
        $customSection->name = $App->translate('Invoice information');
        $customSection->rank = $rank++;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
        
        //Contact
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'noLabel';
        $customSection->object = $object;
        $customSection->fields = 'contacts';
        $customSection->classname = 'box mangeta';
        $customSection->editable = true;
        $customSection->name = $App->translate('Contacts');
        $customSection->rank = $rank++;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
        
        if($App->getComponentByName('Note')){
            //Notes
            $customSection = $customSectionSet->newRecord();
            $customSection->sizePolicy = 'col-md-12';
            $customSection->fieldsLayout = 'noLabel';
            $customSection->object = $object;
            $customSection->fields = 'notes';
            $customSection->classname = 'box blue';
            $customSection->editable = false;
            $customSection->name = $App->translate('Notes');
            $customSection->rank = $rank++;
            $customSection->view = $view;
            $customSection->container = $container->id;
            $customSection->save();
        }
    }
}