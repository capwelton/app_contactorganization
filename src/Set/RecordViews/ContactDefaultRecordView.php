<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set\RecordViews;

class ContactDefaultRecordView
{
    /**
     * @var \Func_App
     */
    private $app;
    public function __construct(\Func_App $App)
    {
        $this->app = $App;
    }
    
    public function generate($force = false)
    {
        $App = $this->app;
        $customSectionSet = $App->CustomSectionSet();
        $object = $App->classPrefix.'Contact';
        $view = '';
        
        $criteria = $customSectionSet->all(
            $customSectionSet->object->is($object)
            ->_AND_($customSectionSet->view->is($view))
        );
        
        $customSections = $customSectionSet->select(
            $criteria
        );
        
        if($customSections->count() > 0 && !$force)
        {
            return $this;
        }
        
        $customContainerSet = $App->CustomContainerSet();
        
        $containerCriteria = $customContainerSet->all(
            $customContainerSet->object->is($object)
            ->_AND_($customContainerSet->view->is($view))
        );
        
        $customContainerSet->delete($containerCriteria);
        $customSectionSet->delete($criteria);
        
        $rank = 0;
        
        $container = $customContainerSet->newRecord();
        $container->view = $view;
        $container->object = $object;
        $container->sizePolicy = 'col-md-12';
        $container->layout = 'vbox';
        $container->save();
        
        //Contact
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-4';
        $customSection->fieldsLayout = 'verticalLabel';
        $customSection->object = $object;
        $customSection->fields = 'fullname:type=title1;label=__;classname=title1;sizePolicy=,photo:type=;label=__;classname=;sizePolicy=,email,responsible,address,tags,contactPhones:type=;label=Téléphoone Personnel;classname=;sizePolicy=,userAccount';
        $customSection->classname = 'box yellow';
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
        
        //Organisme
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-4';
        $customSection->fieldsLayout = 'noLabel';
        $customSection->object = $object;
        $customSection->fields = 'organizationsAndPositions';
        $customSection->classname = 'box purple';
        $customSection->editable = false;
        $customSection->name = $App->translate('Organizations');
        $customSection->rank = $rank++;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
        
        if($taskComponent = $App->getComponentByName('Task')){
            //Tasks
            $customSection = $customSectionSet->newRecord();
            $customSection->sizePolicy = 'col-md-4';
            $customSection->fieldsLayout = 'verticalLabel';
            $customSection->object = $object;
            $customSection->fields = 'pendingTasks:type=;label=__;classname=;sizePolicy=';
            $customSection->classname = 'box blue';
            $customSection->editable = false;
            $customSection->name = $taskComponent->translate('Actions/Tasks');
            $customSection->rank = $rank++;
            $customSection->view = $view;
            $customSection->container = $container->id;
            $customSection->save();
        }
        
        if($noteComponent = $App->getComponentByName('Note')){
            //Notes
            $customSection = $customSectionSet->newRecord();
            $customSection->sizePolicy = 'col-md-4';
            $customSection->fieldsLayout = 'noLabel';
            $customSection->object = $object;
            $customSection->fields = 'notes';
            $customSection->classname = 'box green';
            $customSection->editable = false;
            $customSection->name = $noteComponent->translate('Edit note type');
            $customSection->rank = $rank++;
            $customSection->view = $view;
            $customSection->container = $container->id;
            $customSection->save();
        }
    }
}