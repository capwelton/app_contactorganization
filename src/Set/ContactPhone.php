<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';
/**
 * A ContactPhone defines a phone for a contact
 *
 * @property int                    $phone
 * @property string                 $title
 * @property int                    $rank
 * @property Contact                $contact
 * @property ContactOrganization    $contactOrganization
 *
 * @method ContactPhoneSet      getParentSet()
 * @method Contact              contact()
 * @method ContactOrganization  contactOrganization()
 * @method Func_App             App()
 */
class ContactPhone extends \app_Record
{
    /**
     * {@inheritDoc}
     * @see \ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->phone;
    }
    
    public function save()
    {
        $this->phone = str_replace(' ', '', $this->phone);
        return parent::save();
    }
    
    /**
     * @return boolean
     */
    public function isPersonal()
    {
        return $this->contact != 0 && $this->contactOrganization == 0;
    }
    
    
    /**
     * 
     * @param boolean $simpleMode
     * @return \Widget_Label|\Widget_Displayable_Interface
     */
    public function displayPhone($simpleMode = true, $noTitle = false)
    {
        $phoneField = $this->getParentSet()->phone;
        
        if ($simpleMode) {
            $phoneText = $this->phone;
            $phoneClass = \Func_Icons::OBJECTS_PHONE;
            $PhoneNumber = \bab_Functionality::get('PhoneNumber');
            if ($PhoneNumber) {
                $phoneNumberUtil = $PhoneNumber->PhoneNumberUtil();
                
                try {
                    $phoneNumberObject = $phoneNumberUtil->parse($this->phone, 'FR');
                    $phoneType = $phoneNumberUtil->getNumberType($phoneNumberObject);
                    if ($phoneType == \libphonenumber\PhoneNumberType::FIXED_LINE) {
                        $phoneClass = \Func_Icons::OBJECTS_PHONE;
                    } else {
                        $phoneClass = \Func_Icons::OBJECTS_MOBILE;
                    }
                    $phoneText = $phoneNumberUtil->formatOutOfCountryCallingNumber($phoneNumberObject, 'FR');
                } catch (\libphonenumber\NumberParseException $e) {
                }
                
                if ($this->isPersonal()) {
                    $phoneClass = \Func_Icons::PLACES_USER_HOME;
                }
            }
            
            $W = bab_Widgets();
            $labelPhone = $W->Label(
                $phoneText
            )->addClass('icon', $phoneClass);
                
            if(!$noTitle){
                $labelTitle = $W->Label(
                    $this->getTitle()
                );
            }
            else{
                $labelTitle = null;
            }
            
            return $W->FlowItems($labelPhone,$labelTitle)
            ->setSizePolicy(\Func_Icons::ICON_LEFT_16)
            ->setTitle($this->getTitle());
        }
        
        /* @var $widget \Widget_InternationalTelLineEdit */
        $widget = $phoneField->getWidget();
        $widget->setValue($this->phone);
        $widget->allowDropdown(false);
        $widget->disable();
        $widget->setTitle($this->getTitle());
                
        $link = $phoneField->outputWidget($this->phone);
        $link->setItem($widget);
        return $link;
    }
    
    
    /**
     * 
     * @return string
     */
    public function getTitle()
    {
        if (empty($this->title)) {
            if ($this->isPersonal()) {
                return $this->App()->translate('Personal');
            }
        }
        return $this->title;
    }
}
