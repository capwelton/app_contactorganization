<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

use Capwelton\App\Address\Set;
use Capwelton\App\ContactOrganization\Ui\OrganizationRecordView;
use Capwelton\App\ContactOrganization\Set\RecordViews\OrganizationDefaultRecordView;
use Capwelton\App\ContactOrganization\Set\RecordViews\OrganizationPreviewRecordView;
use Capwelton\App\Address\Set\AddressSet;

/**
 * An Organization may be a company, association...
 *
 * @property ORM_StringField                $name
 * @property ORM_TextField                  $description
 * @property ORM_StringField                $activity
 * @property ORM_EmailField                 $email
 * @property ORM_StringField                $phone
 * @property ORM_StringField                $fax
 * @property ORM_UrlField                   $website
 *
 * @property OrganizationSet                $parent
 * @property OrganizationTypeSet            $type
 * @property AddressSet                     $address
 *
 * @property OrganizationSet                $responsibleOrganization
 * @property ContactSet                     $responsible
 * 
 * @property ContactSet                     $commercialReferralContact
 * 
 * @method Organization                     get()
 * @method Organization                     request()
 * @method Organization[]|\ORM_Iterator     select()
 * @method Organization                     newRecord()
 * @method AddressSet                       address()
 * @method Func_App                         App()
 */
class OrganizationSet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Organization');

        $this->setDescription('Organization');
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('code')->setDescription(
                $App->translatable('Internal identification code')
            ),
            ORM_StringField('accountNumber')->setDescription(
                $App->translatable('Account number')
            ),
            ORM_StringField('initials')->setDescription(
                $App->translatable('Initials')
            ),
            ORM_StringField('name')->setDescription(
                $App->translatable('Trade name 1')    
            ),
            ORM_StringField('name2')->setDescription(
                $App->translatable('Trade name 2')    
            ),
            ORM_TextField('description')->setDescription(
                $App->translatable('Description')
            ),
            ORM_EmailField('email')->setDescription(
                $App->translatable('Email')
            ),
            ORM_StringField('phone')->setDescription(
                $App->translatable('Phone')
            ),
            ORM_StringField('phone2')->setDescription(
                $App->translatable('Phone 2')
            ),
            ORM_StringField('fax')->setDescription(
                $App->translatable('Fax')
            ),
            ORM_UrlField('website')->setDescription(
                $App->translatable('Web site')
            ),
            ORM_StringField('siret')->setDescription(
                $App->translatable('Siret number')
            ),
            ORM_StringField('siren')->setDescription(
                $App->translatable('Siren number')
            ),
            ORM_StringField('rcs')->setDescription(
                $App->translatable('RCS')
            ),
            ORM_StringField('ape')->setDescription(
                $App->translatable('APE')
            ),
            ORM_StringField('intracommunityVat')->setDescription(
                $App->translatable('Intracommunity VAT')
            ),
            ORM_StringField('iban')->setDescription(
                $App->translatable('IBAN')
            ),
            ORM_StringField('bic')->setDescription(
                $App->translatable('BIC')
            ),
            ORM_StringField('rib')->setDescription(
                $App->translatable('RIB')
            ),
            ORM_IntField('effectif')->setDescription(
                $App->translatable('Effective')
            ),
            ORM_DateTimeField('effectifDate')->setDescription(
                $App->translatable('Effective update date')
            ),
            ORM_IntField('effectifPerson')->setDescription(
                $App->translatable('Person who edited the effectif')
            ),
            ORM_CurrencyField('CA')->setDescription(
                $App->translatable('CA')
            ),
            ORM_DateTimeField('CADate')->setDescription(
                $App->translatable('CA Update Date')
            ),
            ORM_IntField('CAPerson')->setDescription(
                $App->translatable('Person who edited the CA')
            ),
            ORM_BoolField('ESS')->setDescription(
                $App->translatable('ESS')
            )->setOutputOptions($App->translate('No'), $App->translate('Yes')),
            ORM_StringField('account')->setDescription(
                $App->translatable('Account')
            ),
            ORM_StringField('paymentCode')->setDescription(
                $App->translatable('Payment code')
            ),
            ORM_StringField('accountNature')->setDescription(
                $App->translatable('Account nature')
            ),
            ORM_TextField('paymentTerms')->setDescription(
                $App->translatable('Terms of payment')
            )
        );

        $this->hasOne('parent', $App->OrganizationSetClassName())->setDescription(
            $App->translatable('Parent organization')
        );
        $this->hasOne('payingOrganization', $App->OrganizationSetClassName())->setDescription(
            $App->translatable('Paying organization')
        );

        $this->hasOne('address', $App->AddressSetClassName())->setDescription(
            $App->translatable('Address')
        );

        $this->hasOne('type', $App->OrganizationTypeSetClassName())->setDescription(
            $App->translatable('Type')
        );

        $this->hasOne('responsibleOrganization', $App->OrganizationSetClassName())->setDescription(
            $App->translatable('Responsible organization')
        );
        $this->hasOne('responsibleContact', $App->ContactSetClassName())->setDescription(
            $App->translatable('Responsible contact')
        );
        $this->hasOne('responsible', $App->ContactSetClassName())->setDescription(
            $App->translatable('Internal responsible contact')
        );
        
        $this->hasOne('commercialReferralContact', $App->ContactSetClassName())->setDescription(
            $App->translatable('Commercial referral contact')
        );
        $this->hasOne('legalStatus', $App->LegalStatusSetClassName())->setDescription(
            $App->translatable('Legal status')
        );
        
        // Contacts
        $this->hasMany('contactOrganizations', $App->ContactOrganizationSetClassName(), 'organization');
        
        $projectCmp = $App->getComponentByName('PROJECT');
        if($projectCmp){
            // Deals
            $App->includeProjectSet();
            $this->hasMany('projectsAsCustomer', $App->ProjectSetClassName(), 'customer');
            $this->hasMany('projectsAsResponsible', $App->ProjectSetClassName(), 'responsibleOrganization');
        }
        $salesDocumentsCmp = $App->getComponentByName('INCOTERMS');
        if($salesDocumentsCmp){
            $this->hasOne("incoterms",$App->IncotermsSetClassName());
            $this->hasOne("paymentTerm",$App->PaymentTermSetClassName());
        }
        
        $currencyCmp = $App->getComponentByName('CURRENCY');
        if($currencyCmp){
            $this->hasOne('currency', $App->CurrencySetClassName());
        }

        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new OrganizationBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new OrganizationAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }

    public function getOptionalComponents()
    {
        return array(
            'DEALORDER',
            'ATTACHMENT',
            'TAG',
            'CURRENCY'
        );
    }

    public function getRequiredComponents()
    {
        return array(
            'ADDRESS'
        );
    }
    
    public function onUpdate()
    {
        $defaultView = new OrganizationDefaultRecordView($this->App());
        $defaultView->generate();
        $previewView = new OrganizationPreviewRecordView($this->App());
        $previewView->generate();
    }
    
    /**
     * Returns the current main organization or null if not defined.
     * @param \Func_App App
     * @return int|null
     */
    public static function getMainOrganizationId(\Func_App $App)
    {
        return \bab_Registry::get("/{$App->addonName}/mainOrganization", null);
    }
    
    
    
    /**
     * Sets the current main organization.
     *
     * @param int $organizationId
     */
    function setMainOrganizationId($organizationId, \Func_App $App)
    {
        \bab_Registry::set("/{$App->addonName}/mainOrganization", $organizationId);
    }



    /**
     * Matches organizations whose parent, grand-parent, great-grand-parent or
     * great-great-grand-parent is $organization.
     *
     * @since 1.0.23
     * @param Organization|int $organization
     * @return \ORM_Criterion
     */
    public function isDescendantOf($organization)
    {
        $App = $this->App();
        $organizationSet = $App->OrganizationSet();

        if ($organization instanceof Organization) {
            $organization = $organization->id;
        }

        $isDescendant = $organizationSet->any(
            $organizationSet->id->is($organization),
            $organizationSet->parent()->id->is($organization),
            $organizationSet->parent()->parent()->id->is($organization),
            $organizationSet->parent()->parent()->parent()->id->is($organization),
            $organizationSet->parent()->parent()->parent()->parent()->id->is($organization)
        );

        return $this->id->in($isDescendant, 'id');
    }


    /**
     * Returns an iterator of organization linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @return \ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = 'hasOrganization')
    {
        return parent::selectLinkedTo($source, $linkType);
    }


    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->all();
    }


    /**
     *
     * @return \ORM_Criteria
     */
    public function isInternal()
    {
        if (!isset($this->isInternal)) {
            $App = $this->App();
            $mainOrganizationId = $App->getMainOrganizationId();

            $isInternal = $this->isDescendantOf($mainOrganizationId);

            $this->addFields(
                ORM_CriterionOperation('isInternal', $isInternal)
                    ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
                    ->setDescription('Is an internal organization')
            );
        }

        return $this->isInternal;
    }



    	/**
	 * Import a CSV row
	 *
	 * @throw crm_ImportException
	 *
	 * @param	\app_Import			$import
	 * @param	\Widget_CsvRow		$row
	 */
	public function import(\app_Import $import, \Widget_CsvRow $row, $notrace = false)
	{
	    $App = $this->App();
		if ($row->uuid) {

			$organization = $this->get($this->uuid->is($row->uuid));

			if (!$organization) {

				$message = sprintf($App->translate('Error on line %d, there is a value in the unique identifier column but the organization does not exists in the database.
To create the organization, you must remove the value in the universally unique identifier column'), $row->line());

				$exception = new \Widget_ImportException($message);
				$exception->setCsvRow($row);

				throw $exception;
			}

		} 
		else {
		    if($this->get($this->accountNumber->is($row->accountNumber))){
		        $message = sprintf($App->translate('Error on line %d, there is a value in the account number that already exist'), $row->line());
		        $exception = new \Widget_ImportException($message);
		        $exception->setCsvRow($row);
                throw $exception;
		    }
		    if($this->get($this->name->is($row->name))){
		        $message = sprintf($App->translate('Error on line %d, there is a value in the name that already exist'), $row->line());
		        $exception = new \Widget_ImportException($message);
		        $exception->setCsvRow($row);
		        throw $exception;
		    }
		    if(!empty($row->intracommunityVat) && $this->get($this->intracommunityVat->is($row->intracommunityVat))){
		        $message = sprintf($App->translate('Error on line %d, there is a value in the intracommunityVAT that already exist'), $row->line());
		        $exception = new \Widget_ImportException($message);
		        $exception->setCsvRow($row);
		        throw $exception;
		    }
		    
			$organization = $this->newRecord();
		    $organization->save(true);
		}


		$organization->import($row);
        $organization->save();
        $organization->updateMainAddress();
        $import->addOrganization($organization);

		$organization->importLinked($row);
		$organization->save();

		return true;
	}


	/**
	 * @param int	$organizationType
	 * @return \ORM_Criteria
	 */
	public function hasType($organizationType)
	{
	    $App = $this->App();

	    $organizationTypeOrganizationSet = $App->OrganizationTypeOrganizationSet();

	    $organizationTypeOrganizations = $organizationTypeOrganizationSet->select(
	        $organizationTypeOrganizationSet->organizationType->is($organizationType)
        );

	    $organizationIds = array();
	    foreach ($organizationTypeOrganizations as $organizationTypeOrganization) {
	        $organizationId = $organizationTypeOrganization->organization;
	        $organizationIds[$organizationId] = $organizationId;
	    }

	    return $this->id->in($organizationIds);
	}
}

class OrganizationBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class OrganizationAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}