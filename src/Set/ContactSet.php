<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

use Capwelton\App\Address\Set;
use Capwelton\App\ContactOrganization\Set\RecordViews\ContactDefaultRecordView;

/**
 * A Contact is a person
 *
 * @property ORM_StringField                $firstname      First name
 * @property ORM_StringField                $lastname       Last name
 * @property ORM_EnumField                  $title          The civil title
 * @property ORM_StringField                $initials       Initials
 * @property ORM_UserField                  $user           The (optional) ovidentia user associated to this contact.
 * @property ORM_StringField                $phone
 * @property ORM_StringField                $mobile
 * @property ORM_StringField                $fax
 * @property ORM_StringField                $email
 * @property AddressSet                     $address
 * @property ContactSet                     $responsible	The collaborator responsible for this contact.
 * @property OrganizationSet                $responsibleOrganization
 *
 * @method Contact      get(mixed $criteria)
 * @method Contact      request(mixed $criteria)
 * @method Contact[]    select(\ORM_Criteria $criteria = null)
 * @method Contact      newRecord()
 * @method \Func_App     App()
 */
class ContactSet extends \app_TraceableRecordSet
{
    /**
     *
     * @var ContactOrganizationSet
     */
    protected $contactOrganizationSet = null;

    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Contact');

        $this->setDescription('Contact');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('firstname')
                ->setDescription($App->translatable('First name')),
            ORM_StringField('lastname')
                ->setDescription($App->translatable('Last name')),
            ORM_EnumField('title', $App->translateArray(Contact::getTitles()))
                ->setDescription($App->translatable('Title')),
            ORM_StringField('initials')
                ->setDescription($App->translatable('Initials')),
            ORM_UserField('user')
                ->setDescription($App->translatable('Associated portal user id')),
            ORM_PhoneField('phone')
                ->setDescription($App->translatable('Phone')),
            ORM_PhoneField('mobile')
                ->setDescription($App->translatable('Mobile')),
            ORM_PhoneField('fax')
                ->setDescription($App->translatable('Fax')),
            ORM_EmailField('email')
                ->setDescription($App->translatable('Email'))
        );

        $this->hasOne('mainPosition', $App->ContactOrganizationSetClassName())
            ->setDescription('Main Position');

        $this->hasOne('responsible', $App->ContactSetClassName())
            ->setDescription('The collaborator responsible for this contact');
        
        $this->hasOne('responsibleOrganization', $App->OrganizationSetClassName())
            ->setDescription('Responsible organization');

        $this->hasOne('address', $App->AddressSetClassName())
            ->setDescription('Address');

        $this->hasMany('contactOrganizations', $App->ContactOrganizationSetClassName(), 'contact');

        $this->hasMany('contactRoles', $App->ContactRoleSetClassName(), 'contact');
        
        $this->addCustomFields();
    }
    
    public function getCurrentContact()
    {
        $App = $this->App();
        $currentUser = $App->getCurrentUser();
        if(!isset($currentUser)){
            return null;
        }
        
        $set = $App->ContactSet();
        return $set->get($set->user->is($currentUser));
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ContactBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ContactAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function onUpdate()
    {
        $defaultView = new ContactDefaultRecordView($this->App());
        $defaultView->generate();
    }
    
    
    
    public function getRequiredComponents()
    {
        return array(
            'ADDRESS',
            'EMAIL'
        );
    }


    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @return \ORM_Criterion
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @return \ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @return \ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->all();
    }


    /**
     * Returns a criterion matching records for which the current user can grant access rights.
     *
     * @return \ORM_Criterion
     */
    public function isGrantableAccess()
    {
        if (bab_isUserAdministrator()) {
            return $this->all();
        }
        return $this->none();
    }


    /**
     * Creates the specified number of random contacts.
     *
     * @param int	$nbContacts
     * @return array	The ids of contacts actually created
     */
    public function populateRandomly($nbContacts)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $Pop = \bab_functionality::get('Populator');
        if (!($Pop instanceof \bab_functionality)) {
            return 0;
        }

        // if joined with address we also fill addresses
        $withAddress = ($this->address instanceof \ORM_RecordSet);

        $CountrySet = $this->App()->CountrySet();
        $country = $CountrySet->get($CountrySet->code->is('FR'));


        $recordIds = array();
        for ($nbCreatedContacts = 0; $nbCreatedContacts < $nbContacts; $nbCreatedContacts++) {
            $userinfo = $Pop->getRandomUserInfo();
            $contact = $this->newRecord();
            $contact->firstname = $userinfo['givenname'];
            $contact->lastname = $userinfo['sn'];
            $contact->title = $userinfo['gender'] == 'M' ? Contact::TITLE_MR : Contact::TITLE_MRS;
            $contact->gender = $userinfo['gender'] == 'M' ? Contact::GENDER_MALE : Contact::GENDER_FEMALE;
            $birthdate = \BAB_DateTime::now();
            $birthdate->add(-rand(18 * 365, 65 * 365), BAB_DATETIME_DAY);
            $contact->birthday = $birthdate->getIsoDate();

            $filepath = $Pop->getRandomPhoto($userinfo['gender']);

            $contact->photo = basename($filepath);

            if ($withAddress) {
                $contact->address->street = $userinfo['bstreetaddress'];
                $contact->address->postalCode = $userinfo['bpostalcode'];
                $contact->address->city = $userinfo['bcity'];
                $contact->address->country = $country->id;
            }
            $contact->save();
            $contact->importPhoto(new \bab_Path($filepath), false);

            $recordIds[] = $contact->id;
        }
        return $recordIds;
    }





    /**
     * Contact organization set with cache in contactSet
     *
     * @see Contact::getContactOrganizationSet()
     *
     * @return ContactOrganizationSet
     */
    public function getContactOrganizationSet()
    {
        if (null === $this->contactOrganizationSet)
        {
            $this->App()->includeOrganizationSet();

            $this->contactOrganizationSet = $this->App()->ContactOrganizationSet();
            $this->contactOrganizationSet->organization();
        }

        return $this->contactOrganizationSet;
    }




    /**
     * Returns a criteria for matching contacts being members of the specified
     * organization.
     *
     * @param int|array	$organization		The organization id
     *
     * @return \ORM_Criteria
     */
    public function hasOrganization($organization)
    {
        $contactOrganizationSet = $this->getContactOrganizationSet();

        return $this->id->in(
            $contactOrganizationSet->all(
                $contactOrganizationSet->organization->id->in($organization),
                $contactOrganizationSet->isActive(),
                $contactOrganizationSet->organization->deleted->is(0)
            ),
            'contact'
        );
    }


//     /**
//      * @param Import $import
//      * @param \Widget_CsvRow $row
//      * @param boolean $notrace
//      * @throws \Widget_ImportException
//      * @return boolean
//      */
//     public function import(Import $import, \Widget_CsvRow $row, $notrace = false)
//     {
//         $App = $this->App();

//         $isUpdate = false;

//         if (isset($row->uuid) && $row->uuid) {
//             $contact = $this->get($this->uuid->is($row->uuid));

//             echo $contact->uuid . ' - ' . $contact->lastname . ' ' . $contact->firstname;

//             if (!$contact) {
//                 $message = sprintf(
//                     $App->translate(
//                         'Error on line %d, there is a value in the unique identifier column but the contact does not exists in the database.' . "\n"
//                         . 'To create the contact, you must remove the value in the universally unique identifier column'
//                     ),
//                     $row->line()
//                 );

//                 $exception = new \Widget_ImportException($message);
//                 $exception->setCsvRow($row);
//                 throw $exception;
//             }

//             $isUpdate = true;
//         } else {
//             $contact = $this->newRecord();
//         }

//         $contact->import($row);

//         if (!$isUpdate) {
//             $sameContacts = $this->select(
//                 $this->lastname->like($contact->lastname)->_AND_(
//                     $this->firstname->like($contact->firstname)
//                 )
//             );

//             if ($sameContacts->count() > 0) {
//                 $message = sprintf($App->translate("Line %d, a contact named '%s' already exists."), $row->line(), $contact->getFullName());
//                 $exception = new \Widget_ImportException($message);
//                 $exception->setCsvRow($row);
//                 throw $exception;
//             }
//         }

//         $contact->save();

//         $contact->importLinked($row);
//         $contact->save();

//         $import->addContact($contact);

//         return true;
//     }
}

class ContactBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ContactAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}