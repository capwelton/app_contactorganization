<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * This set stores information about who knows who.
 *
 * @property ContactSet         $contactKnown   The contact who is known
 * @property ContactSet         $contactKnowing The contact who knows
 * @property ContactAcquaintanceLevelSet $level The level of acquaintance
 */
class ContactAcquaintanceSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ContactAcquaintance');
        
        $this->setDescription('Contact acquaintance');
        
        $this->hasOne('contactKnown', $App->ContactSetClassName());   // The contact who is known
        $this->hasOne('contactKnowing', $App->ContactSetClassName()); // The contact who knowns
        $this->hasOne('level', $App->ContactAcquaintanceLevelSetClassName());
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ContactAcquaintanceBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ContactAcquaintanceAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }

    /**
     * Select contacts knowning the specified contact
     *
     * @param int | Contact             $contactKnown   The contact
     * @param ContactAcquaintanceLevel  $minLevel       The minimum level of acquaintance the contact should have
     * @param ContactAcquaintanceLevel  $maxLevel       The maximum level of acquaintance the contact should have
     *
     * @return \ORM_Iterator <Contact>
     */
    public function selectKnowing($contactKnown, ContactAcquaintanceLevel $minLevel = null, ContactAcquaintanceLevel $maxLevel = null)
    {
        $set = $this->App()->ContactAcquaintanceSet();
        $set->level();
        
        if ($contactKnown instanceof Contact) {
            $contactKnown = $contactKnown->id;
        }
        
        $criteria = $set->contactKnown->is($contactKnown);
        if (isset($minLevel)) {
            $criteria = $criteria->_AND_($set->level->rank->greaterThanOrEqual($minLevel->rank));
        }
        if (isset($maxLevel)) {
            $criteria = $criteria->_AND_($set->level->rank->lessThanOrEqual($maxLevel->rank));
        }
        return $set->select($criteria)->orderAsc($set->level->rank);
    }



    /**
     * Select contacts known by the specified contact
     *
     * @param int | Contact             $contactKnowing The contact
     * @param ContactAcquaintanceLevel  $minLevel       The minimum level of acquaintance the contact should have
     * @param ContactAcquaintanceLevel  $maxLevel       The maximum level of acquaintance the contact should have
     *
     * @return \ORM_Iterator <Contact>
     */
    public function selectKnownBy($contactKnowing, ContactAcquaintanceLevel $minLevel = null, ContactAcquaintanceLevel $maxLevel = null)
    {
        $set = $this->App()->ContactAcquaintanceSet();
        $set->level();
        
        if ($contactKnowing instanceof Contact) {
            $contactKnowing = $contactKnowing->id;
        }
        
        $criteria = $set->contactKnowing->is($contactKnowing);
        if (isset($minLevel)) {
            $criteria = $criteria->_AND_($set->level->rank->greaterThanOrEqual($minLevel->rank));
        }
        if (isset($maxLevel)) {
            $criteria = $criteria->_AND_($set->level->rank->lessThanOrEqual($maxLevel->rank));
        }
        return $set->select($criteria)->orderAsc($set->level);
    }
    
    public function isUpdatable()
    {
        if(bab_isUserAdministrator()){
            return $this->all();
        }
        
        $currentContact = $this->App()->ContactSet()->getCurrentContact();
        if($currentContact){
            return $this->contactKnowing->is($currentContact->id)->_OR_($this->contactKnown->is($currentContact->id));
        }
        
        return $this->none();
    }
    
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class ContactAcquaintanceBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ContactAcquaintanceAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}