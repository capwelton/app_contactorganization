<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\Set;

include_once 'base.php';

/**
 * A Contact is a person
 *
 * @property string                     $firstname
 * @property string                     $lastname
 * @property string                     $title
 * @property ORM_StringField            $initials       Initials
 * @property Address                    $address
 * @property int                        $user           The (optional) ovidentia user associated to this contact.
 * @property string                     $phone
 * @property string                     $mobile
 * @property string                     $fax
 * @property string                     $email
 * @property Contact            $responsible    The collaborator responsible for this contact.
 * @property Organization               $responsibleOrganization
 *
 * @method \Func_App     App()
 */
class Contact extends \app_TraceableRecord
{
    const TITLE_MR = 1;
    const TITLE_MRS = 2;
    
    const USER_TYPE_NO_ACCOUNT = 1;
    const USER_TYPE_EXISTING_ACCOUNT = 2;
    const USER_TYPE_NEW_ACCOUNT = 3;
    
    /**
     * Contact titles
     * @return array
     */
    public static function getTitles()
    {
        return array(
            self::TITLE_MR => 'Mr.',
            self::TITLE_MRS => 'Mrs'
        );
    }

    /**
     * Contact titles in full
     * @return array
     */
    public static function getFullTitles()
    {
        return array(
            self::TITLE_MR => 'Mister',
            self::TITLE_MRS => 'Madam'
        );
    }


    const SUBFOLDER = 'contacts';
    const PHOTOSUBFOLDER = 'photo';
    const ATTACHMENTSSUBFOLDER = 'attachments';

    /**
     * if true, a contact has been saved into an ovidentia directory entry
     * @var bool
     */
    private static $directory_update = false;



    /**
     * @return string
     */
    public function getFullName()
    {
        return bab_composeUserName($this->firstname, $this->lastname);
    }


    /**
     * @return string
     */
    public function getFullNameWithTitle()
    {
        return $this->getParentSet()->title->output($this->title) . ' ' . bab_composeUserName($this->firstname, $this->lastname);
    }


    /**
     * overload default record title
     * @return string
     */
    public function getRecordTitle()
    {
        return $this->getFullName();
    }



    /**
     * Computes the initials string for the user.
     *
     * @param number $initialsSize  The expected size of the initials string
     * @return string
     */
    public function computeInitials($initialsSize = 3)
    {
        $initials = '';
        $parts = explode(' ', str_replace('-', ' ', mb_strtoupper($this->firstname)));
        foreach ($parts as $part) {
            $lastPart = $part;
            $initials .= mb_substr($part, 0, 1);
            if (mb_strlen($initials) >= 2) {
                break;
            }
        }
        $parts = explode(' ', str_replace('-', ' ', mb_strtoupper($this->lastname)));
        foreach ($parts as $part) {
            $lastPart = $part;
            $initials .= mb_substr($part, 0, 1);
            if (mb_strlen($initials) >= $initialsSize) {
                break;
            }
        }

        if (mb_strlen($initials) < $initialsSize) {
            $initials .= mb_substr($lastPart, 1, $initialsSize - mb_strlen($initials));
        }

        return $initials;
    }


    /**
     * Get the link beetween organization and contact record
     * default beahavior : link is set manually as the main link
     *
     * @return ContactOrganization | null
     */
    public function getMainContactOrganization()
    {
        $App = $this->App();

        $contactOrganizationSet = $App->ContactOrganizationSet();

        $contactOrganizations = $contactOrganizationSet->select(
            $contactOrganizationSet->all(
                $contactOrganizationSet->contact->is($this->id),
                $contactOrganizationSet->isActive()
            )
        );

        $contactOrganizations->orderAsc($contactOrganizationSet->contactRank);
        $contactOrganizations->orderAsc($contactOrganizationSet->id);

        foreach ($contactOrganizations as $contactOrganization) {
            return $contactOrganization;
        }

        return null;
    }
    
    /**
     *
     * @return string
     */
    public function getMainPosition()
    {
        $record = $this->getMainContactOrganization();
        if (null === $record) {
            return '';
        }
        return $record->getMainPosition();
    }
    
    
    /**
     * @return string
     */
    public function getMainEmail()
    {
        $mainContactOrganization = $this->getMainContactOrganization();
        if (isset($mainContactOrganization)) {
            return $mainContactOrganization->email;
        }
        return $this->email;
    }
    
    
    /**
     * @return ContactPhone[]
     */
    public function getPhoneRecords()
    {
        $mainContactOrganization = $this->getMainContactOrganization();
        if ($mainContactOrganization) {
            return $mainContactOrganization->getPhoneRecords();
        }
        
        $App = $this->App();
        $contactPhoneSet = $App->ContactPhoneSet();
        
        $contactPhones = $contactPhoneSet->select(
            $contactPhoneSet->contact->is($this->id)
        );
        
        $contactPhones->orderAsc($contactPhoneSet->rank);
        $contactPhones->orderAsc($contactPhoneSet->id);
        
        return $contactPhones;
    }
    
    
    /**
     *
     * @return ContactPhone|null
     */
    public function getMainPhoneRecord()
    {      
        $contactPhones = $this->getPhoneRecords();
        
        foreach ($contactPhones as $contactPhone) {
            return $contactPhone;
        }
        
        return null;
    }

    
    /**
     * @return string
     */
    public function getMainPhone()
    {
        $mainPhoneRecord = $this->getMainPhoneRecord();
        if (isset($mainPhoneRecord)) {
            return $mainPhoneRecord->phone;
        }
        
        return null;
    }
    
    public function getMainPersonalPhoneRecord()
    {
        $App = $this->App();
        $contactPhoneSet = $App->ContactPhoneSet();
        
        $contactPhones = $contactPhoneSet->select(
            $contactPhoneSet->contact->is($this->id)
            ->_AND_($contactPhoneSet->isPersonal())
        );
        
        $contactPhones->orderAsc($contactPhoneSet->rank);
        $contactPhones->orderAsc($contactPhoneSet->id);
        
        foreach ($contactPhones as $contactPhone) {
            return $contactPhone;
        }
        
        return null;
    }
    
    /**
     * @return string
     */
    public function getMainPersonalPhone()
    {
        $mainPhoneRecord = $this->getMainPersonalPhoneRecord();
        if(isset($mainPhoneRecord)){
            return $mainPhoneRecord->phone;
        }
        
        return null;
    }
    
    /**
     * @return string
     */
    public function getMainMobilePhone()
    {
        return $this->getMainPhone();
    }
    
    /**
     * @return string
     */
    public function getMainFax()
    {
        return $this->fax;
    }
    
    /**
     * @return Address|null
     */
    public function getMainAddress()
    {
        if (!$this->address) {
            return null;
        }
        $App = $this->App();
        
        $App->includeAddressSet();
        $App->includeCountrySet();
        if ($this->address instanceof Address) {
            return $this->address;
        }
        
        $addressSet = $App->AddressSet();
        $addressSet->join('country');
        
        return $addressSet->get($this->address);
    }
    

    public function updateMainAddress()
    {
        $App = $this->App();
        $addressSet = $App->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this);

        if ($this->address instanceof Address) {
            $thisAddressId = $this->address->id;
        } else {
            $thisAddressId = $this->address;
        }

        $firstAddressId = null;
        $firstAddressRecord = null;
        $thisAddressOk = false;
        foreach ($addressLinks as $addressLink) {
            if ($addressLink->targetId->id == $thisAddressId) {
                // We found the current main address in the linked addresses.
                $thisAddressOk = true;
                break;
            }
            if (!isset($firstAddressId)) {
                $firstAddressId = $addressLink->targetId->id;
                $firstAddressRecord = $addressLink->targetId;
            }
        }

        if (!$thisAddressOk) {
            // If the current main address is not part of the linked addresses,
            // we replace it with the first linked address.
            if ($this->address instanceof Address) {
                $this->address = $firstAddressRecord;
            } else {
                $this->address = $firstAddressId;
            }

            $this->save();
        }
    }



    /**
     * Adds a typed address to the contact.
     *
     * @param Address $address
     * @param string $type
     */
    public function addAddress(Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }

        if (!$address->isLinkedTo($this, $type)) {
            $address->linkTo($this, $type);
            $this->updateMainAddress();
        }
        return $this;
    }


    /**
     * Removes a typed address from the contact.
     *
     * @param Address $address
     * @param string $type
     *
     * @return self
     */
    public function removeAddress(Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }
        $address->unlinkFrom($this, $type);

        $this->updateMainAddress();
        return $this;
    }



    /**
     * Update the type of a linked address for the contact.
     *
     * @param Address $address
     * @param string $previousAddressType
     * @param string $newAddressType
     *
     * @return self
     */
    public function updateAddress(Address $address, $previousAddressType, $newAddressType)
    {
        if (!isset($this->id)) {
            return;
        }

        $App = $this->App();
        $addressSet = $App->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this, $previousAddressType);
        foreach ($addressLinks as $addressLink) {
            $addressLink->type = $newAddressType;
            $addressLink->save();
        }

        return $this;
    }



    /**
     *
     * @param string $type
     *
     * @return array  Eg. array('Type 1' => array(address1), 'Type 2' => array(address2, address3 ), ...)
     */
    public function getAddresses($type = null)
    {
        $App = $this->App();

        $addressSet = $App->AddressSet();

        $addresses = $addressSet->selectLinkedTo($this, $type);

        $typedAddresses = array();

        foreach ($addresses as $address) {
            $type = $address->type;
            if (!isset($typedAddresses[$type])) {
                $typedAddresses['' . $type] = array();
            }
            $typedAddresses['' . $type][] = $address->targetId;
        }


        return $typedAddresses;
    }



    /**
     * Get the upload path for files related to this contact.
     *
     * @return \bab_Path
     */
    public function uploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

        $path = $this->App()->getUploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }


    /**
     * Get the upload path for attached files.
     *
     * @return \bab_Path
     */
    public function getAttachmentsUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::ATTACHMENTSSUBFOLDER);
        return $path;
    }

    /**
     * Get the upload path for the photo file
     * this method can be used with the image picker widget
     *
     * @return \bab_Path
     */
    public function getPhotoUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::PHOTOSUBFOLDER);
        return $path;
    }




    /**
     * Return the full path of the photo file
     * The photo field contain a file name or a description of the photo
     * this method return null if there is no photo to display
     *
     * @return \bab_Path | null
     */
    public function getPhotoPath()
    {
        $uploadpath = $this->getPhotoUploadPath();
        if (!isset($uploadpath)) {
            return null;
        }
        $W = bab_Widgets();
        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);
        if (!isset($uploaded)) {
            return null;
        }
        foreach($uploaded as $file) {
            return $file->getFilePath();
        }
        return null;
    }



    /**
     * Update the main position field in the contact record according
     * to the main contact organization.
     *
     * @return bool True if contact was changed.
     */
    public function resetMainPosition()
    {
        $contactOrganization = $this->getMainContactOrganization();

        if (!isset($contactOrganization)) {
            $this->mainPosition = 0;
            return true;
        } elseif ($contactOrganization->id != $this->mainPosition) {
            $this->mainPosition = $contactOrganization->id;
            return true;
        }
        return false;
    }



    /**
     * User account creation and password modification
     *
     * @throws ContactUserException
     * @param	Array	$data	posted values from ContactEditor
     */
    public function manageUserAccount($data)
    {
        $App = $this->App();

        if (isset($data['user_extra'])) {
            self::$directory_update = true;
            if ($this->user && isset($data['user_extra']['disabled'])) {
                // change password or disable account
                $new_password = trim($data['user_extra']['new_password']);
                $infos = array(
                    'disabled' => $data['user_extra']['disabled']
                );

                if (isset($data['user_extra']['nickname'])) {
                    $infos['nickname'] = trim($data['user_extra']['nickname']);
                }
                if ($new_password) {
                    $infos['password'] = $new_password;
                }
                
                $error = '';
                bab_updateUserById($this->user, $infos, $error);
                if ($error) {
                    throw new ContactUserException($error);
                }

                $this->updateUser();

                // Notify the user about the new password
                if ($new_password) {
                    $this->sendUserUpdateNotification($new_password);
                }
            } 
            elseif ((int) $data['usertype'] === self::USER_TYPE_NEW_ACCOUNT) {
                $nickname 	= $data['user_extra']['nickname'];
                if (!empty($nickname)) {
                    // create user account
                    $user = $this->createUser($nickname, $data['user_extra']['password'], $data['user_extra']['notify'], true);
                    if ($user) {
                        $this->user = $user;
                        $this->save();
                    }
                }
            }
        }
    }
    
    protected function sendUserUpdateNotification($newPassword)
    {
//         $notification = $App->Notify()->myContact_newPassword($this, $new_password);
//         $notification->send();
    }


    /**
     * Update user password
     * @param	string	$newPassword
     * @throws  ContactUserException
     * @return bool
     */
    public function updatePassword($newPassword)
    {
        $App = $this->App();

        if (!$this->user) {
            throw new ContactUserException($App->translate('Missing user account'));
        }

        $info = array(
            'password' => $newPassword
        );
        $error = '';
        bab_updateUserById($this->user, $info, $error);

        if ($error) {
            throw new ContactUserException($error);
        }

        return true;
    }


    /**
     * Update user nickname
     * @param string $nickname
     * @throws  ContactUserException
     * @return bool
     */
    public function updateNickname($nickname)
    {
        $App = $this->App();
        if (!$this->user) {
            throw new ContactUserException($App->translate('Missing user account'));
        }

        $info = array(
            'nickname' => $nickname
        );
        $error = '';
        bab_updateUserById($this->user, $info, $error);

        if ($error) {
            throw new ContactUserException($error);
        }

        return true;
    }



    /**
     * Create ovidentia account and return user id
     *
     * @throw ContactUserException
     *
     * @param	string	$nickname
     * @param	string	$password
     * @param	bool	$notify				1|0 as string or bool
     * @param	bool	$notify_password	1|0 as string or bool
     *
     * @return int
     */
    public function createUser($nickname, $password, $notify = false, $notify_password = false)
    {
        $error = '';

        // add a middlename to allow firstname/lastname duplicates
        $middlename = $this->email;

        $id_user = bab_registerUser(
            empty($this->firstname) ? ' ' : $this->firstname,
            $this->lastname,
            $middlename,
            $this->email,
            $nickname,
            $password,
            $password,
            1,
            $error
        );


        if (!empty($error)) {
            $e = new ContactUserException($error);
            $e->redirect = false;
            throw $e;
            return false;
        }

        if ($id_user) {
            $infos = $this->getUserInfos();
            if ($infos) {
                bab_updateUserById($id_user, $infos, $error);

                if ($error) {
                    $oError = new ContactUserException($error);
                    $oError->redirect = false;
                    throw $oError;
                    return $id_user;
                }
            }

            if ($notify) {
                if ($notify_password) {
                    $this->createUserNotify($nickname, $password);
                } 
                else {
                    $this->createUserNotify($nickname, null);
                }
            }

            return $id_user;
        }

        return false;
    }




    /**
     * Update ovidentia user account from record if there is a user id in the user field
     *
     * @throw ContactUserException
     *
     * @return bool
     */
    public function updateUser()
    {
        self::$directory_update = true;

        if ($this->user) {
            $infos = $this->getUserInfos();
            $error = null;
            $return = bab_updateUserById($this->user, $infos, $error);
            if ($error) {
                throw new ContactUserException($error);
            }

            return $return;
        }

        return false;
    }




    /**
     * Update contact from user infos
     * @see self::onUserModified()
     * @param	Array	$infos
     */
    protected function updateFromUserInfos(array $infos)
    {
        $App = $this->App();
        $mapSet = $App->ContactDirectoryMapSet();
        $res = $mapSet->select($mapSet->type->is('contact')->_AND_($mapSet->contactfield->startsWith('Contact.')));

        foreach($res as $record)
        {
            $contactfield = explode('.',$record->contactfield);
            bab_debug($contactfield);

            $value = $infos[$record->userfield]['value'];

            if (!isset($value) || '' === $value)
            {
                continue;
            }

            $fieldname = array_shift($contactfield);

            switch($fieldname)
            {
                case 'id':
                case 'user':
                    // never update these fields
                    break;
                case 'address':
                    $fieldname = array_shift($contactfield);
                    if ('country' === $fieldname)
                    {
                        $cSet = $App->CountrySet();
                        $res = $cSet->select($cSet->name_en->like($value)->_OR_($cSet->name_fr->like($value)));
                        $value = null;
                        foreach($res as $country)
                        {
                            $value = $country->id;
                        }
                    }

                    $this->address->$fieldname = $value;
                    break;
                default:
                    $this->$fieldname = $value;
                    break;
            }
        }
    }



    /**
     * Method used to get user info for the bab_updateUserById function
     *
     * @return array
     */
    protected function getUserInfos()
    {
        $output = array();

        $output['email'] 		= $this->email;
        $output['givenname'] 	= empty($this->firstname) ? ' ' : $this->firstname;
        $output['sn'] 			= $this->lastname;

        if ($path = $this->getPhotoPath()) {
            $output['jpegphoto'] = \bab_fileHandler::copy($path->toString());
        }
        else {
            $output['jpegphoto'] = '';
        }

        $output['mobile'] 		= $this->mobile;
        $output['htel']			= $this->phone;


        if ($contactOrganization = $this->getMainContactOrganization()) {
            /*@var $contactOrganization ContactOrganization */
            if($organization = $contactOrganization->getMainOrganization()){
                $output += $organization->getUserOrganizationInfos();
            }

        } 
        else {
            $output['btel']				= '';
            $output['bfax']				= '';
            $output['organisationname']	= '';
            $output['bstreetaddress']	= '';
            $output['bcity']			= '';
            $output['bpostalcode']		= '';
            $output['bstate']			= '';
            $output['bcountry']			= '';
        }

        if ($address = $this->address()) {
            /* @var $address Address */

            $output['hstreetaddress']	= $address->street;
            $output['hcity']			= $address->city;

            if ($address->cityComplement) {
                $output['hcity'] .= ' '.$address->cityComplement;
            }

            $output['hpostalcode']	= $address->postalCode;
            $output['hstate']		= $address->state;

            if ($country = $address->getCountry()) {
                /* @var $orgcountry Country */
                $output['hcountry']	= $country->getName();
            }
            else {
                $output['hcountry']	= '';
            }

        } 
        else {
            $output['hstreetaddress']	= '';
            $output['hcity']			= '';
            $output['hpostalcode']		= '';
            $output['hstate']			= '';
            $output['hcountry']			= '';
        }

        return $output;
    }

    /**
     * Notify the user about account creation
     *
     * @param	string			$nickname
     * @param	string | null	$password		set password to null to notify without password
     *
     *
     * @return bool
     */
    protected function createUserNotify($nickname, $password)
    {
        $email = $this->email;
        if (null === $email) {
            return;
        }

        return bab_registerUserNotify($this->getFullName(), $email, $nickname, $password);
    }

    /**
     * Test if the user associated to contact is currently connected to portal
     * return the last hit timestamp if user is logged in
     *
     * @return false | int	timestamp
     */
    public function isOnline()
    {
        if (!$this->user) {
            return false;
        }

        static $sessions = null;
        if (null === $sessions) {
            $sessions = bab_getActiveSessions($this->user);
        }

        if (0 === count($sessions)) {
            return false;
        }

        // additional verification because the id_user parameter need a version 7.3.0 and may be not taken
        foreach($sessions as $arr) {
            if ($this->user === $arr['id_user']) {
                return (int) $arr['last_hit_date'];
            }
        }

        return false;
    }

    public function addFollowedItems($ref){
        if(!$this->isFollowed($ref)){
            $foSet = $this->App()->FollowObjectSet();
            $fo = $foSet->newRecord();
            $fo->contact = $this->id;
            $fo->object = $ref;
            $fo->save();
        }
    }

    public function isFollowed($ref){
        $foSet = $this->App()->FollowObjectSet();
        $fos = $foSet->select(
            $foSet->all(
                $foSet->contact->is($this->id),
                $foSet->object->is($ref)
            )
        );
        return count($fos)>0;
    }

    public function removeFollowedItems($ref){
        $foSet = $this->App()->FollowObjectSet();
        $fos = $foSet->select(
            $foSet->all(
                $foSet->contact->is($this->id),
                $foSet->object->is($ref)
            )
        );
        foreach($fos as $f){
            $f->delete();
        }
        return true;
    }

    /**
     * Test if the associated user is valid (can log-in)
     * @return bool
     */
    public function isUserValid()
    {
        if (!$this->user) {
            return false;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';
        return \bab_userInfos::isValid($this->user);
    }

    /**
     * Checks if the contact can be granted access rights by the current user.
     *
     * @return bool
     */
    public function isGrantableAccess()
    {
        $set = $this->getParentSet();
        return $set->select($set->isGrantableAccess()->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * Marks the associated user account as confirmed.
     *
     * @throws \app_Exception
     * @return bool
     */
    public function confirm()
    {
        $infos = array('is_confirmed' => 1);
        $error = null;
        $return = bab_updateUserById($this->user, $infos, $error);

        if ($error) {
            throw new \app_Exception($error);
        }

        return $return;
    }

    /**
     *
     */
    public function renameRandomly()
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $Pop = \bab_functionality::get('Populator');
        if (!($Pop instanceof \bab_functionality)) {
            return 0;
        }
        $userinfo = $Pop->getRandomUserInfo();
        $this->firstname = $userinfo['givenname'];
        $this->lastname = $userinfo['sn'];
        $this->title = $userinfo['gender'] == 'M' ? self::TITLE_MR : self::TITLE_MRS;
        $this->phone = $userinfo['btel'];
        $this->email = $userinfo['email'];
        $this->save();

        $this->updateUser();
    }



    /**
     * @param \Widget_CsvRow $row
     * @return number
     */
    public function import(\Widget_CsvRow $row)
    {
        $up_prop = 0;

        if (isset($row->lastname)) {
            $up_prop += $this->importProperty('lastname', $row->lastname);
        }

        if (isset($row->firstname)) {
            $up_prop += $this->importProperty('firstname', $row->firstname);
        }

        if (isset($row->email)) {
            $up_prop += $this->importProperty('email', $row->email);
        }

        if (isset($row->phone)) {
            $up_prop += $this->importProperty('phone', $row->phone);
        }

        if (isset($row->mobile)) {
            $up_prop += $this->importProperty('mobile', $row->mobile);
        }

        if (isset($row->fax)) {
            $up_prop += $this->importProperty('fax', $row->fax);
        }

        if (isset($row->glpiId)) {
            $up_prop++;
            $this->glpiId = $row->glpiId;
        }

        return $up_prop;
    }


    /**
     * Update properties with CSV row
     *
     * @return 	int
     */
    public function importLinked(\Widget_CsvRow $row)
    {
        $App = $this->App();

        $up_prop = 0;

        if ($this->id == 0) {
            return 0;
        }

        static $organizationSet = null;
        if (!isset($organizationSet)) {
            $organizationSet = $App->OrganizationSet();
        }
        static $contactOrganizationSet = null;
        if (!isset($contactOrganizationSet)) {
            $contactOrganizationSet = $App->ContactOrganizationSet();
        }


        if (isset($row->entity_id)) {
            $organization = $organizationSet->get($organizationSet->glpiNumber->is($row->entity_id));
            if ($organization) {
                $contactOrganization = $contactOrganizationSet->newRecord();
                $contactOrganization->contact = $this->id;
                $contactOrganization->organization = $organization->id;
                $contactOrganization->save();
            }
        }

        return $up_prop;
    }


    /**
     * {@inheritDoc}
     * @see \app_TraceableRecord::save()
     */
    public function save($noTrace = false)
    {
        if ($this->initials === '') {
            $this->initials = $this->computeInitials();
        }
        return parent::save($noTrace);
    }
    
    /**
     * Returns the contacts knowing this contact.
     *
     * @return \ORM_Iterator <Contact>
     */
    public function selectContactsKnowing()
    {
        $acquaintanceSet = $this->App()->ContactAcquaintanceSet();
        return $acquaintanceSet->selectKnowing($this);
    }
    
    /**
     * Returns the contacts known
     *
     * @return \ORM_Iterator <Contact>
     */
    public function selectContactsKnown()
    {
        $acquaintanceSet = $this->App()->ContactAcquaintanceSet();
        return $acquaintanceSet->selectKnownBy($this);
    }
    
    public function getTrigram()
    {
        $firstname = substr($this->firstname, 0, 1);
        $lastname = substr($this->lastname, 0, 2);
        
        return strtoupper($firstname.$lastname);
    }
    
    public function getOrganizations()
    {
        $App = $this->App();
        
        $contactOrganizationSet = $App->ContactOrganizationSet();
        $organizationSet = $App->OrganizationSet();
        
        return $organizationSet->select(
            $organizationSet->id->in(
                $contactOrganizationSet->all(
                    $contactOrganizationSet->contact->is($this->id),
                    $contactOrganizationSet->isActive()
                ),
                'organization'
            )
        );
        
    }
    
    public function isInternal()
    {
        $App = $this->App();
        
        $contactOrganizationSet = $App->ContactOrganizationSet();
        $organizationSet = $App->OrganizationSet();
        $organizationTypeOrganizationSet = $App->OrganizationTypeOrganizationSet();
        $organizationTypeOrganizationSet->organizationType();
        
        return $organizationSet->select(
            $organizationSet->all(
                array(
                    $organizationSet->id->in(
                        $contactOrganizationSet->all(
                            $contactOrganizationSet->contact->is($this->id),
                            $contactOrganizationSet->isActive()
                        ),
                        'organization'
                    ),
                    $organizationSet->id->in(
                        $organizationTypeOrganizationSet->all(
                            $organizationTypeOrganizationSet->organizationType->profile->is(OrganizationType::PROFILE_INTERNAL)
                        ),
                        'organization'
                    )
                )
            )
        )->count() > 0;
    }
    
    /**
     * Get the vCard object corresponding to the contact
     *
     * @return ContactVCard
     */
    public function vCard()
    {
        $vCard = new ContactVCard();
        
        if ('0000-00-00 00:00:00' === $this->modifiedOn) {
            $vCard->setRev($this->createdOn);
        } else {
            $vCard->setRev($this->modifiedOn);
        }
        
        $vCard->addProperty('UID', $this->uuid);
        $vCard->setName($this->lastname, $this->firstname);
        
        if ($title = $this->getMainPosition()) {
            $vCard->addProperty('TITLE', $title);
        }
        
        $vCard->addTel('WORK', $this->getMainPhone());
        $vCard->addEmail('WORK', $this->getMainEmail());
        
        
        if ($contactOrganization = $this->getMainContactOrganization()) {
            if($organization = $contactOrganization->organization()){
                $vCard->setOrganization($organization->name);
                if ($workaddress = $organization->getMainAddress()) {
                    $country = '';
                    if($addressCountry = $workaddress->country()){
                        $country = $addressCountry->getName();
                    }
                    $vCard->addAddress('WORK', $workaddress->street, null, $workaddress->postalCode, $workaddress->city, $workaddress->state, $country);
                }
            }
        }
        
        if ($homeaddress = $this->getMainAddress()) {
            $country = '';
            if($addressCountry = $homeaddress->country()){
                $country = $addressCountry->getName();
            }
            $vCard->addAddress('HOME', $homeaddress->street, null, $homeaddress->postalCode, $homeaddress->city, $homeaddress->state, $country);
        }
        
        return $vCard;
    }
    
}






/**
 * Exception on errors with the user creation or modification
 */
class ContactUserException extends \app_SaveException {}
