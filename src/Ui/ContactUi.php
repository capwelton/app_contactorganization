<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Contact;


class ContactUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * Generates and returns an image representing the specified contact.
     *
     * @param Contact   $contact	The contact to generate a photo of.
     * @param int     		        $width		Width in px.
     * @param int     		        $height		Height in px.
     * @param bool    		        $border		True to add a 1px light-grey border around the image.
     */
    public function ContactAvatar(Contact $contact = null, $width = 28, $height = 28, $border = false)
    {
        $W = bab_Widgets();
        
        if (!isset($contact)) {
            $image = $W->Html('');
        }
        
        if (!isset($image)) {
            $image = $this->ContactPhoto($contact, $width, $height, $border);
        }
        
        if (!isset($image)) {
            $image = $this->ContactInitialsImage($contact, $width, $height);
        }
        
        if (!isset($image)) {
            $image = $W->Html('');
        }
        
        return $image;
    }
    
    /**
     * @param Contact $contact
     * @return ContactCardFrame
     */
    public function ContactCardFrame(Contact $contact)
    {
        return new ContactCardFrame($this->app, $contact);
    }
    
    /**
     * Contact card view
     * @return ContactCardsView
     */
    public function ContactCardsView()
    {
        return new ContactCardsView($this->App());
    }
    
    public function ContactFullFrame(Contact $contact)
    {
        $fullFrame = new ContactRecordView($this->app);
        $fullFrame->setRecord($contact);
        return $fullFrame;
    }
    
    public function editor(Contact $contact = null)
    {
        return $this->ContactEditor($contact);
    }
    
    /**
     * Contact editor
     * @return ContactEditor
     */
    public function ContactEditor(Contact $contact = null)
    {
        return new ContactEditor($this->app, $contact);
    }
    
    public function ContactInitialsImage(Contact $contact, $width, $height)
    {
        $W = bab_Widgets();
        
        $initials = $contact->initials;
        
        $color = $W->Color();
        $color->setHueFromString($contact->getFullName(), 0.7, 0.5);
        
        $image = $W->Html('<div style="font-weight: bold; color: #fff; font-size: ' . bab_toHtml($height / 2.5) . 'px; display:inline-block; vertical-align: middle">' . bab_toHtml($initials) . '</div><div style="display:inline-block; vertical-align: middle; height: 100%"></div>')->setCanvasOptions(\Widget_Item::options()->backgroundColor('#' . $color->getHexa())->width($width, 'px')->height($height, 'px'));
        
        $image->setTitle($contact->getFullName());
        $image->addClass('app-contact-image app-element-image small widget-align-center');
        
        return $image;
    }
    
    /**
     * Generates and returns an image representing the specified contact.
     *
     * @param Contact	        $contact	The contact to generate a photo of.
     * @param int     		    $width		Width in px.
     * @param int     		    $height		Height in px.
     * @param bool    		    $border		True to add a 1px light-grey border around the image.
     
     * @return \Widget_Image|null
     */
    public function ContactPhoto(Contact $contact = null, $width = 32, $height = 32, $border = false)
    {
        $W = bab_Widgets();
        
        /* @var $T \Func_Thumbnailer */
        $T = \bab_functionality::get('Thumbnailer');
        
        if (!$T) {
            return null;
        }
        
        $path = $contact->getPhotoPath();
        if (!isset($path)) {
            return null;
        }
        
        $image = $W->ImageThumbnail($path)->setThumbnailSize($width, $height);
        
        $image->setResizeMode(\Func_Thumbnailer::CROP_CENTER);
        
        $image->setTitle($contact->getFullName());
        
        $image->addClass('crm-contact-image crm-element-image small');
        
        return $image;
    }
    
    /**
     * Contact editor
     * @return ContactSectionEditor
     */
    public function ContactSectionEditor(Contact $contact = null)
    {
        return new ContactSectionEditor($this->app, $contact);
    }
    
    /**
     * Contact table view
     * @return ContactTableView
     */
    public function ContactTableView()
    {
        return new ContactTableView($this->app);
    }
    
    /**
     * @return SuggestContact
     */
    public function SuggestContact($id = null, $exludedContacts = array())
    {
        $suggest = new SuggestContact($this->app, $id);
        $suggest->setDataSource($this->app->Controller()->Contact()->suggestContact(null, null, $exludedContacts));
        
        return $suggest;
    }
    
    public function tableView()
    {
        return $this->ContactTableView();
    }
}
