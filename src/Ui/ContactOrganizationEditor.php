<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactOrganization;

/**
 *  @method \Func_App    App()
 */
class ContactOrganizationEditor extends \app_Editor
{
    protected $forOrganizationMode = false;
    
    protected $forContactMode = false;
    
    /**
     * @param Func_App App
     * @param ContactOrganization $contactOrganization
     * @param int $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, ContactOrganization $contactOrganization = null, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($App, $id, $layout);
    }
    
    
    public function setForOrganizationMode()
    {
        $this->forOrganizationMode = true;
    }
    
    public function setForContactMode()
    {
        $this->forContactMode = true;
    }
    
    protected function contact()
    {
        if ($this->forContactMode) {
            $W = bab_Widgets();
            $contactFormItem = $W->Items(
                $W->Label($this->record->contact()->getFullName()),
                $W->Hidden()->setName('contact')
            );
        } else {
            $contactFormItem = $this->App()->Controller()->ContactOrganization(false)->SuggestContact($this->record->id);
        }
        
        return $contactFormItem;
    }
    
    protected function organization()
    {
        if ($this->forOrganizationMode) {
            $W = bab_Widgets();
            $organizationFormItem = $W->Items(
                $W->Label($this->record->organization()->name),
                $W->Hidden()->setName('organization')
            );
        } else {
            $App = $this->App();
            $organizationFormItem = $this->labelledField(
                $App->translate('Organization'),
                $this->suggestOrganization = $App->Ui()->SuggestOrganization()
                ->addClass('widget-100pc'),
                'organization'
            );
        }
        
        return $organizationFormItem;
    }
    
    protected function position()
    {
        $App = $this->App();
        $positionFormItem = $this->labelledField(
            $App->translate('Position'),
            $this->suggestOrganization = $App->Ui()->SuggestPosition()
            ->addClass('widget-100pc'),
            'position'
        );
        
        return $positionFormItem;
    }
    
    
    protected function service()
    {
        $App = $this->App();
        $positionFormItem = $this->labelledField(
            $App->translate('Service'),
            $this->suggestService = $App->Ui()->SuggestPosition()
            ->addClass('widget-100pc'),
            'service'
        );
        
        return $positionFormItem;
    }
    
    protected function type()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $typeSet = $App->ContactOrganizationTypeSet();
        $types = $typeSet->select()->orderAsc($typeSet->name);
        
        $widget = $W->Select();
        $widget->setTitle($App->translate('Type'));
        $options = array(0 => '');
        foreach ($types as $type) {
            $options[$type->id] = $type->name;
        }
        $widget->setOptions($options);
        
        return $this->labelledField(
            $App->translate('Type'),
            $widget
            ->addClass('widget-100pc'),
            'type'
        );
    }
    
    protected function email()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $this->labelledField(
            $App->translate('Email'),
            $W->EmailLineEdit()
            ->setPlaceHolder($this->record->contact()->email)
            ->addClass('widget-100pc'),
            'email'
        );
    }
    
    /**
     * Add fields into form
     * @return self
     */
    public function prependFields()
    {
        return $this;
    }
    
    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();
        
        $header = $W->VBoxItems();
        if ($this->forContactMode) {
            $this->addItem($this->contact());
        } else {
            $header->addItem($this->contact());
        }
        if ($this->forOrganizationMode) {
            $this->addItem($this->organization());
        } else {
            $header->addItem($this->organization());
        }
        
        $this->addItem($header);
        $this->addItem($this->position());
        $this->addItem($this->service());
        $this->addItem($this->type());
        $this->addItem($this->email());
        $this->addItem(
            $W->HBoxItems(
                $this->start(),
                $this->end()
            )->setHorizontalSpacing(1, 'em')
        );
        
        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
        }
        return $this;
    }
    
    public function start()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $this->labelledField(
            $App->translate('Since'),
            $W->DatePicker()
            ->addClass('widget-100pc'),
            'start'
        );
    }
    
    public function end()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $this->labelledField(
            $App->translate('Until'),
            $W->DatePicker()
            ->addClass('widget-100pc'),
            'end'
        );
    }
}
