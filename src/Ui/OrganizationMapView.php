<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

/**
 *
 * @method \Func_App App()
 */
class OrganizationMapView extends OrganizationModelView
{

    /**
     *
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setVerticalSpacing(1, 'em');

        $this->setLayout($layout);

        $this->setPageLength(10000);
    }


    public function getDisplaySubTotalRowCheckbox()
    {
        return null;
    }


    public function getFilterCriteria($filter = null)
    {
        $recordSet = $this->getRecordSet();

        $conditions = array(
            parent::getFilterCriteria($filter),
            $recordSet->address()->latitude->isNot('')
        );

        return $recordSet->all($conditions);
    }

    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(\ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see \Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::handleRow()
     */
    protected function handleRow(\ORM_Record $record, $row)
    {
        $address = $record->address();

        if ($address->latitude === '-' || $address->longitude === '-') {
            return true;
        }

        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();

        $card = $Ui->OrganizationCardFrame($record);
        $title = $record->name;

        $this->map->addMarker(
            $address->latitude,
            $address->longitude,
            $title,
            $W->VBoxItems(
                $card->setId($record->getRef()),
                $W->Link(
                    $App->translate('Show details'), 
                    $App->Controller()->Organization()->setHighlightedRecords(array($record->id))
                )->setAjaxAction()
            )->addClass('widget-30em')
        );

        return true;
    }

    /**
     * Fills the table view with data from the data source.
     */
    protected function init()
    {
        $iterator = $this->iterator;

        if (!isset($iterator)) {
            return;
        }

        $set = $iterator->getSet();

        $this->initSortField($set, $iterator);
        $this->initColumns($set);
        $this->initHeaderRow($set);

        $this->addSection('body', null, 'widget-table-body');
        $this->setCurrentSection('body');

        $nbRows = $iterator->count();

        if (isset($this->pageLength)) {
            if ($this->currentPage > floor($nbRows / $this->pageLength)) {
                $this->currentPage = 0;
            }
        }

        $startRow = isset($this->pageLength) ? $this->currentPage * $this->pageLength : 0;
        $iterator->seek($startRow);

        $row = 0;

        $currentGroupValue = null;

        while ($iterator->valid() && (!isset($this->pageLength) || $row < $this->pageLength)) {

            if (isset($this->limit) && $row >= $this->limit) {
                break;
            }

            $record = $iterator->current();
            if ($this->isGrouping() && isset($this->sortField)) {
                $newGroupValue = self::getRecordFieldValue($record, $this->sortField);
                if ($newGroupValue !== $currentGroupValue) {
                    $this->addSection($newGroupValue, $newGroupValue);
                    $this->setCurrentSection($newGroupValue);
                    $currentGroupValue = $newGroupValue;
                }
            }
            $iterator->next();

            $row = $this->initRowNumber($record, $row);
            $record = $this->initRow($record, $row);
            if ($this->handleRow($record, $row)) {
                $row++;
            }
        }

        $this->initFooterRow($row);
    }

    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $map = $W->Map('map1');
        $map->setMapFrom('leafletjs');

        $map->setWidth('100%');
        $map->setHeight('90vh');

        $map->setCenter(46.8, 1.5);
        $map->setZoom(6);

        $map->addClass('widget-100pc');

        $this->getLayout()->addItem($map->setSizePolicy('widget-67pc'));

        $this->getLayout()->addItem(
            $App->Controller()->Organization(false)->previewHighlightedRecords()
                ->setSizePolicy('widget-33pc box')
        );

        $this->map = $map;

        $this->init();

        $this->addClass('depends-' . $this->getId());

        $items = array();

        $selector = $this->handlePageSelector();

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        $this->setMetadata('options', $this->options);

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js');
    }
}