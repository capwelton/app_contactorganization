<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

/**
 *
 * @method \Func_App App()
 */
class OrganizationCardsView extends OrganizationModelView
{

    /**
     *
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setSpacing(1, 'em');
        $this->setLayout($layout);

        $this->setFixedHeader(false);
    }

    public function getDisplaySubTotalRowCheckbox()
    {
        return null;
    }


    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(\ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see \Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::handleRow()
     */
    protected function handleRow(\ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $card = $Ui->OrganizationCardFrame($record);
        $card->addClass(/*'col-md-3', */'crm-card');
        $card->setSizePolicy('col-xs-12 col-sm-6 col-md-4 col-lg-3');
        $this->addItem($card);
        return true;
    }

    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->init();

        $this->addClass('depends-' . $this->getId());

        $items = array();

        $total = $this->handleTotalDisplay();
        $selector = $this->handlePageSelector();


        if (null !== $total) {
            $items[] = $total->display($canvas);
        }

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        $this->setMetadata('options', $this->options);

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js');
    }
}