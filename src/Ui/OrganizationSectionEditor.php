<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Organization;

/**
 *
 * @method \Func_App App()
 */
class OrganizationSectionEditor extends \app_RecordEditor
{
    
    protected $parentOrganizationItem = null;
    
    protected $parentOrganizationField = null;
    protected $incotermsItem = null;
    
    protected $items = array();
    
    public function __construct(\Func_App $app, Organization $record = null, $id = null, $layout = null)
    {
        $this->record = $record;
        
        parent::__construct($app, $id, $layout);
    }
    
    public function setRecord($record)
    {
        $this->record = $record;
        $this->recordSet = $record->getParentSet();
        
        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues();
            
            if (isset($this->items['parent'])) {
                if($parent = $record->parent()){
                    $this->items['parent']->addOption($parent->id, $parent->getName());
                    $this->items['parent']->setValue($parent->id);
                }
                else{
                    $this->items['parent']->addOption(0, '');
                    $this->items['parent']->setValue(0);
                }
                unset($values['parent']);
            }
            if (isset($this->items['secondaryParent'])) {
                $this->items['secondaryParent']->setIdValue($record->secondaryParent);
                unset($values['secondaryParent']);
            }
            if (isset($this->items['responsibleOrganization'])) {
                $this->items['responsibleOrganization']->setIdValue($record->responsibleOrganization);
                unset($values['responsibleOrganization']);
            }
            if (isset($this->items['responsible'])) {
                $contact = $record->responsible();
                if($contact){
                    $this->items['responsible']->addOption($contact->id, $contact->getFullName());
                }
                else{
                    $this->items['responsible']->addOption(0, '');
                }
//                 unset($values['responsible']);
            }
            if (isset($this->items['commercialReferralContact'])) {
                $contact = $record->commercialReferralContact();
                if($contact){
                    $this->items['commercialReferralContact']->addOption($contact->id, $contact->getFullName());
                }
                else{
                    $this->items['commercialReferralContact']->addOption(0, '');
                }
//                 unset($values['commercialReferralContact']);
            }
            
            $this->setValues($values, array($name));
        }
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     * @see \app_Editor::setValues()
     */
    public function setValues($organization, $namePathBase = array())
    {
        $App = $this->App();
        
        if ($organization instanceof Organization) {
            
            $values = $organization->getFormOutputValues();
            if(isset($this->incotermsItem)){
                if($organization->incoterms() != null){
                    $this->incotermsItem->addOption($organization->incoterms()->id,$organization->incoterms()->acronym);
                    $this->incotermsItem->setValue($organization->incoterms()->id);
                }
            }
            if(isset($this->paymentTermItem)){
                if($organization->paymentTerm() != null){
                    $this->paymentTermItem->addOption($organization->paymentTerm()->id,$organization->paymentTerm()->acronym);
                    $this->paymentTermItem->setValue($organization->paymentTerm()->id);
                }
            }
            $this->setValues(array('data' => $values));
        } else {
            parent::setValues($organization, $namePathBase);
        }
        return $this;
    }
    
    protected function _createdBy($customSection)
    {
        return null;
    }
    
    protected function _createdOn($customSection)
    {
        return null;
    }
    
    protected function _modifiedBy($customSection)
    {
        return null;
    }
    
    protected function _modifiedOn($customSection)
    {
        return null;
    }
    
    protected function _id($customSection)
    {
        return null;
    }
    
    
    protected function _uuid($customSection)
    {
        return null;
    }
    
    protected function _incoterms(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $selector = $W->Select2();
        $selector->setDataSource($App->Controller()->Incoterms()->search());
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Incoterms'),
            $this->incotermsItem = $selector->addClass('widget-100pc')->setName('incoterms'),
            $customSection
        );
    }
    
    protected function _paymentTerm(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $selector = $W->Select2();
        $selector->setDataSource($App->Controller()->PaymentTerm()->search());
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Payment terms'),
            $this->paymentTermItem = $selector->addClass('widget-100pc')->setName('paymentTerm'),
            $customSection
        );
    }
    
    protected function _currency(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select();
        
        $options = array();
        $currencySet = $App->CurrencySet();
        $currencies = $currencySet->select()->orderAsc($currencySet->name_fr);
        foreach ($currencies as $currency){
            $options[$currency->id] = sprintf('%s (%s)', $currency->name_fr, $currency->symbol);
        }
        
        $select->setOptions($options);
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Currency'),
            $select,
            'currency'
        );
    }
    
    protected function _description(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Description'),
            $this->descriptionItem = $W->CKEditor()->addClass('widget-100pc')->setName('description'),
            $customSection
        );
    }
    
    protected function _address(\app_CustomSection $customSection, $label = null)
    {
        return null;
    }
    
    protected function _parent(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $item = $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Parent organization'),
            $suggest = $Ui->SuggestParentOrganization($this->record->id)->setName('parent'),
            $customSection
        );
        
        if($parent = $this->record->parent()){
            $suggest->addOption($parent->id, $parent->getName());
            $suggest->items['parent']->setValue($parent->id);
        }
        else{
            $suggest->addOption(0, '');
            $suggest->setValue(0);
        }
            
        return $item;
    }
    
    protected function _payingOrganization(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $item = $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Paying organization'),
            $this->items['payingOrganization'] = $Ui->SuggestOrganization()->setName('payingOrganization'),
            $customSection
        );
        
        return $item;
    }
    
    protected function _status(\app_CustomSection $customSection, $label = null)
    {
        return null;
    }
    
    protected function _types(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $organizationTypeSet = $App->OrganizationTypeSet();
        
        $organizationTypes = $organizationTypeSet->select();
        $organizationTypes->orderAsc($organizationTypeSet->name);
        $select = $W->Select2()->setName('types')->setMultiple(true);
        
        foreach ($organizationTypes as $organizationType) {
            $select->addOption($organizationType->id, $organizationType->name);
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Type'),
            $select,
            $customSection
        );
    }
    
    protected function _logo(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Logo'),
            $App->Controller()->Organization(false)->logo($this->record->id),
            $customSection
        );
    }
    
    
    protected function _responsibleOrganization(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $item = $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Responsible organization'),
            $this->items['responsibleOrganization'] = $Ui->SuggestOrganization()->setName('responsibleOrganization'),
            $customSection
        );
        
        if (isset($this->items['responsible'])) {
            $this->items['responsible']->setRelatedOrganization($this->items['responsibleOrganization']);
        }
        
        return $item;
    }
    
    protected function _responsible(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $item = $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Responsible contact'),
            $this->items['responsible'] = $Ui->SuggestContact()->setName('responsible'),
            $customSection
        );
        
        if (isset($this->items['responsibleOrganization'])) {
            $this->items['responsible']->setRelatedOrganization($this->items['responsibleOrganization']);
        }
        
        return $item;
    }
    
    
    protected function _commercialReferralContact(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Commercial referral contact'),
            $this->items['commercialReferralContact'] = $Ui->SuggestContact()->setName('commercialReferralContact'),
            $customSection
        );
    }
    
    protected function _legalStatus(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $legalStatusSet = $App->LegalStatusSet();
        $legalStatuses = $legalStatusSet->select()->orderAsc($legalStatusSet->name)->orderAsc($legalStatusSet->code);
        
        $options = array();
        foreach ($legalStatuses as $legalStatus){
            $options[$legalStatus->id] = $legalStatus->name;
        }
        
        $select = $W->Select()->setOptions($options)->setName('legalStatus');
        $item = $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Legal status'),
            $select,
            $customSection
        );
        
        return $item;
    }
}