<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\OrganizationSet;


include_once 'base.php';

$W = bab_Widgets();
$W->includePhpClass('Widget_Select2');
$W->includePhpClass('Widget_SuggestLineEdit');

class SuggestOrganization extends \Widget_Select2
{
    private $App = null;

    protected $criteria = null;
    protected $contactSet = null;
    protected $contactOrganizationSet = null;
    protected $organizationSet = null;

    protected $relatedOrganizations = null;

    public function __construct(\Func_App $App, $id = null)
    {
        $this->setApp($App);
        parent::__construct($id);
        $this->setMultiple(false);
        $this->addSelect2Option('minimumInputLength', 0);
        //         $this->addOption('tags', true);
    }

    /**
     * Get App object
     * @return \Func_App
     */
    public function App()
    {
        return $this->App;
    }

    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param \Func_App	$App
     * @return self
     */
    public function setApp(\Func_App $App = null)
    {
        $this->App = $App;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'app-suggestorganization';
        return $classes;
    }
}

/**
 * A SuggestOrganization
 */
class SuggestOrganizationOld extends \Widget_SuggestLineEdit implements \Widget_Displayable_Interface, \app_Object_Interface
{

    private $App = null;

    protected $criteria = null;

    protected $organizationSet = null;

    protected $relatedOrganizations = null;

    private $excludedBranch = array();

    protected $excludedOrganizationIds = null;

    /**
     * Get App object
     * @return \Func_App
     */
    public function App()
    {
        return $this->App;
    }

    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param \Func_App	$App
     * @return self
     */
    public function setApp(\Func_App $App = null)
    {
        $this->App = $App;
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see \Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'app-suggestmember';
        return $classes;
    }


    /**
     * @return OrganizationSet
     */
    public function getOrganizationSet()
    {
        if (!isset($this->organizationSet)) {
            $this->organizationSet = $this->App()->OrganizationSet();
            $this->organizationSet->parent();
        }
        return $this->organizationSet;
    }



    /**
     * Specifies criteria that will be applied to suggested contacts.
     *
     * @return self
     */
    public function setCriteria(\ORM_Criteria $criteria = null)
    {
        $this->criteria = $criteria;
        return $this;
    }



    /**
     * Excludes the specified organization and all its descendants from the returned results.
     *
     * @return self
     */
    public function excludeDescendantsOf($organizationId)
    {
        $App = $this->App();

        $this->excludedBranch[$organizationId] = $organizationId;
        $this->excludedOrganizationIds = null;

        $this->setSuggestAction($App->Controller()->Organization()->suggestParent($organizationId), 'search');
        return $this;
    }



    /**
     * Set the widget used for specifying the organization
     *
     * @return self
     */
    public function setRelatedOrganizations($orgs)
    {
        $this->relatedOrganizations = $orgs;
        $suggestUrl =  $this->getMetadata('suggesturl');
        $suggestUrl['relatedOrganizations'] = $this->relatedOrganizations;
        $this->setMetadata('suggesturl', $suggestUrl);
        return $this;
    }


    /**
     * get search keyword
     * return a string search keyword if the widget try to get suggestions
     * or false if the widget does not search for suggestions
     *
     * @return false|string
     */
    public function getRelatedOrganization()
    {
        $org = bab_rp('relatedOrganizations', false);
        if (false === $org) {
            return false;
        }
        return $org;
    }

    /**
     *
     * @param array $descendants 	Contains the result of descendants ids.
     */
    private static function getDescendants($organization, &$descendants)
    {
        $set = $organization->getParentSet();
        $children = $set->select($set->parent->is($organization->id));
        foreach ($children as $child) {
            if ($child->id != $organization->id && !isset($descendants[$child->id])) {
                $descendants[$child->id] = $child->id;
                $child->getDescendants($descendants);
            }
        }
    }

    /**
     * Initializes the list of excluded organizations.
     *
     * @return void
     */
    protected function init()
    {
        if (isset($this->excludedOrganizationIds)) {
            return;
        }

        $App = $this->App();
        $organizationSet = $App->OrganizationSet();
        $this->excludedOrganizationIds = array();
        foreach ($this->excludedBranch as $parentId) {
            $this->excludedOrganizationIds[$parentId] = $parentId;
            $org = $organizationSet->get($parentId);
            self::getDescendants($org, $this->excludedOrganizationIds);
        }
    }

    /**
     * Send suggestions
     */
    public function suggest()
    {
        $this->init();

        if (false !== $keyword = $this->getSearchKeyword()) {

            $members = array();

            $organizationSet = $this->getOrganizationSet();
            $organizationSet->address();

            $criteria = array();
            $this->relatedOrganizations = $this->getRelatedOrganization();
            if (!empty($this->relatedOrganizations)) {
                $criteria[] = $organizationSet->id->in($this->relatedOrganizations);
            }

            $criteria[] = $organizationSet->name->startsWith($keyword);

            if (isset($this->criteria)) {
                $criteria[] = $this->criteria;
            }
            if (count($this->excludedOrganizationIds) > 0) {
                $criteria[] = $organizationSet->id->notIn($this->excludedOrganizationIds);
            }

            $organizations = $organizationSet->select($organizationSet->all($criteria));
            $organizations->orderAsc($organizationSet->name);

            $i = 0;
            foreach ($organizations as $organization) {
                $i++;
                if ($i > \Widget_SuggestLineEdit::MAX) {
                    break;
                }

                $value = $organization->name;
                if ($organization->parent->id) {
                    $value = $organization->parent->name . ' > ' . $value;
                }

                $members[$organization->id] = array(
                    'value' => $value,
                    'info' => $organization->getSuggestInfos(),
                    'css' => \Func_Icons::OBJECTS_ORGANIZATION
                );
            }

            $criteria = array();
            if (!empty($this->relatedOrganizations)) {
                $criteria[] = $organizationSet->id->in($this->relatedOrganizations);
            }
            $criteria[] = $organizationSet->name->contains($keyword);

            if (isset($this->criteria)) {
                $criteria[] = $this->criteria;
            }
            if (count($this->excludedOrganizationIds) > 0) {
                $criteria[] = $organizationSet->id->notIn($this->excludedOrganizationIds);
            }

            $organizations = $organizationSet->select($organizationSet->all($criteria));
            $organizations->orderAsc($organizationSet->name);

            $i = 0;
            foreach ($organizations as $organization) {
                $i++;
                if ($i > \Widget_SuggestLineEdit::MAX) {
                    break;
                }

                $value = $organization->name;
                if ($organization->parent->id) {
                    $value = $organization->parent->name . ' > ' . $value;
                }

                if (!isset($members[$organization->id])) {
                    $members[$organization->id] = array(
                        'value' => $value,
                        'info' => $organization->getSuggestInfos(),
                        'css' => \Func_Icons::OBJECTS_ORGANIZATION
                    );
                }
            }

            \bab_Sort::asort($members, 'value');


            $i = 0;
            foreach ($members as $memberRef => $member) {
                $i++;
                if ($i > \Widget_SuggestLineEdit::MAX) {
                    break;
                }

                $this->addSuggestion(
                    $memberRef,
                    $member['value'],
                    $member['info'],
                    '',
                    $member['css']
                );
            }

            $this->sendSuggestions();
        }
    }


    /**
     * {@inheritDoc}
     * @see \Widget_SuggestLineEdit::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->suggest();
        return parent::display($canvas);
    }


    /**
     * Set the value of the organization name from the id of this organization
     * If the organization id does not exist it does nothing
     *
     * @param   int     $id             The organization id
     * @return  self
     */
    public function setIdValue($id)
    {
        $organizationSet = $this->getOrganizationSet();
        $organization = $organizationSet->get($id);
        if ($organization) {
            parent::setIdValue($organization->id);
            $this->setValue($organization->getRecordTitle());
        } else {
            $this->setValue('');
        }

        return parent::setIdValue($id);
    }
}
