<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactPhone;
use Capwelton\App\ContactOrganization\Set\ContactSet;
use Capwelton\App\ContactOrganization\Ctrl\ContactController;
use Capwelton\App\ContactOrganization\Set\Contact;

/**
 * @method \Func_App App()
 */
class ContactModelView extends \app_TableModelView
{
    protected $contactRoleSet;
    protected $organizationCtrl;
    protected $widgets;
    protected $contactUi;
    
    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App = null, $id = null)
    {
        $contactRoleSet = $App->ContactRoleSet();
        $contactRoleSet->role();
        $this->contactRoleSet = $contactRoleSet;
        $this->organizationCtrl = $App->Controller()->Organization();
        $this->contactUi = $App->Ui();
        $this->widgets = bab_Widgets();
        parent::__construct($App, $id);
        
        $App = $this->App();
        $this->isUserAdministrator = bab_isUserAdministrator();
        $this->addClass('depends-contactRole');
    }
    
    
    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(ContactController $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(\ORM_RecordSet $set)
    {
        /* @var $set ContactSet */
        $App = $this->App();
        $this->addColumn(app_TableModelViewColumn($set->uuid)->setSearchable(false)->setVisible(false));
        $this->addColumn(app_TableModelViewColumn('image', ' ')->setSelectable(true, 'Image')->setSortable(false)->setExportable(false)->addClass('widget-column-thin', 'widget-column-center'));
        $this->addColumn(app_TableModelViewColumn('imageSmall', ' ')->setSelectable(true, 'Small image')->setVisible(false)->setSortable(false)->setExportable(false)->addClass('widget-column-thin', 'widget-column-center'));
        $this->addColumn(app_TableModelViewColumn('imageLarge', ' ')->setSelectable(true, 'Large image')->setVisible(false)->setSortable(false)->setExportable(false)->addClass('widget-column-thin', 'widget-column-center'));
        $this->addColumn(app_TableModelViewColumn($set->title)->addClass('widget-4em')->setSearchable(false)->setVisible(false));
        $this->addColumn(app_TableModelViewColumn($set->lastname));
        $this->addColumn(app_TableModelViewColumn($set->firstname));
        $this->addColumn(app_TableModelViewColumn($set->mainPosition()->organization()->name, $App->translate('Main organization')));
        $this->addColumn(app_TableModelViewColumn($set->mainPosition()->position, $App->translate('Position')));
        $this->addColumn(app_TableModelViewColumn('mainPhone', $App->translate('Main phone'))->setSearchable(true)->setVisible(true));
        $this->addColumn(app_TableModelViewColumn('mainPersonalPhone', $App->translate('Main personal phone'))->setSearchable(false)->setVisible(true));
        $this->addColumn(app_TableModelViewColumn($set->email));
        $this->addColumn(app_TableModelViewColumn('_actions_', ' ')->setSortable(false)->setExportable(false)->addClass('widget-column-thin'));
    }
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initRow()
     */
    public function initRow(Contact $record, $row)
    {
        $this->addRowClass($row, 'widget-actions-target');
        return parent::initRow($record, $row);
    }
    
     /**
     *
     */
    protected function getDisplayAction(Contact $record)
    {
        return $this->App()->Controller()->Contact()->display($record->id);
    }
    
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellTextContent()
     */
    protected function computeCellTextContent(Contact $record, $fieldPath)
    {
        switch ($fieldPath) {
            case 'modifiedBy':
                $content = bab_getUserName($record->modifiedBy, true);
                break;
            case 'name':
                $content = $record->getFullName();
                break;
                
            case 'roles':
                $contactRoleSet = $this->contactRoleSet;
                $contactRoles = $contactRoleSet->select($contactRoleSet->contact->is($record->id));
                
                $roles = array();
                foreach ($contactRoles as $contactRole) {
                    $roles[] = $contactRole->role->name;
                }
                
                $content = implode(', ', $roles);
                break;
            case 'organization_name':
                $org = $record->organization();
                $value = (isset($org) ? $org->name : '');
                $content = $value;
                break;
            case 'address/street':
                $content = '';
                if ($this->address) {
                    $content = $this->address->street;
                }
                break;
            case 'address/postalCode':
                $content = '';
                if ($this->address) {
                    $content = $this->address->postalCode;
                }
                break;
            case 'address/city':
                $content = '';
                if ($this->address) {
                    $content = $this->address->city;
                }
                break;
            case 'type':
                $values = array();
                $this->contactTypes;
                foreach ($this->contactTypes as $contactType) {
                    $values[] = $contactType->type;
                }
                $content = implode('; ', $values);
                break;
            case 'tags':
                $tags = $this->tagSet->selectFor($record);
                $values = array();
                foreach ($tags as $tag) {
                    $values[] = $tag->targetId->label;
                }
                $content = implode('; ', $values);
                break;
            case 'mainPhone':
                $content = $record->getMainPhone();
                break;
            case 'mainPersonalPhone':
                $content = $record->getMainPersonalPhone();
                break;
            default:
                $content = parent::computeCellTextContent($record, $fieldPath);
                break;
        }
        
        return $content;
    }
    
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellContent()
     */
    protected function computeCellContent(Contact $record, $fieldPath)
    {
        $W = $this->widgets;
        $App = $this->App();
        
        $displayAction = $this->getDisplayAction($record);
        
        switch ($fieldPath) {
            case 'image':
                $displayAction = $this->getDisplayAction($record);
                $content = $W->Link(
                    $this->contactUi->ContactAvatar($record, 24, 24),
                    $displayAction
                )->setSizePolicy('condensed');
                break;
            case 'imageSmall':
                $displayAction = $this->getDisplayAction($record);
                $content = $W->Link(
                    $this->contactUi->ContactAvatar($record, 16, 16),
                    $displayAction
                )->setSizePolicy('condensed');
                break;
            case 'imageLarge':
                $displayAction = $this->getDisplayAction($record);
                $content = $W->Link(
                    $this->contactUi->ContactAvatar($record, 40, 40),
                    $displayAction
                )->setSizePolicy('condensed');
                break;
            case 'name':
                $displayAction = $this->getDisplayAction($record);
                $content = $W->Link($record->getFullName(), $displayAction);
                break;
            case 'lastname':
            case 'firstname':
                $content = $W->Link($record->$fieldPath, $displayAction);
                break;
            case 'mainPosition/organization/name':
                $App = $this->App();
                $content = $W->Link(
                    $record->mainPosition->organization->name,
                    $this->organizationCtrl->display($record->mainPosition->organization->id)
                );
                break;
            case 'roles':
                $App = $this->App();
                $contactRoleSet = $this->contactRoleSet;
                $contactRoles = $contactRoleSet->select($contactRoleSet->contact->is($record->id));
                
                $roles = array();
                foreach ($contactRoles as $contactRole) {
                    $roles[] = $contactRole->role->name;
                }
                
                $content = $W->FlowItems(
                    $W->Html(bab_toHtml(implode(', ', $roles))),
                    $W->Link('', $this->recordController->editUserRoles($record->id))
                    ->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT, 'widget-actions')
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                )->addClass(\Func_Icons::ICON_LEFT_16);
                break;
            case 'mainPhone':
            case 'mainPersonalPhone':
                $content = $W->Label();
                $method = 'get'.ucfirst($fieldPath).'Record';
                /* @var $mainPhone ContactPhone */
                $mainPhone = $record->$method();
                if($mainPhone){
                    $content = $mainPhone->displayPhone();
                }
                break;
            default:
                $content = parent::computeCellContent($record, $fieldPath);
                break;
        }
        
        return $content;
    }
    
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        $recordSet = $this->getRecordSet();
        
        $conditions = array(
            $recordSet->isReadable()
        );
        
        $conditions[] = parent::getFilterCriteria($filter);
        
        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->lastname->contains($filter['search']),
                $recordSet->firstname->contains($filter['search']),
                $recordSet->mainPosition()->organization()->name->contains($filter['search'])
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }
        
        if(isset($filter['mainPhone']) && !empty($filter['mainPhone'])){
            $App = $this->App();
            $contactPhoneSet = $App->ContactPhoneSet();
            
            $contactPhones = $contactPhoneSet->select(
                $contactPhoneSet->phone->is($filter['mainPhone'])
            );
            
            $contactIds = array();
            foreach ($contactPhones as $contactPhone){
                if($contactPhone->isPersonal()){
                    $contactIds[] = $contactPhone->contact;
                    continue;
                }
                $contactOrganization = $contactPhone->contactOrganization();
                if($contactOrganization){
                    $contactIds[] = $contactOrganization->contact;
                }
            }
            
            if(count($contactIds) > 0){
                $conditions[] = $recordSet->id->in($contactIds);
            }
        }
        
        if (isset($filter['tags']) && !empty($filter['tags'])) {
            $tagNames = explode(',', $filter['tags']);
            $tags = array();
            foreach ($tagNames as $tagName) {
                $tagName = trim($tagName);
                if (empty($tagName)) {
                    continue;
                }
                $tags[] = $tagName;
            }
            
            $conditions[] = $recordSet->haveTagLabels($tags);
        }
        
        return $recordSet->all($conditions);
    }
    
    
    /**
     * Handle filter field input widget
     * If the method returns null, no filter field is displayed for the field
     *
     * @param   string          $name       table field name
     * @param   \ORM_Field       $field      ORM field
     *
     * @return \Widget_InputWidget | null
     */
    protected function handleFilterInputWidget($name, \ORM_Field $field = null)
    {
        $W = $this->widgets;
        $App = $this->App();
        $Ui = $App->Ui();
        
        
        if($name === 'tags'){
            return $Ui->SuggestTag()
            ->setName('tag')
            ->setMinChars(0)
            ->setSize(30)
            ->setMultiple(',');
        }
        if($name === 'type'){
            $select = $W->Select()->setName('type')->addOption('', '');
            $typeClassificationSet = $App->TypeClassificationSet();
            $typeClassifications = $typeClassificationSet->select();
            $typeClassifications->orderAsc($typeClassificationSet->type);
            foreach($typeClassifications as $typeClassification){
                $select->addOption($typeClassification->id, $typeClassification->type);
            }
            return $select;
        }
        if($name === 'mainPhone'){
            //We don't search for mainPersonalPhone
            //With mainPhone, we'll search for every phone numbers associated to the contact
            return $W->LineEdit()->setName($name);
        }
        
        if ($field instanceof \ORM_DateField || $field instanceof \ORM_DateTimeField){
            return $W->PeriodPicker()
            ->setLabels('', '/')
            ->setSize(7);
        }
        
        return parent::handleFilterInputWidget($name, $field);
    }
}