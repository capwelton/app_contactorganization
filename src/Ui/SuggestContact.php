<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactSet;
use Capwelton\App\ContactOrganization\Set\OrganizationSet;
use Capwelton\App\ContactOrganization\Set\ContactOrganizationSet;

include_once 'base.php';

$W = bab_Widgets();
$W->includePhpClass('Widget_Select2');
$W->includePhpClass('Widget_SuggestLineEdit');

class SuggestContact extends \Widget_Select2
{
    private $App = null;

    protected $criteria = null;
    protected $contactSet = null;
    protected $contactOrganizationSet = null;
    protected $organizationSet = null;

    protected $relatedOrganizations = null;

    public function __construct(\Func_App $App, $id = null)
    {
        $this->setApp($App);
        parent::__construct($id);
        $this->setMultiple(false);
        $this->addSelect2Option('minimumInputLength', 0);
        //         $this->addOption('tags', true);
    }

    /**
     * Get App object
     * @return \Func_App
     */
    public function App()
    {
        return $this->App;
    }

    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param \Func_App	$App
     * @return self
     */
    public function setApp(\Func_App $App = null)
    {
        $this->App = $App;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'app-suggestmember';
        return $classes;
    }
}

/**
 * A SuggestContact
 */
class SuggestContactOld extends \Widget_SuggestLineEdit implements \Widget_Displayable_Interface, \app_Object_Interface
{

    private $App = null;

    protected $criteria = null;
    protected $contactSet = null;
    protected $contactOrganizationSet = null;
    protected $organizationSet = null;

    protected $relatedOrganizations = null;

    /**
     * Get App object
     * @return \Func_App
     */
    public function App()
    {
        return $this->App;
    }

    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param \Func_App	$App
     * @return self
     */
    public function setApp(\Func_App $App = null)
    {
        $this->App = $App;
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see \Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'app-suggestmember';
        return $classes;
    }



    /**
     * @return ContactSet
     */
    public function getContactSet()
    {
        if (!isset($this->contactSet)) {
            $this->contactSet = $this->App()->ContactSet();
        }
        return $this->contactSet;
    }

    /**
     * @return OrganizationSet
     */
    public function getOrganizationSet()
    {
        if (!isset($this->organizationSet)) {
            $this->organizationSet = $this->App()->OrganizationSet();
        }
        return $this->organizationSet;
    }


    /**
     * @return ContactOrganizationSet
     */
    public function getContactOrganizationSet()
    {
        if (!isset($this->organizationOrganizationSet)) {
            $this->contactOrganizationSet = $this->App()->ContactOrganizationSet();
            $this->contactOrganizationSet->contact();
            $this->contactOrganizationSet->organization();
        }
        return $this->contactOrganizationSet;
    }



    /**
     * Specifies criteria that will be applied to suggested contacts.
     *
     * @return self
     */
    public function setCriteria(\ORM_Criteria $criteria = null)
    {
        $this->criteria = $criteria;
        return $this;
    }



    /**
     * Set the widget used for specifying the organization
     *
     * @return self
     */
    public function setRelatedOrganizations($orgs)
    {
        $this->relatedOrganizations = $orgs;
        $suggestUrl =  $this->getMetadata('suggesturl');
        $suggestUrl['relatedOrganizations'] = $this->relatedOrganizations;
        $this->setMetadata('suggesturl', $suggestUrl);
        return $this;
    }

    /**
     * get search keyword
     * return a string search keyword if the widget try to get suggestions
     * or false if the widget does not search for suggestions
     *
     * @return false|string
     */
    public function getRelatedOrganization()
    {
        $org = bab_rp('relatedOrganizations', false);
        if (false === $org) {
            return false;
        }
        return $org;
    }


    /**
     * Send suggestions
     */
    public function suggest()
    {
        $App = $this->App();
        $this->relatedOrganizations = $this->getRelatedOrganization();
        if (false !== $keyword = $this->getSearchKeyword()) {
            $contactSet = $App->ContactSet();
            if ($this->relatedOrganizations != 0) {
                $contactOrg = $App->ContactOrganizationSet();
                $contactsOrganizations = $contactOrg->select(
                    $contactOrg->organization->is($this->relatedOrganizations)
                );
                $contactId = array();
                foreach ($contactsOrganizations as $CO) {
                    $contactId[] = $CO->contact;
                }
                $this->criteria = $contactSet->id->in($contactId);
            }

            $members = array();
            $criteria = $contactSet->any(
                $contactSet->lastname->startsWith($keyword),
                $contactSet->firstname->startsWith($keyword),
                $contactSet->firstname->concat(' ', $contactSet->lastname)->startsWith($keyword)
            );
            if (isset($this->criteria)) {
                $criteria = $criteria->_AND_($this->criteria);
            }

            $contacts = $contactSet->select($criteria);
            $contacts->orderAsc($contactSet->lastname);

            $i = 0;
            foreach ($contacts as $contact) {
                $i++;
                if ($i > \Widget_SuggestLineEdit::MAX) {
                    break;
                }
                $members[$contact->id] = array(
                    'value' => $contactSet->title->output($contact->title) . ' ' . $contact->getFullName(),
                    'info' => '',
                    'css' => \Func_Icons::OBJECTS_CONTACT
                );
            }

            \bab_Sort::asort($members, 'value');


            $i = 0;
            foreach ($members as $memberRef => $member) {
                $i++;
                if ($i > \Widget_SuggestLineEdit::MAX) {
                    break;
                }

                $this->addSuggestion(
                    $memberRef,
                    $member['value'],
                    $member['info'],
                    '',
                    $member['css']
                );
            }

            if (!empty(trim($keyword))) {
                $this->addSuggestion(
                    'new',
                    $keyword,
                    $App->translate('New contact'),
                    '',
                    'widget-actionbutton'
                );
            }

            $this->sendSuggestions();
        }
    }


    /**
     * {@inheritDoc}
     * @see \Widget_SuggestLineEdit::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->suggest();
        return parent::display($canvas);
    }



    /**
     * Set the value of the organization name from the id of this organization
     * If the organization id does not exist it does nothing
     *
     * @param   int     $id             The contact id
     * @return  self
     */
    public function setIdValue($id)
    {
        $contactSet = $this->getContactSet();
        $contact = $contactSet->get($id);
        if ($contact) {
            parent::setIdValue($contact->id);
            $this->setValue($contactSet->title->output($contact->title) . ' ' . $contact->getFullName());
        } else {
            $this->setValue('');
        }

        return parent::setIdValue($id);
    }
}
