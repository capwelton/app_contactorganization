<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Role;

/**
 * @return RoleEditor
 */
class RoleEditor extends \app_Editor
{
    /**
     * @var Role
     */
    protected $role;
    
    /**
     * @param \Widget_Layout $layout	The layout that will manage how widgets are displayed in this form.
     * @param string $id			The item unique id.
     */
    public function __construct(\Func_App $App, Role $role = null, $id = null, \Widget_Layout $layout = null)
    {
        if (null === $layout) {
            $W = bab_Widgets();
            $layout = $W->VBoxLayout()->setVerticalSpacing(2, 'em');
        }
        
        parent::__construct($App, $id, $layout);
        $this->role = $role;
        
        $this->colon();
        $this->setHiddenValue('tg', $App->controllerTg);
        $this->setName('data');
        
        
        $this->addButtons();
        
        if (isset($role)) {
            $this->setHiddenValue('role[id]', $role->id);
            $this->setValues($role->getValues(), array('role'));
        }
    }
    
    public function prependFields()
    {
        return $this;
    }
    
    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();
        
        $this->addItem($this->name());
        $this->addItem($this->description());
        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
        }
        return $this;
    }
    
    protected function name()
    {
        $App = $this->App();
        return $this->labelledField(
            $App->translate('Name'),
            $this->record->getParentSet()->name->getWidget()
            ->setMandatory(true, $App->translate('The role name must not be empty')),
            'name'
        );
    }
    
    protected function description()
    {
        $App = $this->App();
        return $this->labelledField(
            $App->translate('Description'),
            $this->record->getParentSet()->description->getWidget()
            ->addClass('widget-autoresize'),
            'description'
        );
    }
}