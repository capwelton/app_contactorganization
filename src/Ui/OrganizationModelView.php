<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Organization;
use Capwelton\App\ContactOrganization\Set\OrganizationSet;

/**
 * @method \Func_App    App()
 */
class OrganizationModelView extends \app_TableModelView
{
    
    private $componentUi;

    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App, $id = null)
    {
        parent::__construct($App, $id);
        $this->componentUi = $App->Ui();

        $this->addClass('depends-Organization');
        $this->addClass("depends-{$App->classPrefix}Organization");
    }


    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(\app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \app_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(OrganizationSet $organizationSet = null)
    {
        $App = $this->App();

        if (!isset($organizationSet)) {
            $organizationSet = $this->getRecordSet();
        }

        $organizationSet->address();
        $organizationSet->address->country();

        
        $this->addColumn(app_TableModelViewColumn('_actions_', ' ')->setVisible(true)->setSortable(false)->setExportable(false)->addClass('widget-column-thin'));
        $this->addColumn(
            app_TableModelViewColumn('image', ' ')
                ->setSelectable(true, 'Image')
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin', 'widget-column-center')
        );
        $this->addColumn(
            app_TableModelViewColumn('imageSmall', ' ')
                ->setSelectable(true, 'Small image')
                ->setVisible(false)
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin', 'widget-column-center')
        );
        $this->addColumn(
            app_TableModelViewColumn('imageLarge', ' ')
                ->setSelectable(true, 'Large image')
                ->setVisible(false)
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin', 'widget-column-center')
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->id, $App->translate('N.'))
                ->addClass('widget-column-thin')
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->createdOn, $App->translate('Created on'))
            ->addClass('widget-column-thin')
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->isInternal(), $App->translate('Internal'))
                ->setVisible(false)
                ->addClass('widget-column-thin')
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->name, $App->translate('Name'))
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->code, $App->translate('Code'))
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->accountNumber, $App->translate('Account number'))
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->siren, $App->translate('Siren number'))
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->siret, $App->translate('Siret number'))
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->paymentCode, $App->translate('Payment code'))
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->parent()->name, $App->translate('Parent organization'))
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->payingOrganization()->name, $App->translate('Paying organization'))
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->phone, $App->translate('Phone'))
                ->setSearchable(false)
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->fax, $App->translate('Fax'))
                ->setSearchable(false)
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->email, $App->translate('Email'))
                ->setSearchable(false)
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->website, $App->translate('Web site'))
                ->setSearchable(false)
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->address->street, $App->translate('Number / pathway'))
                ->setSearchable(false)
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->address->postalCode, $App->translate('Postal code'))
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->address->city, $App->translate('City'))
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->address->cityComplement, $App->translate('Complement'))
                ->setSearchable(false)
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($organizationSet->address->country->getNameField(), $App->translate('Country'))
                ->setSearchable(true)
                ->setVisible(false)
        );

        $this->addColumn(
            app_TableModelViewColumn("organizationtype", $App->translate('type'))
            ->setVisible(false)
            ->setExportable(true)
        );
        $this->addColumn(
            app_TableModelViewColumn('_actions_', ' ')
                ->setSelectable(true, '[Actions]')
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin')
        );

    }


    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initRow()
     */
    public function initRow(\ORM_Record $record, $row)
    {
        $this->addRowClass($row, 'widget-actions-target');
        return parent::initRow($record, $row);
    }


    /**
     *
     */
    protected function getDisplayAction(Organization $record)
    {
        return $this->App()->Controller()->Organization()->display($record->id);
    }


    /**
     * @param Organization    $record
     * @param string        $fieldPath
     * @return \Widget_Item
     */
    protected function computeCellContent(Organization $record, $fieldPath)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $ctrl = $App->Controller()->Organization();

        switch ($fieldPath) {

            case 'name':
            case 'name2':
            case 'code':
            case 'id':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    self::getRecordFieldValue($record, $fieldPath),
                    $displayAction
                );
            case 'parent/name':
                return $W->Link(
                    $record->parent->name,
                    $ctrl->display($record->parent->id)
                );
            case 'payingOrganization/name':
                $payingOrganization = $record->getPayingOrganization();
                return $W->Link(
                    $payingOrganization->name,
                    $ctrl->display($payingOrganization->id)
                );
                
            case 'initials':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $record->initials,
                    $displayAction
                );
            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '', $ctrl->setHighlightedRecords(array($record->id))
                        )->addClass('icon', \Func_Icons::ACTIONS_ARROW_RIGHT_DOUBLE, 'app_highlightButton')
                        ->setAjaxAction(null, '')
                    );  
                }
                return $box ;
                
                
            case 'image':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $this->componentUi->OrganizationLogo($record, 40, 24),
                    $displayAction
                )->setSizePolicy('condensed');

            case 'imageSmall':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $this->componentUi->OrganizationLogo($record, 24, 24),
                    $displayAction
                )->setSizePolicy('condensed');

            case 'imageLarge':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $this->componentUi->OrganizationLogo($record, 80, 64),
                    $displayAction
                )->setSizePolicy('condensed');
        }

        return parent::computeCellContent($record, $fieldPath);
    }

    public function computeCellTextContent($record,$fieldPath){
        if($fieldPath == "organizationtype"){
            $App = $this->App();
            $organizationTypeOrganizationSet = $App->OrganizationTypeOrganizationSet();
            $organizationTypeOrganizationSet->organizationType();
            $organizationTypeOrganizations = $organizationTypeOrganizationSet->select(
                $organizationTypeOrganizationSet->organization->is($record->id)
            );
            $organizationTypeOrganizations->orderAsc($organizationTypeOrganizationSet->organizationType->id);
            
            
            $organizationTypes = array();
            foreach ($organizationTypeOrganizations as $organizationTypeOrganization) {
                if (empty($organizationTypeOrganization->organizationType->id)) {
                    continue;
                }
                $organizationTypes[$organizationTypeOrganization->organizationType->id] = $organizationTypeOrganization->organizationType->name;
            }
            return implode(',',$organizationTypes);
        }
        return parent::computeCellTextContent($record, $fieldPath);
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        $recordSet = $this->getRecordSet();
        $App = $this->App();
        $conditions = array(
            $recordSet->isReadable()
        );

        $conditions[] = parent::getFilterCriteria($filter);

        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->name->contains($filter['search']),
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }
        
        if (isset($filter['organizationtype']) && !empty($filter['organizationtype'])) {
            $organizationTypeOrganizationSet = $App->OrganizationTypeOrganizationSet();
            $organizationTypeOrganizations = $organizationTypeOrganizationSet->select(
                $organizationTypeOrganizationSet->organizationType->in($filter["organizationtype"])
            );
            $orgaId = array();
            foreach($organizationTypeOrganizations as $orgType){
                $orgaId[] = $orgType->organization;
            }
            $conditions[] = $recordSet->id->in($orgaId);
        }
        
        return $recordSet->all($conditions);
    }
    
   
     /**
     * Handle filter field input widget
     * If the method returns null, no filter field is displayed for the field
     *
     * @param   string          $name       table field name
     * @param   \ORM_Field       $field      ORM field
     *
     * @return \Widget_InputWidget | null
     */
    protected function handleFilterInputWidget($name, \ORM_Field $field = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        if($name === 'organizationtype'){
            $organizationTypeOrganizationSet = $App->OrganizationTypeSet();
            $orgtypes = $organizationTypeOrganizationSet->select();
            $orgtypes->orderAsc($organizationTypeOrganizationSet->name);
            $options = array();
            foreach($orgtypes as $orgtype){
                $options[$orgtype->id]= $orgtype->name;
            }
            $select = $W->Multiselect()->setOptions($options);
            return $select;
        }

        return parent::handleFilterInputWidget($name, $field);
    }
}