<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Organization;
use Capwelton\App\ContactOrganization\Set\OrganizationSet;

/**
 * organization card frame
 *
 */
class OrganizationCardFrame extends \app_CardFrame
{
    /**
     * @var OrganizationSet
     */
    protected $organizationSet = null;
    
    /**
     * @var Organization
     */
    protected $organization = null;
    
    /**
     * @param \Func_App $app
     * @param Organization $organization
     * @param string|null $id
     */
    public function __construct(\Func_App $app, Organization $organization, $id = null, $openMode = \Widget_Link::OPEN_PAGE)
    {
        $W = bab_Widgets();
        $cardLayout = $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
        parent::__construct($app, $id, $cardLayout);
        
        $App = $this->App();
        
        $this->organization = $organization;
        $this->organizationSet = $organization->getParentSet();
        $image = $App->Ui()->OrganizationLogo($organization, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
        
        $image = $W->Link(
            $image->setSizePolicy(\Widget_SizePolicy::MINIMUM),
            $App->Controller()->Organization()->display($this->organization->id)
        );
        
        $cardLayout->addItem(
            $W->HBoxItems(
                $image,
                $W->VBoxItems(
                    $W->VBoxItems(
                        $W->Link(
                            $organization->name,
                            $App->Controller()->Organization()->display($organization->id)
                        )->setOpenMode($openMode)->addClass('crm-card-title', 'widget-strong'),
                        $this->parent()
                        ->addClass('crm-small')
                    ),
                    $App->Controller()->OrganizationMaintenanceType(false)->listForOrganization($organization->id, false),
                    $W->VBoxItems(
                        $this->email(),
                        $this->phone(),
                        $this->address()
                    )->addClass('crm-small')
                )->setVerticalSpacing(0.5, 'em')
            ->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
            )->setSpacing(0.5, 'em')
        );
    }
    
    protected function parent()
    {
        $W = bab_Widgets();
        $parent = $this->organization->parent();
        if (!$parent || !$parent->id) {
            return $W->Label('');
        }
        $W = bab_Widgets();
        $App = $this->App();
        return $W->Link(
            $parent->name,
            $App->Controller()->Organization()->display($parent->id)
        );
    }
    
    protected function phone()
    {
        $W = bab_Widgets();
        $phone = $this->organization->phone;
        if (empty($phone)) {
            return null;
        }
        return $W->Label(
            $phone
        )->addClass('icon', "objects-phone" /*Func_Icons::OBJECTS_PHONE*/)
        ->setSizePolicy(\Func_Icons::ICON_LEFT_16);
    }
    
    protected function email()
    {
        $W = bab_Widgets();
        $email = $this->organization->email;
        if (empty($email)) {
            return null;
        }
        return $W->Label(
            $email
        )->addClass('icon', \Func_Icons::OBJECTS_EMAIL)
        ->setSizePolicy(\Func_Icons::ICON_LEFT_16);
    }
    
    protected function address()
    {
        $W = bab_Widgets();
        $address = $this->organization->getMainAddress();
        $html = array();
        if (!empty($address->street)) {
            $html[] = bab_toHtml($address->street, BAB_HTML_BR);
        }
        $line2 = array();
        if (!empty($address->postalCode)) {
            $line2[] = bab_toHtml($address->postalCode);
        }
        if (!empty($address->city)) {
            $line2[] = $address->city;
        }
        if (!empty($address->cityComplement)) {
            $line2[] = bab_toHtml($address->cityComplement);
        }
        $line2 = implode(' ', $line2);
        if (!empty($line2)) {
            $line2 = array($line2);
        } else {
            $line2 = array();
        }
        if (!empty($address->country->name_fr)) {
            $line2[] = bab_toHtml($address->country->name_fr);
        }
        $html[] = implode(' - ', $line2);
        $html = implode('<br>', $html);
        return $W->Html(
            '<div style="display: inline-block; vertical-align: top">' . $html. '</div>'
        )->addClass('icon', "objects-map-marker", 'widget-nowrap')
        ->setSizePolicy(\Func_Icons::ICON_LEFT_16);
    }
}