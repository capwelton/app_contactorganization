<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Organization;

/**
 * Organization editor
 *
 * @method \Func_App    App()
 */
class OrganizationEditor extends \app_Editor
{
    
    protected $nameItem = null;
    
    /**
     * @var Organization
     */
    protected $organization;
    
    /**
     * @param \Widget_Layout $layout	The layout that will manage how widgets are displayed in this form.
     * @param string $id			The item unique id.
     */
    public function __construct(\Func_App $App, Organization $organization = null, $id = null, \Widget_Layout $layout = null)
    {
        if (null === $layout) {
            $W = bab_Widgets();
            $layout = $W->VBoxLayout()->setVerticalSpacing(2, 'em');
        }
        
        parent::__construct($App, $id, $layout);
        $this->organization = $organization;
        
        $this->colon();
        $this->setHiddenValue('tg', $App->controllerTg);
        $this->setName('data');
        
        
        $this->addButtons();
        
        if (isset($organization)) {
            $this->setHiddenValue('organization[id]', $organization->id);
            $this->setValues($organization->getValues(), array('organization'));
        }
    }
    
    
    /**
     * Add a default field set to form
     */
    protected function appendFields()
    {
        $W = bab_Widgets();
        $this->addItem($this->id());
        $this->addItem(
            $W->FlowItems(
                $this->name()->setSizePolicy('widget-25pc'),
                $this->initials()->setSizePolicy('widget-25pc')
            )->setHorizontalSpacing(2, 'em')
        );
        $this->addItem($this->description());
        $this->addItem($this->getContactInformationsSection());
        $this->addItem(
            $W->FlowItems(
                $this->phone()->setSizePolicy('widget-25pc'),
                $this->fax()->setSizePolicy('widget-25pc'),
                $this->email()->setSizePolicy('widget-50pc')
            )->setHorizontalSpacing(2, 'em')
        );
        $this->addItem($this->types());
        $this->addItem($this->intracommunityVat());
        $this->addItem($this->website());
        $this->addItem($this->address());
    }
    
    
    protected function types(){
        $App = $this->App();
        $W = bab_Widgets();
        
        $organizationTypeSet = $App->OrganizationTypeSet();
        
        $organizationTypes = $organizationTypeSet->select();
        $organizationTypes->orderAsc($organizationTypeSet->name);
        $select = $W->Select2()->setName('types')->setMultiple(true);
        foreach ($organizationTypes as $organizationType) {
            $select->addOption($organizationType->id, $organizationType->name);
        }
        
        if(isset($this->record)){
            $values = $this->record->getFormOutputValues();
            if(isset($values['types'])){
                $select->setValue($values['types']);
            }
        }
        
        return $W->LabelledWidget(
            $App->translate('Types'),
            $select
        );
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        
        $this->setSaveAction($App->Controller()->Organization()->save());
    }
    
    
    protected function id()
    {
        $W = bab_Widgets();
        return $W->Hidden()->setName('id');
    }
    
    protected function name()
    {
        $App = $this->App();
        $W = $this->widgets;
        return $this->labelledField(
            $App->translate('Name'),
            $this->nameItem = $W->LineEdit()
            ->addClass('widget-100pc')
            ->setMandatory(true, $App->translate('You must specify the organization name')),
            'name'
        );
    }
    
    protected function description()
    {
        $App = $this->App();
        $W = $this->widgets;
        return $this->labelledField(
            $App->translate('Description'),
            $this->descriptionItem = $W->CKEditor()
            ->addClass('widget-100pc'),
            'description'
        );
    }
    
    protected function initials()
    {
        $App = $this->App();
        $W = $this->widgets;
        return $this->labelledField(
            $App->translate('Initials'),
            $this->initialsItem = $W->LineEdit()
            ->addClass('widget-100pc'),
            'initials'
        );
    }
    
    protected function siret()
    {
        $App = $this->App();
        $W = $this->widgets;
        return $this->labelledField(
            $App->translate('Siret'),
            $this->initialsItem = $W->LineEdit()
            ->addClass('widget-100pc'),
            'siret'
        );
    }
    
    
    
    
    protected function email()
    {
        $W = bab_Widgets();
        return $this->labelledField(
            $this->App()->translate('Email'),
            $W->EmailLineEdit()
            ->addClass('widget-100pc'),
            'email'
        );
    }
    
    
    protected function phone()
    {
        $W = bab_Widgets();
        return $this->labelledField(
            $this->App()->translate('Phone'),
            $W->TelLineEdit(),
            'phone'
        );
    }
    
    
    protected function fax()
    {
        $W = bab_Widgets();
        return $this->labelledField(
            $this->App()->translate('Fax'),
            $W->TelLineEdit(),
            'fax'
        );
    }
    
    
    protected function website()
    {
        $W = bab_Widgets();
        return $this->labelledField(
            $this->App()->translate('Web site'),
            $W->UrlLineEdit()
            ->setSchemeCheck(false)
            ->setSize(75)
            ->setMaxSize(255)
            ->addClass('widget-fullwidth'),
            'website'
        );
    }
    
    
    protected function address()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $this->addressSection = $W->Section(
            $App->translate('Address'),
            $App->Ui()->AddressEditor()->setName('address')
        );
    }
    
    protected function intracommunityVat()
    {
        $W = bab_Widgets();
        return $this->labelledField(
            $this->App()->translate('Intracommunity VAT'),
            $W->LineEdit(),
            'intracommunityVat'
        );
    }
    
    
    /**
     * @param	array	$row
     * @param	array 	$namePathBase
     * @return 	OrganizationEditor
     */
    public function setValues($organization, $namePathBase = array())
    {
        if (!($organization instanceof Organization)) {
            return parent::setValues($organization, $namePathBase);
        }
        
        $name = $this->getName();
        $organizationValues = $organization->getValues();
        parent::setValues($organizationValues, $namePathBase);
        
        if (!empty($organization->id)) {
            $this->setHiddenValue($name . '[id]', $organization->id);
            $parentOrganization = $organization->parent();
            $this->suggestParentOrganization->excludeDescendantsOf($organization->id);
            $this->suggestParentOrganization->setValue($parentOrganization);
        }
        return $this;
    }
}
