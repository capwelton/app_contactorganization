<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\Entry\Set\Entry;

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');

class SuggestPosition extends \Widget_SuggestLineEdit implements \Widget_Displayable_Interface, \app_Object_Interface
{
    /**
     * Get App object
     * @return \Func_App
     */
    public function App()
    {
        return $this->App;
    }
    
    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param \Func_App	$App
     * @return self
     */
    public function setApp(\Func_App $App = null)
    {
        $this->App = $App;
        return $this;
    }
}