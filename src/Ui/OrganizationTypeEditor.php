<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\OrganizationType;

/**
 * Article editor, the field can depend on article type
 * @return \Widget_Form
 */
class OrganizationTypeEditor extends \app_Editor
{
    /**
     * @var OrganizationType
     */
    protected $organizationType = null;
    
    
    
    /**
     *
     * @param \Func_App $App
     * @param OrganizationType $organizationType
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, OrganizationType $organizationType = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->organizationType = $organizationType;
        
        parent::__construct($App, $id, $layout);
        $this->setName('organizationType');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        
        if (isset($organizationType)) {
            $this->setHiddenValue('organizationType[id]', $organizationType->id);
            $this->setValues($organizationType, array('organizationType'));
        }
        else{
            $this->setValues(array('profile' => OrganizationType::PROFILE_PURCHASE), array('organizationType'));
        }
    }
    
    protected function addFields()
    {
        $this->addItem($this->name());
        $this->addItem($this->profile());
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addButton(
            $submit = $W->SubmitButton()
            ->setLabel($App->translate('Save'))
            ->validate(true)
        );
        
        $this->addButton(
            $cancel = $W->SubmitButton()
            ->setLabel($App->translate('Cancel'))
        );
        
        if(bab_isAjaxRequest()){
            $submit->setAjaxAction($App->Controller()->OrganizationType()->save());
            $cancel->setAjaxAction($App->Controller()->OrganizationType()->cancel());
        }
        else{
            $submit->setAction($App->Controller()->OrganizationType()->save());
            $cancel->setAction($App->Controller()->OrganizationType()->cancel());
        }
    }
    
    /**
     *
     * @return \Widget_Item
     */
    protected function name()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Name'),
            $W->LineEdit()
            ->setMandatory(true, $App->translate('The name is mandatory'))
            ->addClass('widget-100pc'),
            'name'
        );
    }
    
    protected function profile()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $profiles = OrganizationType::getProfiles($App);
        
        $radio = $W->RadioSet();
        $radio->setOptions($profiles);
        
        return $this->labelledField(
            $App->translate('Profile'),
            $radio,
            'profile'
        );
    }
    
    /**
     *
     * @return \Widget_Item
     */
    protected function description()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Description'),
            $W->TextEdit()
            ->addClass('widget-100pc'),
            'description'
        );
    }
    
    public function setValues($organizationType, $namePathBase = array())
    {
        if ($organizationType instanceof OrganizationType) {
            $values = $organizationType->getFormOutputValues();
            $this->setValues(array('organizationType' => $values));
        } else {
            parent::setValues($organizationType, $namePathBase);
        }
    }
}