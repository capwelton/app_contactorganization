<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Ctrl\RoleController;
use Capwelton\App\ContactOrganization\Set\RoleSet;
use Capwelton\App\ContactOrganization\Set\Role;

/**
 *
 * @method \Func_App    App()
 */
class RoleTableView extends RoleModelView
{
    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);
        
        $this->setFixedHeader();
        $this->setMultiSelect(true);
    }

    /**
     * @param Role    $record
     * @param string        $fieldPath
     * @return \Widget_Item
     */
    protected function computeCellContent(Role $record, $fieldPath)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $ctrl = $App->Controller()->Role();
        /* @var $record  */
        switch ($fieldPath) {
            case '_actions_':
                $box = $W->HBoxItems();
                $box->setSizePolicy(\Func_Icons::ICON_LEFT_16);
                if ($record->isUpdatable()) {
                    $box->addItem(
                        $W->Link(
                            '', 
                            $ctrl->edit($record->id)
                        )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    );
                }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '', 
                            $ctrl->confirmDelete($record->id)
                        )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    );
                }

                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}