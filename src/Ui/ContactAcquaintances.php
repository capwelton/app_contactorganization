<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Contact;

/**
 * List of contacts for knowing a specified contact
 * @method Func_App App()
 */
class ContactAcquaintances extends \app_UiObject
{
    public function __construct(\Func_App $app)
    {
        parent::__construct($app);
    }
    
    /**
     *
     * @param Contact   $contact
     * @param int       $limit          Maximum contact to display (default 50)
     *
     * @return \Widget_Frame
     */
    public function getFrame(Contact $contact, $limit = 50)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $App->Ui()->includeContact();
        
        $contactFrame = $W->VBoxItems();
        $contactFrame->setVerticalSpacing(2, 'em');
        
        $contactFrame->addItem($this->contactsBox($contact, $limit));
        
        return $contactFrame;
    }
    
    public function contactsBox(Contact $contact, $limit = 50)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        $Ui->includeContact();
        
        $box = $W->VBoxLayout();
        $box->setIconFormat(16, 'left');
        $box->setVerticalSpacing(1, 'em');
        
        $contactAquaintances = $contact->selectContactsKnowing();
        
        $max = $limit;
        
        $level = null;
        $nb = 0;
        
        $contactCtrl = $App->Controller()->Contact();
        $organizationCtrl = $App->Controller()->Organization();
        
        foreach ($contactAquaintances as $contactAquaintance) {
            $contact = $contactAquaintance->contact();
            
            $organization = $contact->getMainContactOrganization();
            
            $menu = $W->Menu(null, $W->VBoxItems());
            if ($contact->isUpdatable()) {
                $menu->addItem(
                    $W->Link(
                        $App->translate('Edit'),
                        $contactCtrl->editAcquaintance($contactAquaintance->acquaintance, $contact->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                );
            }
            
            $contactCard = $W->FlowItems(
                $W->VBoxItems(
                    $W->Link(
                        $contact->getFullName(),
                        $contactCtrl->display($contact->id)
                    )->addClass('crm-card-title'),
                    $W->Link(
                        $organization->name,
                        $organizationCtrl->display($organization->id)
                    )->addClass('widget-small grey')
                    ),
                $W->FlowItems(
                    $W->Link(
                        $Ui->ContactPhoto($contact, 24, 24),
                        $contactCtrl->display($contact->id)
                    ),
                    $menu
                )->setSizePolicy('pull-right')
            )->setSizePolicy('widget-100pc');
                
            if ($level != $contactAquaintance->level) {
                $level = $contactAquaintance->level;
                $levelSection = $W->Section(
                    sprintf($App->translate('Level %s'), $level),
                    $contactBox = $W->VBoxItems(),
                    7
                )->setFoldable(true);
                $levelSection->addClass('app-contactacquaintance-level-' . $contactAquaintance->level);
                $box->addItem($levelSection);
            }
                
            $contactCard->setSizePolicy('widget-list-element');
            $contactBox->addItem($contactCard);
                
            $nb++;
            $max--;
                
            $remains = $contactAquaintances->count() - $limit;
            
            if ($max <= 0 && $remains > 1) {
                
                $item = $this->remains($contact, $remains);
                
                if (null !== $item) {
                    $box->addItem($item);
                }
                break;
            }
        }
        
        if ($nb == 0) {
            return null;
        }
        
        return $box;
    }
    
    /**
     * Called if the getFrame method return not all the contacts
     *
     * @param	Contact   $contact
     * @param 	int       $remains
     * @return \Widget_Item
     **/
    protected function remains(Contact $contact, $remains)
    {
        $W = bab_Widgets();
        return $W->Label(sprintf($this->App()->translate('And %d contacts not displayed'), $remains));
    }
}