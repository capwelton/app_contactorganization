<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\OrganizationType;


class OrganizationTypeUi extends \app_Ui implements \app_ComponentUi
{
    /**
     * Organization type editor
     * @return OrganizationTypeEditor
     */
    public function OrganizationTypeEditor(OrganizationType $record = null)
    {
        return new OrganizationTypeEditor($this->app, $record);
    }
    
    /**
     * Organization table view
     * @return OrganizationTypeTableView
     */
    public function OrganizationTypeTableView()
    {
        return new OrganizationTypeTableView($this->app);
    }
    
    public function tableView()
    {
        return $this->OrganizationTypeTableView();
    }
    
    public function editor(OrganizationType $record = null)
    {
        return $this->OrganizationTypeEditor($record);
    }
}
