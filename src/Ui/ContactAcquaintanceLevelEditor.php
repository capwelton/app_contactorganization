<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactAcquaintanceLevel;

/**
 * Article editor, the field can depend on article type
 * @return \Widget_Form
 */
class ContactAcquaintanceLevelEditor extends \app_Editor
{
    /**
     * @var ContactAcquaintanceLevel
     */
    protected $contactAcquaintanceLevel = null;
    
    
    
    /**
     *
     * @param \Func_App $App
     * @param ContactAcquaintanceLevel $contactAcquaintanceLevel
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, ContactAcquaintanceLevel $contactAcquaintanceLevel = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->contactAcquaintanceLevel = $contactAcquaintanceLevel;
        
        parent::__construct($App, $id, $layout);
        $this->setName('data');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        
        if(isset($contactAcquaintanceLevel)){
            $this->setRecord($contactAcquaintanceLevel);
        }
    }
    
    protected function addFields()
    {
        $this->addItem($this->id());
        $this->addItem($this->name());
        $this->addItem($this->rank());
        $this->addItem($this->color());
    }
    
    protected function id()
    {
        $W = $this->widgets;
        return $W->Hidden(null, 'id');
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addButton(
            $submit = $W->SubmitButton()
            ->setLabel($App->translate('Save'))
            ->validate(true)
        );
        
        $this->addButton(
            $cancel = $W->SubmitButton()
            ->setLabel($App->translate('Cancel'))
        );
        
        if(bab_isAjaxRequest()){
            $submit->setAjaxAction($App->Controller()->ContactAcquaintanceLevel()->save());
            $cancel->setAjaxAction($App->Controller()->ContactAcquaintanceLevel()->cancel());
        }
        else{
            $submit->setAction($App->Controller()->ContactAcquaintanceLevel()->save());
            $cancel->setAction($App->Controller()->ContactAcquaintanceLevel()->cancel());
        }
    }
    
    /**
     *
     * @return \Widget_Item
     */
    protected function name()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Name'),
            $W->LineEdit()
            ->setMandatory(true, $App->translate('The name is mandatory'))
            ->addClass('widget-100pc'),
            'name'
        );
    }
    
    protected function rank()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Rank'),
            $W->NumberEdit()->setMin(0),
            'rank'
        );
    }
    
    protected function color()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Color'),
            $W->ColorPicker(),
            'color'
        );
    }
}