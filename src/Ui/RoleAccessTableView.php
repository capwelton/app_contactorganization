<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Ctrl\RoleAccessController;
use Capwelton\App\ContactOrganization\Set\RoleAccessSet;

/**
 * @method \Func_App    App()
 */
class RoleAccessTableView extends \app_TableModelView
{
    /**
     * @param \Func_App $app
     * @param string $id
     */
    public function __construct(\Func_App $app = null, $id = null)
    {
        parent::__construct($app, $id);
        $this->addClass('depends-app-roleaccess');
    }
    
    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(\app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }
    
    /**
     * @return RoleAccessController
     */
    public function getRecordController()
    {
        return $this->recordController;
    }
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(RoleAccessSet $recordSet)
    {
        $App = $this->App();
        $recordSet->role();
        
        $this->addColumn(
            app_TableModelViewColumn($recordSet->object)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->action)
            );  
        $this->addColumn(
            app_TableModelViewColumn($recordSet->role->name, $App->translate('Role'))
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->criterion)
        );
        $this->addColumn(app_TableModelViewColumn('_actions_', ' ')
            ->setSortable(false)->setExportable(false)
            ->addClass('widget-column-thin')
        );
        
        return $this;
    }
}