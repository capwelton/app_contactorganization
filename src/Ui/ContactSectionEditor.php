<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;
use Capwelton\App\ContactOrganization\Set\Contact;

/**
 *
 * @method \Func_App App()
 */
class ContactSectionEditor extends \app_RecordEditor
{
    public function __construct(\Func_App $app, Contact $record = null, $id = null, $layout = null)
    {
        $this->record = $record;
        parent::__construct($app, $id, $layout);
    }
    
    public function setRecord($record)
    {
        $this->record = $record;
        $this->recordSet = $record->getParentSet();
        
        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues();
            $this->setValues($values, array($name));
        }
        
        return $this;
    }
    
    
    
    protected function _createdBy($customSection)
    {
        return null;
    }
    
    protected function _createdOn($customSection)
    {
        return null;
    }
    
    protected function _modifiedBy($customSection)
    {
        return null;
    }
    
    protected function _modifiedOn($customSection)
    {
        return null;
    }
    
    protected function _address($customSection)
    {
        return null;
    }
    
    protected function _fullname(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Name'),
            $W->Frame(
                null,
                $W->HBoxItems(
                    app_OrmWidget($this->recordSet->title)->setSizePolicy('widget-20pc'),
                    app_OrmWidget($this->recordSet->firstname)->setSizePolicy('widget-40pc'),
                    app_OrmWidget($this->recordSet->lastname)->setSizePolicy('widget-40pc')
                )->setSizePolicy('widget-100pc')
            ),
            $customSection
        );
    }
    
    
    
    protected function _roles(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $roleSet = $App->RoleSet();
        
        $box = $W->Frame()
        ->setName('roles')
        ->setLayout($W->VBoxLayout());
        
        $roles = $roleSet->select()->orderAsc($roleSet->name);
        foreach ($roles as $role) {
            $box->addItem(
                $this->labelledField(
                    $role->name,
                    $W->CheckBox(),
                    $role->id
                )
            );
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Roles'),
            $box,
            $customSection
        );
    }
    
    protected function _userAccount(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $user = null;
        
        if (!empty($this->record->user)) {
            $username = bab_getUserName($this->record->user);
            if (!empty($username)) {
                $user = $this->record->user;
            }
        }
        
        $layout = $W->VBoxLayout()->addClass('icon-left-16 icon-left icon-16x16')->setVerticalSpacing(1,'em');
        $extra = $W->Frame('app_contact_user_extra', $W->VBoxLayout()->setSpacing(.5,'em'))->setName('user_extra');
        $extra->setIconFormat(16, 'left');
        
        if (isset($user)) {
            $layout->addItem(
                $this->userPicker($user)
            );
            $layout->additem($extra);
            
            $entry = bab_getUserInfos($user);
            
            $nickname = $W->FlowItems(
                $W->Label($App->translate('User email'))->colon(),
                $W->Label(bab_getUserEmail($user))
            )->setHorizontalSpacing(.5,'em');
            
            $ctrl = $App->Controller()->Contact();
            
            $changepassword = $W->Link(
                $App->translate('Edit user password'),
                $ctrl->editUserPassword($this->record->id)
            )->setOpenMode(\Widget_Link::OPEN_DIALOG)->addClass('widget-actionbutton')->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT);
            
//             $disabled = $W->Link(
//                 $App->translate('Disable user account'),
//                 $ctrl->editUserPassword($this->record->id)
//             )->setOpenMode(\Widget_Link::OPEN_DIALOG)->addClass('widget-actionbutton')->setIcon(\Func_Icons::ACTIONS_LIST_REMOVE_USER);
            
            
            $extra->addItem($nickname);
            
            $extra->addItem($disabled);
            $extra->addItem($changepassword);
            $extra->addItem($this->UserRoles());
            
        }
        else {
            $userType = $this->userType();
            $userPicker = $this->userPicker($user);
            $layout->addItem($userType);
            $layout->addItem($userPicker);
            $layout->additem($extra);
            
            $userType->setAssociatedDisplayable($userPicker, array('2'));
            $userType->setAssociatedDisplayable($extra, array('3'));
            
            // user creation form
            
            $loginFormItem = $this->labelledField(
                $App->translate('User login'),
                $W->LineEdit()->setSize(30)->setAutoComplete(false),
                'nickname'
            );
            
            $passwordFormItem = $this->labelledField(
                $App->translate('Password'),
                $W->LineEdit()->setSize(30)->obfuscate(true)->setAutoComplete(false),
                'password'
            );
            
            $notify = $this->labelledField(
                $App->translate('Notify the user about is account'),
                $W->CheckBox(),
                'notify'
            );
            
            $extra
            ->addItem($loginFormItem)
            ->addItem($passwordFormItem)
            ->addItem($notify);
        }
        
        return $layout;
    }
    
    protected function userType()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $W->RadioSet()
        ->addOption(Contact::USER_TYPE_NO_ACCOUNT, $App->translate('No user account'))
        ->addOption(Contact::USER_TYPE_EXISTING_ACCOUNT, $App->translate('Link an existing user account to contact'))
        ->addOption(Contact::USER_TYPE_NEW_ACCOUNT, $App->translate('Create a new user account'))
        ->setName('usertype')
        ->setValue(Contact::USER_TYPE_NO_ACCOUNT);
    }
    
    protected function userPicker($user = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        if (empty($user)) {
            $label = $App->translate('Attach a user account');
            $description = $App->translate('An existing user account can be attached to this contact');
        } else {
            $label = $App->translate('User account');
            $description = $App->translate('The contact can login into the application with this account');
        }
        
        
        return $this->labelledField(
            $label,
            $W->UserPicker(),
            'user',
            $description
        );
    }
}