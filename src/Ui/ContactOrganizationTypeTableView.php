<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactOrganizationType;
use Capwelton\App\ContactOrganization\Set\ContactOrganizationTypeSet;

class ContactOrganizationTypeTableView extends \app_TableModelView
{
    protected $ctrl;
    
    public function __construct(\Func_App $app = null, $id = null)
    {
        $this->ctrl = $app->Controller()->ContactOrganizationType();
        parent::__construct($app, $id);
    }
    
    public function addDefaultColumns(ContactOrganizationTypeSet $contatOrganizationTypeSet = null)
    {
        if (!isset($contatOrganizationTypeSet)) {
            $contatOrganizationTypeSet = $this->getRecordSet();
        }
        
        $this->addColumn(app_TableModelViewColumn($contatOrganizationTypeSet->name)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($contatOrganizationTypeSet->description)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn('_actions_', ' ')->setVisible(true)->setSortable(false)->setExportable(false)->addClass('widget-column-thin'));
    }
    /**
     * @param ContactOrganizationType    $record
     * @param string        $fieldPath
     * @return \Widget_Item
     */
    protected function computeCellContent(ContactOrganizationType $record, $fieldPath)
    {
        $W = bab_Widgets();
        
        switch ($fieldPath) {
            case 'name':
                return $W->Link(
                    $record->name,
                    $this->ctrl->edit($record->id)
                )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC)->setOpenMode(\Widget_Link::OPEN_DIALOG);
                break;
            case '_actions_':
                $box = $W->HBoxItems();
                $box->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC);
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '',
                            $this->ctrl->confirmDelete($record->id)
                        )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    );
                }
                return $box;
                break;
        }
        
        return parent::computeCellContent($record, $fieldPath);
    }
}