<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Contact;
use Capwelton\App\ContactOrganization\Set\ContactSet;

class ContactCardFrame extends \app_CardFrame
{
    /**
     * @var ContactSet
     */
    protected $contactSet = null;
    
    /**
     * @var Contact
     */
    protected $contact = null;
    
    /**
     * @param \Func_App $App
     * @param Contact $contact
     * @param string $id
     */
    public function __construct(\Func_App $App, Contact$contact, $id = null)
    {
        $W = bab_Widgets();
        $cardLayout = $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
        
        parent::__construct($App, $id, $cardLayout);
        
        $App = $this->App();
        
        
        $this->contact = $contact;
        $this->contactSet = $this->contact->getParentSet();
        
        $contactCtrl = $App->Controller()->Contact();
        
        $cardLayout->addItem(
            $W->HBoxItems(
                $W->Link(
                    $App->Ui()->ContactAvatar($this->contact)->setSizePolicy('minimum'),
                    $contactCtrl->display($this->contact->id)
                ),
                $W->VBoxItems(
                    $W->VBoxItems(
                        $W->Link(
                            $this->contact->getFullName(),
                            $contactCtrl->display($this->contact->id)
                        )->addClass('crm-card-title', 'widget-strong')
                    ),
                    $W->VBoxItems(
                        $this->email(),
                        $this->phone(),
                        $this->mobile(),
                        $this->address()
                    )->addClass('crm-small')
                )->setVerticalSpacing(0.5, 'em')
                ->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
            )->setSpacing(1, 'em')
        )->setIconFormat(16, 'left');
    }
    
    
    protected function phone()
    {
        $phone = $this->contact->getMainPhoneRecord();
        if($phone && !$phone->isPersonal()) {
            return $phone->displayPhone();
        }
        return null;
    }
    
    protected function mobile()
    {
        $phone = $this->contact->getMainPersonalPhoneRecord();
        if($phone) {
            return $phone->displayPhone();
        }
        return null;
    }
    
    protected function email()
    {
        $W = bab_Widgets();
        $email = $this->contact->email;
        if (empty($email)) {
            return null;
        }
        return $W->Link(
            $email,
            'mailto:' . $email
        )->setIcon(\Func_Icons::OBJECTS_EMAIL);
    }
    
    protected function address()
    {
        $W = bab_Widgets();
        $html = array();
        
        if($this->contact == null){
            return null;
        }
        $address = $this->contact->address();
        if (!$address || $address->isEmpty()) {
            $mainPosition = $this->contact->mainPosition();
            if ($mainPosition) {
                $mainOrganization = $mainPosition->organization();
                if(isset($mainOrganization)){
                    $address = $mainOrganization->address();
                    $html[] = '<i>(' . bab_toHtml($mainOrganization->name) . ')</i>';
                }
            }
        }
        if (!empty($address->street)) {
            $html[] = bab_toHtml($address->street, BAB_HTML_BR);
        }
        $line2 = array();
        if (!empty($address->postalCode)) {
            $line2[] = bab_toHtml($address->postalCode);
        }
        if (!empty($address->city)) {
            $line2[] = $address->city;
        }
        if (!empty($address->cityComplement)) {
            $line2[] = bab_toHtml($address->cityComplement);
        }
        $line2 = implode(' ', $line2);
        if (!empty($line2)) {
            $line2 = array($line2);
        } else {
            $line2 = array();
        }
        if (!empty($address->country->name_fr)) {
            $line2[] = bab_toHtml($address->country->name_fr);
        }
        $html[] = implode(' - ', $line2);
        $html = implode('<br>', $html);
        return $W->Html(
            '<div style="display: inline-block; vertical-align: top">' . $html. '</div>'
        )->addClass('icon', "objects-map-marker", 'widget-nowrap');
    }
}