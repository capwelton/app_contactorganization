<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactAcquaintance;

class ContactAcquaintanceEditor extends \app_Editor
{
    
    protected $contactAcquaintance;
    protected $displayNotes = true;
    
    /**
     * @param \Func_App $App
     * @param ContactAcquaintance $contactAcquaintance
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, ContactAcquaintance $contactAcquaintance, $id = null, \Widget_Layout $layout = null)
    {
        $this->contactAcquaintance = $contactAcquaintance;
        
        parent::__construct($App, $id, $layout);
        $this->setName('data');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        
        $this->setRecord($contactAcquaintance);
    }
    
    protected function addFields()
    {
        $this->addItem($this->id());
        $this->addItem($this->contactKnowing());
        $this->addItem($this->level());
        if($this->displayNotes && $this->App()->getComponentByName('Note')){
            $this->addItem(
                $this->notes()
            );
        }
    }
    
    protected function id()
    {
        $W = $this->widgets;
        return $W->Hidden(null, 'id');
    }
    
    protected function contactKnowing()
    {
        $App = $this->App();
        $W = $this->widgets;
        $suggest = $App->Ui()->SuggestContact(null, array($this->contactAcquaintance->contactKnown));
        
        if(isset($this->contactAcquaintance->contactKnowing) && !empty($this->contactAcquaintance->contactKnowing) && $contactKnowing = $this->contactAcquaintance->contactKnowing()){
            $suggest->addOption(
                $contactKnowing->id,
                $contactKnowing->getFullname()
            );
        }
        else{
            $suggest->addOption(
                '',
                ''
            );
        }
        
        return $W->LabelledWidget(
            $App->translate('Contact knowing'),
            $suggest->setMandatory(),
            'contactKnowing'
        );
    }
    
    protected function level()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $set = $App->ContactAcquaintanceLevelSet();
        $levels = $set->select()->orderAsc($set->rank)->orderAsc($set->name);
        
        $options = array();
        foreach ($levels as $level){
            $options[$level->id] = $level->name;
        }
        
        return $W->LabelledWidget(
            $App->translate('Level'),
            $W->Select()->setOptions($options),
            'level'
        );
    }
    
    protected function notes()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $noteC = $this->App()->getComponentByName('Note');
        
        $ref = $this->contactAcquaintance->getRef();
        
        $box = $W->Section(
            $noteC->translate('Notes'),
            $App->Controller()->Note(false)->listFor($ref)
        )->addClass('box yellow');
        
        $contextMenu = $box->addContextMenu();
        
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->Note()->add($ref)
            )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy(\Func_Icons::ICON_LEFT_16 . ' pull-up')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->setTitle($App->translate('Add note'))
        );
        
        return $box;
    }
}