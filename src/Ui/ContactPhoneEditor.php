<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactPhone;

class ContactPhoneEditor extends \app_Editor
{
    /**
     * @var \Widget_InputWidget
     */
    private $phoneWidget;
    
    /**
     * @param Func_App App
     * @param ContactPhone $contactPhone
     * @param int $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, ContactPhone $contactPhone = null, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($App, $id, $layout);
    }
    
    protected function phone()
    {
        $phoneField = $this->record->getParentSet()->phone;
        
        $this->phoneWidget = $phoneField->getWidget();
        $this->phoneWidget->allowDropdown(false);
        $this->phoneWidget->addClass('widget-100pc');
        
        $App = $this->App();
        return $this->labelledField(
            $App->translate('Phone'),
            $this->phoneWidget,
            array('phone')
        );
    }
    
    protected function title()
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $titleWidget = $W->LineEdit();
        if ($this->record->isPersonal()) {
            $titleWidget->setPlaceHolder($App->translate('Personal'));
        }
        
        return $this->labelledField(
            $App->translate('Title'),
            $titleWidget,
            'title'
        );
    }
    
    /**
     * Add fields into form
     * @return self
     */
    public function prependFields()
    {
        return $this;
    }
    
    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();
        
        $titleItem = $this->title();
        $this->addItem($this->phone());
        $this->addItem($titleItem);
        
        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
            $this->addItem(
                $W->Hidden()->setName('rank')->setValue($this->record->rank)
            );
            $this->addItem(
                $W->Hidden()->setName('contact')->setValue($this->record->contact)
            );
            $this->addItem(
                $W->Hidden()->setName('contactOrganization')->setValue($this->record->contactOrganization)
            );
        }
        
        return $this;
    }
}