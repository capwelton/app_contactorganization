<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactOrganizationType;

class ContactOrganizationTypeEditor extends \app_Editor
{
    /**
     * @var ContactOrganizationType
     */
    protected $contactOrganizationType = null;    
    
    /**
     *
     * @param \Func_App $App
     * @param ContactOrganizationType $contactOrganizationType
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, ContactOrganizationType $contactOrganizationType = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->contactOrganizationType = $contactOrganizationType;
        
        parent::__construct($App, $id, $layout);
        $this->setName('contactOrganizationType');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        
        if (isset($contactOrganizationType)) {
            $this->setHiddenValue('contactOrganizationType[id]', $contactOrganizationType->id);
            $this->setValues($contactOrganizationType, array('contactOrganizationType'));
        }
    }
    
    protected function addFields()
    {
        $this->addItem($this->name());
        $this->addItem($this->description());
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addButton(
            $submit = $W->SubmitButton()
            ->setLabel($App->translate('Save'))
            ->validate(true)
        );
        
        $this->addButton(
            $cancel = $W->SubmitButton()
            ->setLabel($App->translate('Cancel'))
        );
        
        if(bab_isAjaxRequest()){
            $submit->setAjaxAction($App->Controller()->ContactOrganizationType()->save());
            $cancel->setAjaxAction($App->Controller()->ContactOrganizationType()->cancel());
        }
        else{
            $submit->setAction($App->Controller()->ContactOrganizationType()->save());
            $cancel->setAction($App->Controller()->ContactOrganizationType()->cancel());
        }
    }
    
    /**
     *
     * @return \Widget_Item
     */
    protected function name()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Name'),
            $W->LineEdit()
            ->setMandatory(true, $App->translate('The name is mandatory'))
            ->addClass('widget-100pc'),
            'name'
            );
    }
    
    /**
     *
     * @return \Widget_Item
     */
    protected function description()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Description'),
            $W->TextEdit()
            ->addClass('widget-100pc'),
            'description'
        );
    }
    
    public function setValues($contactOrganizationType, $namePathBase = array())
    {
        if ($contactOrganizationType instanceof ContactOrganizationType) {
            $values = $contactOrganizationType->getFormOutputValues();
            $this->setValues(array('contactOrganizationType' => $values));
        } else {
            parent::setValues($contactOrganizationType, $namePathBase);
        }
    }
}