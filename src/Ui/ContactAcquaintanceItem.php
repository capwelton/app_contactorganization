<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactAcquaintance;
use Capwelton\App\ContactOrganization\Set\ContactAcquaintanceSet;
use Capwelton\App\Note\Set\NoteSet;

/**
 * @return ContactAcquaintanceItem
 * @method self addItem(Widget_Displayable_Interface $item = null)
 */
class ContactAcquaintanceItem extends \app_UiObject
{
    /**
     * @var ContactAcquaintance
     */
    protected $contactAcquaintance;
    
    /**
     * @var ContactAcquaintanceSet
     */
    protected $set;
    
    public function __construct(\Func_App $App, ContactAcquaintance $record, \Widget_Layout $layout = null, $itemId = null)
    {
        parent::__construct($App);
        
        $this->contactAcquaintance = $record;
        $this->set = $record->getParentSet();
        
        $W = bab_Widgets();
        
        $this->setInheritedItem($W->Frame($itemId, $layout));
        
        $this->computeContent();
        $this->addClass('app-contactorganization-contactacquaintanceitem');
    }
    
    public function computeContent()
    {
        $W = bab_Widgets();
        
        $actionsBox = $W->FlowItems();
        $actionsBox->addClass('widget-nowrap pull-right');
        $actionsBox->setIconFormat(16, 'left');
        
        $record = $this->contactAcquaintance;
        $App = $this->App();
        
        $ctrl = $App->Controller()->ContactAcquaintance();
        
        $menuItems = array();
        if ($record->isUpdatable()) {
            $menuItems[] = $W->Link(
                '',
                $ctrl->edit($record->id)
            )->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
            ->setTitle($App->translate('Edit'));
        }
        if ($record->isDeletable()) {
            $menuItems[] = $W->Link(
                '',
                $ctrl->confirmDelete($record->id)
            )->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
            ->setTitle($App->translate('Delete'));
        }
        
        foreach ($menuItems as $menuItem){
            $actionsBox->addItem($menuItem);
        }
        
        $listItem = $W->VBoxItems(
            $W->HBoxItems(
                $App->Ui()->ContactCardFrame($record->contactKnowing()),
                $actionsBox
            )->addClass('widget-100pc')
            ->setVerticalAlign('middle')
            ->setHorizontalSpacing(8, 'px')
        );
        
        $noteC = $App->getComponentByName('Note');
        if($noteC){
            /* @var $noteSet NoteSet */
            $noteSet = $App->NoteSet();
            
            $conditions = array();
            $conditions[] = $noteSet->isTargetOf($record);
            $conditions[] = $noteSet->isReadable();
            $notes = $noteSet->select(
                $noteSet->all($conditions)
            );
            
            $nbNotes = $notes->count();
            if($nbNotes > 0){
                $listItem->addItem(
                    $W->Link(
                        sprintf($App->translate('Show %d note', 'Show %d notes', $nbNotes), $nbNotes),
                        $App->Controller()->Note()->listFor($record->getRef())
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                );
            }
        }
        
        $this->addItem($listItem);
    }
}