<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Role;
use Capwelton\App\ContactOrganization\Set\RoleSet;
use Capwelton\App\ContactOrganization\Set\ContactRoleSet;

/**
 * @method \Func_App    App()
 */
class RoleModelView extends \app_TableModelView
{
    
    private $componentUi;
    
    protected $contactRoleSet;

    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App, $id = null)
    {
        parent::__construct($App, $id);
        $this->componentUi = $App->Ui();

        $this->addClass("depends-Role");
        $this->addClass("depends-{$App->classPrefix}Role");
    }


    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(\app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }
    
    protected function getContactRoleSet()
    {
        if(!isset($this->contactRoleSet)){
            $App = $this->App();
            $this->contactRoleSet = $App->ContactRoleSet();
        }
        return $this->contactRoleSet;
    }

    /**
     * {@inheritDoc}
     * @see \app_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(RoleSet $roleSet = null)
    {
        $App = $this->App();

        if (!isset($roleSet)) {
            $roleSet = $this->getRecordSet();
        }
        
        $this->addColumn(
            app_TableModelViewColumn($roleSet->name, $App->translate('Name'))
        );
        $this->addColumn(
            app_TableModelViewColumn($roleSet->description, $App->translate('Description'))
        );
        $this->addColumn(
            app_TableModelViewColumn('nbContact', $App->translate('Contacts'))
        );
        $this->addColumn(
            app_TableModelViewColumn('_actions_', ' ')
                ->setSelectable(true, '[Actions]')
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin')
        );

    }


    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initRow()
     */
    public function initRow(\ORM_Record $record, $row)
    {
        $this->addRowClass($row, 'widget-actions-target');
        return parent::initRow($record, $row);
    }


    /**
     *
     */
    protected function getDisplayAction(Role $record)
    {
        return $this->getRecordControllerProxy()->edit($record->id);
    }


    /**
     * @param Role    $record
     * @param string        $fieldPath
     * @return \Widget_Item
     */
    protected function computeCellContent(Role $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {
            case 'nbContact':
                $contactRoleSet = $this->getContactRoleSet();
                return $W->Label($contactRoleSet->select($contactRoleSet->role->is($record->id))->count());
            case 'name':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $record->name,
                    $displayAction
                )->setOpenMode(\Widget_Link::OPEN_DIALOG);
        }

        return parent::computeCellContent($record, $fieldPath);
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        $recordSet = $this->getRecordSet();
//         $App = $this->App();
        $conditions = array(
            $recordSet->isReadable()
        );

        $conditions[] = parent::getFilterCriteria($filter);

//         if (isset($filter['search']) && !empty($filter['search'])) {
//             $mixedConditions = array(
//                 $recordSet->name->contains($filter['search']),
//             );
//             $conditions[] = $recordSet->any($mixedConditions);
//         }
        
        return $recordSet->all();
    }
}