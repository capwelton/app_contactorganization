<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Contact;
use Capwelton\App\ContactOrganization\Set\Organization;

/**
 * @return ContactEditor
 */
class ContactEditor extends \app_Editor
{
    
    private $imagePicker;
    
    /**
     * @param \Func_App App
     * @param Contact $contact
     * @param Organization $organization
     * @param int $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, Contact $contact = null, Organization $organization = null, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($App, $id, $layout);
    }
    
    
    protected function responsible()
    {
        $App = $this->App();
        $responsibleFormItem = $this->labelledField(
            $App->translate('Responsible contact'),
            $this->suggestResponsibleContact = $App->Ui()->SuggestContact()
            ->setMinChars(0),
            'responsible'
        );
        
        return $responsibleFormItem;
    }
    
    
    protected function responsibleOrganization()
    {
        $App = $this->App();
        $responsibleOrganizationFormItem = $this->labelledField(
            $App->translate('Responsible organization'),
            $this->suggestResponsibleOrganization = $App->Ui()->SuggestOrganization()
            ->setMinChars(0)
            ->addClass('widget-100pc'),
            'responsibleOrganization'
        );
        
        return $responsibleOrganizationFormItem;
    }
    
    /**
     * experimental, add google search to the imagepicker
     *
     */
    public function googleSuggest()
    {
        $searchable = array();
        foreach($this->getFields() as $widget) {
            
            if ('firstname' === $widget->getName() || 'lastname' === $widget->getName()) {
                $searchable[] = $widget;
            }
        }
        return $searchable;
    }
    
    protected function addFields()
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = $this->widgets;
        
        $Ui->includeAddress();
        
        $this->contactSet = $App->ContactSet();
        $this->contactSet->address();
        
        $this->addItem(
            $W->Section(
                $App->translate('Contact information'),
                $W->VBoxItems(
                    $W->HBoxItems(
                        $this->Title(),
                        $this->Firstname(),
                        $this->Lastname()
                    )->setHorizontalSpacing(1, 'em')
                    ->setVerticalAlign('bottom'),
                    $W->HBoxItems(
                        $this->PrivateField(),
                        $this->BusinessPhone(),
                        $this->MobilePhone(),
                        $this->Fax()
                    )->setHorizontalSpacing(2, 'em')
                    ->setVerticalAlign('bottom'),
                    $W->HBoxItems(
                        $this->Linkedin()
                    )->setHorizontalSpacing(2, 'em'),
                    $this->Email()
                )->setVerticalSpacing(1, 'em')
            )->setFoldable(true)
        );
        
        $organizationsSection = $this->OrganizationsSection();
        $organizationsSection->addClass('box', 'transparent');
        
        $this->addItem($organizationsSection);
        $organizationsSection->addItem(
            $W->FlowItems(
                $this->responsibleOrganization()
                ->setSizePolicy('widget-50pc'),
                $this->responsible()
                ->setSizePolicy('widget-50pc')
            )->setHorizontalSpacing(1, 'em')
        );
        
        $this->suggestResponsibleContact->setRelatedOrganization($this->suggestResponsibleOrganization);
        
        $addressFormItem = $W->VBoxItems(
            $Ui->AddressEdit()
            ->setName('address')
        );
        
        $this->addItem(
            $W->Section(
                $App->translate('Address'),
                $addressFormItem
            )->setFoldable(true)
            ->addClass('box', 'transparent')
        );
        
    }
    
    protected function Email()
    {
        $W = bab_Widgets();
        
        return $this->labelledField(
            $this->App()->translate('Email address'),
            $W->EmailLineEdit()
            ->addClass('crm-fullwidth')
            ->setSize(50),
            'email'
        );
    }
    
    protected function BusinessPhone()
    {
        $W = bab_Widgets();
        
        return $this->labelledField(
            $this->App()->translate('Phone'),
            $W->TelLineEdit()
            ->setTelType(\Widget_TelLineEdit::BUSINESS)
            ->setSize(15),
            'phone'
        );
    }
    
    protected function MobilePhone()
    {
        $W = bab_Widgets();
        
        return $this->labelledField(
            $this->App()->translate('Mobile'),
            $W->TelLineEdit()
            ->setTelType(\Widget_TelLineEdit::MOBILE)
            ->setSize(15),
            'mobile'
        );
    }
    
    protected function Fax()
    {
        $W = bab_Widgets();
        
        return $this->labelledField(
            $this->App()->translate('Fax'),
            $W->TelLineEdit()
            ->setTelType(\Widget_TelLineEdit::FAX)
            ->setSize(15),
            'fax'
        );
    }
    
    public function setValues($contact, $namePathBase = array())
    {
        if ($contact instanceof Contact) {
            
            $contactValues = $contact->getValues();
            
            if (null !== $contact->address()->id) {
                $this->setHiddenValue('contact[address]', $contact->address()->id);
            }
            
            parent::setValues($contactValues, $namePathBase);
            
        } else {
            parent::setValues($contact, $namePathBase);
        }
    }
}