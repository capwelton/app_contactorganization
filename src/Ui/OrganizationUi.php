<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Organization;

class OrganizationUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * @param Organization $organization
     * @return OrganizationCardFrame
     */
    public function OrganizationCardFrame(Organization $organization , $openMode = null)
    {
        return new OrganizationCardFrame($this->app, $organization, null, $openMode);
    }
    
    /**
     * Organization cards view
     * @return OrganizationCardsView
     */
    public function OrganizationCardsView()
    {
        return new OrganizationCardsView($this->app);
    }
    
    /**
     * Organization editor
     * @return OrganizationEditor
     */
    public function OrganizationEditor(Organization $organization = null)
    {
        return new OrganizationEditor($this->app, $organization);
    }
    
    /**
     * @param Organization $organization
     * @return OrganizationRecordView
     */
    public function OrganizationFullFrame(Organization $organization)
    {
        $organizationFullFrame = new OrganizationRecordView($this->app);
        $organizationFullFrame->setRecord($organization);
        return $organizationFullFrame;
    }
    
    /**
     * Generates and returns an image representing of the specified organization's logo.
     *
     * @param Organization	    $organization	The organization to generate a photo of.
     * @param int     			$width			Width in px.
     * @param int     			$height			Height in px.
     * @param bool    			$border			True to add a 1px light-grey border around the image.
     
     * @return \Widget_Frame
     */
    public function OrganizationLogo(Organization $organization, $width, $height, $border = false)
    {
        $W = bab_Widgets();
        $T = @\bab_functionality::get('Thumbnailer');
        
        $image = $W->Image();
        if ($T) {
            $logo = $organization->getLogoPath($this->app);
            if (empty($logo)) {
                return $image;   
            }
            
            $T->setSourceFile($logo->toString());
            $T->setResizeMode(\Func_Thumbnailer::KEEP_ASPECT_RATIO);
            
            if ($border) {
                $padWidth = min(array(2, round(min(array($width, $height)) / 24)));
                $T->setBorder(1, '#cccccc', $padWidth, '#ffffff');
            }
            $imageUrl = $T->getThumbnail($width, $height);
            $image->setUrl($imageUrl);
        }
        
        if ($organization->name) {
            $image->setTitle($organization->name);
        }
        
        $image->addClass('app-organization-image app-element-image small');
        
        return $image;
    }
    
    /**
     * Organization cards view
     * @return OrganizationMapView
     */
    public function OrganizationMapView()
    {
        return new OrganizationMapView($this->app);
    }
    
    
    /**
     * Organization editor
     * @return OrganizationSectionEditor
     */
    public function OrganizationSectionEditor(Organization $record = null)
    {
        return new OrganizationSectionEditor($this->app, $record);
    }
    
    /**
     * Organization table view
     * @return OrganizationTableView
     */
    public function OrganizationTableView()
    {
        return new OrganizationTableView($this->app);
    }
    
    /**
     * @return SuggestContact
     */
    public function SuggestOrganization($allowCreation = false, $id = null)
    {
        $suggest = new SuggestOrganization($this->app, $id);
        $suggest->setDataSource($this->app->Controller()->Organization()->search($allowCreation ? 'y' : 'n'));
        
        return $suggest;
    }
    
    /**
     * A input widget to suggest a parent for an organization.
     * The suggest does not propose the specified child organization or its descendants.
     *
     * @return SuggestOrganization
     */
    public function SuggestParentOrganization($childOrganizationId = null, $id = null)
    {
        $suggest = $this->SuggestOrganization(false, $id);
        $suggest->setDataSource($this->app->Controller()->Organization()->search('n', array($childOrganizationId)));
        
        return $suggest;
    }
    
    public function tableView()
    {
        return $this->OrganizationTableView();
    }
    
    public function editor(Organization $record = null)
    {
        return $this->OrganizationEditor($record);
    }
}
