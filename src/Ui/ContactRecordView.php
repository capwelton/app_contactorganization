<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Contact;

/**
 *
 * @property Contact $contact
 */
class ContactRecordView extends \app_RecordView
{    
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->addSections($this->getView());
        
        return parent::display($canvas);
    }
    
    protected function _fullname(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $item = $W->Label($this->record->firstname . ' ' . $this->record->lastname);
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Name'),
            $item,
            $customSection
        );
    }
    
    protected function _userAccount(\app_CustomSection $customSection, $label = null)
    {
        if (!bab_isUserAdministrator()) {
            return null;
        }
        
        $W = bab_Widgets();
        $App = $this->App();
        
        $contactCtrl = $App->Controller()->Contact();
        
        $menu = $W->Menu(null, $W->VBoxItems());
        $menu->setIconFormat(16, 'left');
        $menu->addClass(\Func_Icons::ICON_LEFT_16);
        
        if (!$this->record->user) {
            $item = $W->Html('<i>'.$App->translate('This contact has no user account').'</i>');
            $menu->addItem(
                $W->Link(
                    $App->translate('Create user account'),
                    $contactCtrl->editUserAccount($this->record->id)
                )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
        } else {
            $userInfo = bab_getUserInfos($this->record->user);
            $item = $W->Html('<i>'.sprintf($App->translate('This contact can log in with the nickname %s').'<i>', $userInfo['nickname']));
            $menu->addItem(
                $W->Link(
                    $App->translate('Edit user password'),
                    $contactCtrl->editUserPassword($this->record->id)
                )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
            $menu->addItem(
                $W->Link(
                    $App->translate('Edit user roles'),
                    $contactCtrl->editUserRoles($this->record->id)
                )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
            $menu->addItem(
                $W->Link(
                    $App->translate('Disable user account')
                )->setIcon(\Func_Icons::ACTIONS_LIST_REMOVE_USER)
            );
        }
        
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('User account'),
            $W->FlowItems(
                $item,
                $menu
            )->addClass('widget-actions-target'),
            $customSection
        );
    }
    
    protected function _email(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $email = $this->record->email;
        if (empty($email)) {
            return null;
        }
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Email'),
            $W->Link(
                $email,
                'mailto:' . $email
            ),
            $customSection
        );
    }
    
    protected function _notes(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->Note()->add($this->record->getRef())
            )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy(\Func_Icons::ICON_LEFT_16 . ' pull-up')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->setTitle($App->translate('Add note'))
        );
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Notes'),
            $App->Controller()->Note(false)->listFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _address(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $address = $this->record->address();
        $organizationLabel = $W->Label('');
        if (!$address || $address->isEmpty()) {
            if ($mainPosition = $this->record->mainPosition()) {
                if($mainOrganization = $mainPosition->organization()){
                    $organizationLabel = $W->Link(
                        '(' . $mainOrganization->name . ')',
                        $App->Controller()->Organization()->display($mainOrganization->id)
                    );
                    $address = $mainOrganization->address();
                }
            }
        }
        if (isset($address)) {
            $addressBox = $W->VBoxItems(
                $organizationLabel,
                $W->Label($address->street),
                $W->FlowItems(
                    $W->Label($address->postalCode),
                    $W->Label($address->city),
                    $W->Label($address->cityComplement)
                )->setHorizontalSpacing(1, 'ex'),
                $W->Label($address->country() ? $address->country()->getName() : '')
            )->addClass('SmallFont');
                
            return $this->labelledWidget(
                !empty($label) ? $label : $App->translate('Address'),
                $addressBox,
                $customSection
            );
        }
        return $W->VBoxItems();
    }
    
    
    protected function _photo(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Photo'),
            $App->Controller()->Contact(false)->photo($this->record->id),
            $customSection
        );
    }
    
    
    protected function _organizationsAndPositions(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        $box = $App->Controller()->ContactOrganization(false)->listForContact($this->record->id);
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Organizations'),
            $box,
            $customSection
        );
    }
    
    protected function _contactPhones(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        $box = $App->Controller()->ContactPhone(false)->listForContact($this->record->id);
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Personal phone numbers'),
            $box,
            $customSection
        );
    }
    
    
    protected function _roles(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->Frame()
        ->setName('roles')
        ->setLayout($W->VBoxLayout());
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Roles'),
            $box,
            $customSection
        );
    }
    
    protected function _tags(\app_CustomSection $customSection = null, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $tagComponent = $App->getComponentByName('Tag');
        if(!$tagComponent){
            return null;
        }
        
        $tagsDisplay = $App->Ui()->TagsListForRecord($this->record, $App->Controller()->Contact()->displayListForTag());
        $editor = new \app_Editor($App);
        $editor->addItem($App->Controller()->Tag(false)->SuggestTag()->setName('label'));
        
        $editor->setName('tag');
        $editor->setSaveAction($App->Controller()->Tag()->link(),$App->translate("Add"));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('to', $this->record->getRef());
        $editor->isAjax = true;
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Tags'),
            $W->FlowItems(
                $tagsDisplay,
                $editor
            )->setHorizontalSpacing(0.5, 'em'),
            $customSection
        );
    }
    
    protected function _teams(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->Team()->add($this->record->getRef())
            )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy('pull-up')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->setTitle($App->translate('Add team'))
        );
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Teams'),
            $App->Controller()->Team(false)->listFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _attachments(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        if(!$App->getComponentByName('Attachment')){
            return null;
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Documents'),
            $App->Controller()->Attachment(false)->_attachments($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _acquaintances(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->ContactAcquaintance()->add($this->record->id)
            )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy('pull-up')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->setTitle($App->translate('Add acquaintance'))
        );
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Who knows this contact'),
            $App->Controller()->ContactAcquaintance(false)->listFor($this->record->id),
            $customSection
        );
    }
    
    protected function _pendingTasks(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $taskC = $App->getComponentByName('Task');
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        if(!$contextMenu->getById($section->getId().'_addTaskLink')){
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $App->Controller()->Task()->add($this->record->getRef()),
                    $section->getId().'_addTaskLink'
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($taskC->translate('Add task'))
            );
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $taskC->translate('Pending tasks'),
            $App->Controller()->Task(false)->_pendingTasksFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _completedTasks(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $taskC = $App->getComponentByName('Task');
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        if(!$contextMenu->getById($section->getId().'_addTaskLink')){
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $App->Controller()->Task()->add($this->record->getRef()),
                    $section->getId().'_addTaskLink'
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($taskC->translate('Add task'))
            );
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $taskC->translate('Completed tasks'),
            $App->Controller()->Task(false)->_completedTasksFor($this->record->getRef()),
            $customSection
        );
    }
}
