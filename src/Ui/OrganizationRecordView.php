<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\Organization;
use Capwelton\App\ContactOrganization\Ctrl\OrganizationController;

/**
 * OrganizationRecordView
 *
 * @method \Func_App    App()
 * @property Organization $record
 */
class OrganizationRecordView extends \app_RecordView
{
    /**
     * @var OrganizationController
     */
    private $ctrlNoProxy;
    /**
     * @var OrganizationController
     */
    private $ctrl;
    
    public function __construct(\Func_App $App, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($App, $id, $layout);
        $this->ctrlNoProxy = $App->Controller()->Organization(false);
        $this->ctrl = $this->ctrlNoProxy->proxy();
    }
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->addSections($this->getView());
        
        return parent::display($canvas);
    }
    
    
    protected function _paymentTerm(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $widget = $W->Label('');
        
        $paymentTerm = $this->record->paymentTerm();
        if($paymentTerm) {
            $widget = $W->Label(sprintf('%s (%s)', $paymentTerm->name, $paymentTerm->description));
        }
        $W = bab_Widgets();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Payment term'),
            $widget,
            $customSection
        );
    }
    
    protected function _incoterms(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $widget = $W->Label('');
        
        $incoterms = $this->record->incoterms();
        if($incoterms) {
            $widget = $W->Label(sprintf('%s (%s)', $incoterms->acronym, $incoterms->description));
        }
        $W = bab_Widgets();
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Incoterms'),
            $widget,
            $customSection
        );
    }
    
    protected function _currency(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
       
        $widget = $W->Label('');
        
        $currency = $this->record->currency();
        if($currency) {
            $widget = $W->Label(sprintf('%s (%s)', $currency->name_fr, $currency->symbol));
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Currency'),
            $widget,
            $customSection
        );
    }
    
    protected function _responsible(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $widget = $W->Label('');
        
        $contact = $this->record->responsible();
        if ($contact) {
            $widget = $App->Ui()->ContactCardFrame($contact);
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Responsible contact'),
            $widget,
            $customSection
        );
    }
    
    
    protected function _commercialReferralContact(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $widget = $W->Label('');
        
        $contact = $this->record->commercialReferralContact();
        if ($contact) {
            $widget = $App->Ui()->ContactCardFrame($contact);
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Commercial referral contact'),
            $widget,
            $customSection
        );
    }
    
    protected function _contacts(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $contactCtrl = $App->Controller()->Contact();
        $contactOrganizationCtrlNoProxy = $App->Controller()->ContactOrganization(false);
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $contactCtrl->displayList(array("mainPosition/organization/name"=>$this->record->name))
            )->addClass('widget-actionbutton section-button')
            ->setIcon(\Func_Icons::APPS_CATALOG)
            ->setTitle($App->translate("List all contacts for this organization"))
            ->setDialogClass($customSection->classname)
        );
        
        $contextMenu->addItem(
            $W->Link(
                '',
                $contactOrganizationCtrlNoProxy->proxy()->addForOrganization($this->record->id)
            )->addClass('widget-actionbutton section-button')
            ->setOpenMode(\Widget_Link::OPEN_MODAL)
            ->setIcon(\Func_Icons::ACTIONS_LIST_ADD)
            ->setTitle($App->translate('Add contact to this organization'))
            ->setDialogClass($customSection->classname)
        );
        
        $link = $W->VBoxItems(
            $contactOrganizationCtrlNoProxy->listForOrganization($this->record->id)
        );
        
        return $this->labelledWidget(
            isset($label) ? $label : $App->translate('Contacts'),
            $link,
            $customSection
        );
    }
    
    
    protected function _parent(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $widget = $W->Label('');
        
        $org = $this->record->parent();
        
        if ($org->id) {
            $widget = $W->FlowItems(
                $W->Link($org->name, $App->Controller()->Organization()->display($org->id)),
                $W->Link('', $App->Controller()->Organization()->orgChart($this->record->id))
                ->setTitle($App->translate('View situation in group'))
                ->addClass('icon', \Func_Icons::APPS_ORGCHARTS)
                ->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC)
            )->setHorizontalSpacing(1, 'em');
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Parent organization'),
            $widget,
            $customSection
        );
    }
    
    protected function _payingOrganization(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $org = $this->record->getPayingOrganization();
        
        $widget = $W->Label('');
        
        if ($org->id) {
            $widget = $W->Link(
                $org->name,
                $App->Controller()->Organization()->display($org->id)
            );
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Paying organization'),
            $widget,
            $customSection
        );
    }
    
    protected function _ESS(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('ESS'),
            $W->Label($this->record->ESS ? $App->translate('Oui') : $App->translate('Non')),
            $customSection
        );
    }
    
    protected function _notes(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->Note()->add($this->record->getRef())
            )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy(\Func_Icons::ICON_LEFT_16 . ' pull-up')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->setTitle($App->translate('Add note'))
        );
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Notes'),
            $App->Controller()->Note(false)->listFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _teams(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->Team()->add($this->record->getRef())
            )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy('pull-up')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            ->setTitle($App->translate('Add team'))
        );
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Teams'),
            $App->Controller()->Team(false)->listFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _children(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $box = $W->VboxItems()->setVerticalSpacing(1,"em");
        $box->addItem($W->Link('', $App->Controller()->Organization()->orgChart($this->record->id))
            ->setTitle($App->translate('View situation in group'))
            ->addClass('icon', \Func_Icons::APPS_ORGCHARTS)
            ->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC));
        $children = $this->record->getAllChildrenOrganization();
        foreach($children as $child){
            $box->addItem($this->childrenLegacy($child,true));
        }
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Children organizations'),
            $box,
            $customSection
        );
    }
    
    protected function childrenLegacy($children,$first=false)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $layout = $W->HBoxItems()->setHorizontalSpacing(2,"em");
        if(!$first){
            $class = array(\Func_Icons::ACTIONS_ARROW_RIGHT, "icon");
        }
        $layout->addItem($W->Link($children["name"],$App->Controller()->Organization()->display($children["id"]))->addClass($class))
        ->addClass(\Func_Icons::ICON_LEFT_16);
        if(count($children["children"])>0){
            $box = $W->VBoxItems()->setVerticalSpacing(1,"em");
            foreach($children["children"]as $child){
                $box->addItem($this->childrenLegacy($child));
            }
            $layout->addItem($box);
        }
        return $layout;
    }
    
    protected function _address(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $addresses = $this->ctrlNoProxy->addresses($this->record->id);
        
        $W = bab_Widgets();
        $box = $W->FlowItems()->setIconFormat(16, 'left');
        
        if($this->record->isUpdatable()){
            $addButton = $W->Link(
                '',
                $this->ctrl->addAddress($this->record->id)
            )->setTitle($App->translate('Add'))
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            ->setIcon(\Func_Icons::ACTIONS_LIST_ADD);
        }
        
        if($customSection->fieldsLayout == \app_CustomSection::FIELDS_LAYOUT_NO_LABEL){
            $box->addItem($addButton);
        }
        else{
            $box->addItem(
                $W->FlowItems(
                    $W->Label($App->translate('Address'))->addClass('crm-horizontal-display-label', 'widget-strong')->setSizePolicy('widget-90pc'),
                    $addButton->setSizePolicy('widget-10pc widget-align-right')
                )->addClass('widget-labelled-widget')->setSizePolicy('widget-100pc')->setVerticalAlign('middle')
                ->setHorizontalSpacing(1, 'ex')
            );
        }
        
        $box->addItem($addresses);
            
        return $box;
    }
    
    protected function _description(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Description'),
            $W->Html($this->record->description),
            $customSection
        );
    }
    
    protected function _actions(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Actions'),
            $W->DelayedItem($App->Controller()->Task()->listFor($this->record->getRef())),
            $customSection
        );
    }
    
    protected function _CA(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $widget = $W->Label($App->numberFormat($this->record->CA,0).$App->translate(" euros"));
        $userName = bab_getUserName($this->record->CAPerson, true);
        
        if(isset($userName)){
            $widget->setTitle(sprintf($App->translate('Modified by %s on %s'), $userName, bab_shortDate(bab_mktime($this->record->CADate))));
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('CA'),
            $widget,
            $customSection
        );
    }
    
    protected function _effectif(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $widget = $W->Label(sprintf($App->translate('%s person', '%s persons', $this->record->effectif), $this->record->effectif));
        $userName = bab_getUserName($this->record->effectifPerson, true);
        
        if(isset($userName)){
            $widget->setTitle(sprintf($App->translate('Modified by %s on %s'), $userName, bab_shortDate(bab_mktime($this->record->effectifDate))));
        }
        
        return $this->labelledWidget(
            !empty($widget) ? $widget : $App->translate('Workfoce'),
            $label,
            $customSection
        );
    }
    protected function _status(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $OrganizationStatusSet = $App->OrganizationStatusSet();
        $OrganizationsStatus = $OrganizationStatusSet->select($OrganizationStatusSet->organization->is($this->record->id));
        $OrganizationsStatus->orderDesc($OrganizationStatusSet->createdOn);
        $OrganizationsStatus->orderDesc($OrganizationStatusSet->date);
        
        $statusDate = '';
        $statusComment = '';
        foreach ($OrganizationsStatus as $OrganizationsStatut) {
            $statusDate = $OrganizationsStatut->date;
            $statusComment = $OrganizationsStatut->comment;
            break;
        }
        
        $statusItem = $W->VBoxItems(
            $W->HBoxItems(
                $W->Frame()->setCanvasOptions(
                    \Widget_Item::Options()->backgroundColor('#' . $OrganizationsStatus->color)
                )->addClass('crm-color-preview'),
                $W->Label($App->translate($OrganizationsStatut->status)),
                $W->Label('(' . bab_shortDate(bab_mktime($statusDate), false) . ')'),
                $W->Link(
                    '',
                    $App->Controller()->Organization()->editStatus($this->record->id)
                )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            )->setHorizontalSpacing(0.5, 'em')
            ->setVerticalAlign('middle'),
            $W->Html(bab_toHtml($statusComment, BAB_HTML_ALL))
        )->addClass(\Func_Icons::ICON_LEFT_16);
            
        $statusItem->addClass('depends-status');
        
        return $this->labelledWidget(
            !empty($label) ? $label :$App->translate('Status'),
            $statusItem,
            $customSection
        );
    }
    
    protected function _logo(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        return $this->labelledWidget(
            isset($label) ? $label : $App->translate('Logo'),
            $App->Controller()->Organization(false)->logo($this->record->id),
            $customSection
        );
    }
    
    protected function _types(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();        
        
        $organizationTypes = $this->record->getOrganizationTypes();
        $typesLabel = array();
        foreach ($organizationTypes as $organizationType) {
            $typesLabel[] = $organizationType->name;
        }
        return $this->labelledWidget(
            empty($label) ? $App->translate('Type') : $label,
            $W->Label(implode(', ', $typesLabel)),
            $customSection
        );
    }
    
    protected function _pendingTasks(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $taskC = $App->getComponentByName('Task');
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        if(!$contextMenu->getById($section->getId().'_addTaskLink')){
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $App->Controller()->Task()->add($this->record->getRef()),
                    $section->getId().'_addTaskLink'
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($taskC->translate('Add task'))
            );
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $taskC->translate('Pending tasks'),
            $App->Controller()->Task(false)->_pendingTasksFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _completedTasks(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $taskC = $App->getComponentByName('Task');
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        if(!$contextMenu->getById($section->getId().'_addTaskLink')){
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $App->Controller()->Task()->add($this->record->getRef()),
                    $section->getId().'_addTaskLink'
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($taskC->translate('Add task'))
            );
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $taskC->translate('Completed tasks'),
            $App->Controller()->Task(false)->_completedTasksFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _attachments(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        if(!$App->getComponentByName('Attachment')){
            return null;
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Documents'),
            $App->Controller()->Attachment(false)->_attachments($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _tags(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $tagComponent = $App->getComponentByName("Tag");
        if(!isset($tagComponent)){
            return null;
        }
        
        $tagsDisplay = $App->Ui()->TagsListForRecord($this->record, $App->Controller()->Organization()->displayListForTag());
        $editor = new \app_Editor($App);
        $editor->addItem($App->Controller()->Tag(false)->SuggestTag()->setName('label'));
        
        $editor->setName('tag');
        $editor->setSaveAction($App->Controller()->Tag()->link(),$App->translate("Add"));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('to', $this->record->getRef());
        $editor->isAjax = true;
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Tags'),
            $W->FlowItems(
                $tagsDisplay,
                $editor
            )->setHorizontalSpacing(0.5, 'em'),
            $customSection
        );
    }
    
    protected function _legalStatus(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $legalStatus = $this->record->legalStatus();
        $name = isset($legalStatus) ? $legalStatus->name : '';
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Legal status'),
            $W->Label($name),
            $customSection
        );
    }
}
