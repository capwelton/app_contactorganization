<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ui;

use Capwelton\App\ContactOrganization\Set\ContactOrganization;


class ContactOrganizationUi extends \app_Ui implements \app_ComponentUi
{    
    public function editor(ContactOrganization $record = null)
    {
        return $this->ContactOrganizationEditor($record);
    }
    
    /**
     * ContactOrganization editor
     * @return ContactOrganizationEditor
     */
    public function ContactOrganizationEditor(ContactOrganization $contactorganization = null)
    {
        return new ContactOrganizationEditor($this->app, $contactorganization);
    }
    
    /**
     * @return SuggestPosition
     */
    public function SuggestPosition($id = null)
    {
        $suggest = new SuggestPosition($id);
        return $suggest;
    }
}
