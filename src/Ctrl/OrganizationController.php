<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

use Capwelton\App\ContactOrganization\Set\Organization;
use Capwelton\App\ContactOrganization\Set\OrganizationStatus;
use Capwelton\App\Address\Set\Address;
use Capwelton\App\Address\Ui\AddressUi;
use Capwelton\App\ContactOrganization\Ui\ContactUi;
use Capwelton\App\ContactOrganization\Ui\OrganizationUi;
use Capwelton\App\Address\Set\AddressSet;
use Capwelton\App\ContactOrganization\Set\Contact;
use Capwelton\App\ContactOrganization\Set\ContactOrganizationSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Organization');


/**
 * This controller manages actions that can be performed on organizations.
 *
 * @method \Func_App App()
 */
class OrganizationController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Organization'));
    }
    
    protected function getRecordSet()
    {
        $recordSet = parent::getRecordSet();
        $recordSet->address();
        return $recordSet;
    }


    /**
     * @param Organization $record
     * @return array
     */
    protected function getActions(Organization $record)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $ctrl = $this->proxy();
        $actions = array();
        $moreActions = array();

        if ($record->isUpdatable()) {
            $action = $ctrl->edit($record->id);
            $action->setTitle($App->translate('Edit'));
            $action->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT);
            $actions['edit'] = $action;
        }

        if ($record->isDeletable()) {
            $moreActions['delete'] =  $W->Link(
                $App->translate('Delete'),
                $ctrl->confirmDelete($record->id)
            )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG);
        }


        if (bab_isUserAdministrator()) {
            $customContainerCtrl = $App->Controller()->CustomContainer();
            $views = $record->getViews();
            $className = $record->getClassName();
            
            foreach ($views as $view) {
                $action = $customContainerCtrl->editContainers($className, $view);
                $action->setTitle(sprintf($App->translate('Edit view layout %s'), $view));
                $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
                $moreActions['editSections_' . $view] = $action;
            }
            
            $action = $customContainerCtrl->importContainersConfirm($className, $view);
            $action->setTitle($App->translate('Import view layout'));
            $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
            $moreActions['importSectionsConfirm'] = $action;
        }

        if($attachmentComponent = $App->getComponentByName('Attachment')){
            $actions['documents'] = $W->Link(
                $attachmentComponent->translate('Attached files'),
                $App->Controller()->Attachment()->attachments($record->getRef(), 48)
            )->addClass('icon', \Func_Icons::PLACES_FOLDER)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG);
        }


        $actions['more'] = array(
            'icon' => 'actions-context-menu',
            'title' => 'More',
            'items' => array(
                $moreActions
            )
        );
        return $actions;
    }

    /**
     *
     * @return string[]
     */
    public function getAvailableDisplayFields()
    {
        $App = $this->App();
        $availableFields = array(
            'contacts' => array(
                'name' => 'contacts',
                'description' => $App->translate('Contacts in this organization')
            ),
            'children' => array(
                'name' => 'children',
                'description' => $App->translate('Children'),
            ),
            'logo' => array(
                'name' => 'logo',
                'description' => 'logo'
            ),
            'address' => array(
                'name' => 'address',
                'description' => 'address'
            ),
            'status' => array(
                'name' => 'status',
                'description' => 'status'
            ),
            'notes' => array(
                'name'=>'notes',
                'description'=> $App->translate('Notes')
            ),
            'types' => array(
                'name' =>'types',
                'description' => $App->translate('Types')
            ),
            'name' => array(
                'name' => 'name',
                'description' => $App->translate('Name')
            )
        );
        
        if($attachmentC = $App->getComponentByName('Attachment')){
            $availableFields['attachments'] = array(
                'name' => 'attachments',
                'description' => $attachmentC->translate('Attached files')
            );
        }
        if($tagC = $App->getComponentByName('Tag')){
            $availableFields['tags'] = array(
                'name' => 'tags',
                'description' => $tagC->translate('Tags')
            );
        }
        if($noteC = $App->getComponentByName('Note')){
            $availableFields['notes'] = array(
                'name' => 'notes',
                'description' => $noteC->translate('Notes')
            );
        }
        if($teamC = $App->getComponentByName('Team')){
            $availableFields['teams'] = array(
                'name' => 'teams',
                'description' => $teamC->translate('Teams')
            );
        }
        if($currencyC = $App->getComponentByName('Currency')){
            $availableFields['currency'] = array(
                'name' => 'currency',
                'description' => $currencyC->translate('Currency')
            );
        }
        if($taskC = $App->getComponentByName('TASK')){
            $availableFields['pendingTasks'] = array(
                'name' => 'pendingTasks',
                'description' => $taskC->translate('Pending tasks')
            );
            $availableFields['completedTasks'] = array(
                'name' => 'completedTasks',
                'description' => $taskC->translate('Completed tasks')
            );
        }

        $availableFields = array_merge(parent::getAvailableDisplayFields(), $availableFields);

        return $availableFields;
    }



    /**
     * Displays an information page about elements linked to an organization
     * before deleting it, and propose the user to confirm or cancel
     * the organization deletion.
     *
     * @param int $id
     *
     * @return \app_Page
     */
    public function confirmDelete($id = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $recordSet = $this->getRecordSet();
        /* @var $record Organization */
        $record = $recordSet->get($recordSet->id->is($id));
        
        if (!isset($record) || $record->deleted == \app_TraceableRecord::DELETED_STATUS_DELETED) {
            throw new \app_Exception($App->translate('Trying to delete a contact with a wrong (unknown) id'));
        }
        
        $page = $App->Ui()->Page();
        $page->setMainPanelLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
        $page->addClass('crm-page-editor');
        
        $page->setTitle($App->translate('Delete organization'));
        
        $form = new \app_Editor($App);
        $form->setHiddenValue('tg', $App->controllerTg);
        
        $relatedRecords = $record->getRelatedRecords();

        $linkSet = $App->LinkSet();

        $links = $linkSet->select(
            $linkSet->any(
                $linkSet->sourceClass->is(get_class($record))->_AND_($linkSet->sourceId->is($record->id)),
                $linkSet->sourceClass->is(get_class($record))->_AND_($linkSet->targetId->is($record->id))
            )
        );
        
        if (count($relatedRecords) > 0 || $links->count() > 0) {
            
            $relatedRecordSection = $W->Section(
                $App->translate('Several elements are related to this organization'),
                $W->VBoxItems()->setVerticalSpacing(1, 'em')
            );
            $page->addItem($relatedRecordSection);
            
            $confirmedAction = $this->proxy()->delete($record->id);
            $parameters = $confirmedAction->getParameters();
            foreach ($parameters as $key => $value) {
                $form->setHiddenValue($key, $value);
            }
            $form->addButton(
                $W->SubmitButton()
                    ->setAjaxAction($confirmedAction)
                    ->setLabel($App->translate('Delete the organization and all associated elements'))
            );
            
            $replaceAction = $this->proxy()->searchReplacement($record->id);
            $parameters = $confirmedAction->getParameters();
            foreach ($parameters as $key => $value) {
                $form->setHiddenValue($key, $value);
            }
            $form->addButton(
                $W->SubmitButton()
                    ->setAjaxAction($replaceAction)
                    ->setLabel($App->translate('Replace with an existing organization'))
            );
            
        } else {
            
            $confirmedAction = $this->proxy()->delete($record->id);
            $parameters = $confirmedAction->getParameters();
            foreach ($parameters as $key => $value) {
                $form->setHiddenValue($key, $value);
            }
            $form->addButton(
                $W->SubmitButton()
                    ->setAjaxAction($confirmedAction)
                    ->setLabel($App->translate('Delete permanently'))
            );
            
            $page->addItem($W->Html(bab_toHtml($App->translate('This organization is not associated with any CRM element')))->addClass('alert'));
        }
        
        $page->addItem($form);
        
        return $page;
    }

    
    
    /**
     *
     * @param int $id
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function replacementMatches($id, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        $recordSet = $this->getRecordSet();

        $box = $W->VBoxLayout();
        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $keyword = $_SESSION['organizationReplacementMatches_' . $id];
        $keywords = array();
        foreach (explode(' ', $keyword) as $k) {
            if (mb_strlen($k) > 3) {
                $keywords[] = $k;
            }
        }

        if (count($keywords) > 0) {
            $similarElementsSection = $W->Section(
                $App->translate('Similar organizations'),
                $W->VBoxLayout()
                    ->setVerticalSpacing(1, 'em'),
                3
            );
            $box->addItem($similarElementsSection);

            $criteria = $recordSet->none();
            foreach ($keywords as $k) {
                $criteria = $criteria->_OR_($recordSet->name->contains($k));
            }

            $matchingRecords = $recordSet->select(
                $recordSet->id->isNot($id)
                ->_AND_($criteria)
            );
            
            foreach ($matchingRecords as $matchingRecord) {
                $similarElementsSection->addItem(
                    $W->FlowItems(
                        $Ui->OrganizationCardFrame($matchingRecord)
                            ->addClass('widget-30em'),
                        $W->Link(
                            $App->translate('Replace with this organization'),
                            $this->proxy()->replace($id, $matchingRecord->id)
                        )->addClass('widget-actionbutton')
                    )
                    ->setHorizontalSpacing(2, 'em')
                    ->setVerticalAlign('middle')
                    ->setSizePolicy('widget-list-element')
                );
            }
        }

        $box->setReloadAction($this->proxy()->replacementMatches($id, $box->getId()));

        $box->addClass('depends-replacementKeyword');

        return $box;
    }


    /**
     *
     * @param int $id
     * @param string $keyword
     * @return bool
     */
    public function setReplacementKeyword($id = null, $keyword = null)
    {
        $_SESSION['organizationReplacementMatches_' . $id] = $keyword;

        $this->addReloadSelector('.depends-replacementKeyword');

        return true;
    }

    /**
     *
     * @param int $id
     *
     * @return \app_Page
     */
    public function searchReplacement($id = null, $keyword = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($recordSet->id->is($id));

        if (!isset($record) || $record->deleted == \app_TraceableRecord::DELETED_STATUS_DELETED) {
            throw new \app_Exception($App->translate('Trying to replace an organization with a wrong (unknown) id'));
        }

        if (!isset($keyword)) {
            $keyword = $record->name();
        }

        $this->setReplacementKeyword($record->id, $keyword);

        $page = $Ui->Page();

        $page->setTitle($App->translate('Search replacement for organization'));

        $page->addContextItem($App->Ui()->OrganizationCardFrame($record), $App->translate('Organization to replace'));


        $editor = new \app_Editor($App, null, $W->VBoxLayout());
        $editor->colon();
        
        $editor->addItem($editor->labelledField($App->translate('Keywords'), $W->LineEdit()->setSize(80), 'keyword'));
        $editor->setSaveAction($this->proxy()->setReplacementKeyword(), $App->translate('Search'));
        $editor->setReadOnly();
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('id', $record->id);
        $editor->setValue('keyword', $keyword);
        $editor->isAjax = true;
        $page->addItem($editor);
        
        $page->addItem(
            $this->replacementMatches($record->id)
        );
        
        return $page;
    }



    /**
     * 
     * @param int $oldId
     * @param int $newId
     * @throws \app_Exception
     * @return boolean
     */
    public function replace($oldId, $newId)
    {
        $App = $this->App();
        
        $recordSet = $this->getRecordSet();
        $oldRecord = $recordSet->request($oldId);
        $newRecord = $recordSet->request($newId);
        
        if (!$oldRecord->isUpdatable() || !$newRecord->isUpdatable()) {
            throw new \app_Exception($App->translate('You do not have access to this action'));
        }
        
        $oldRecord->replaceWith($newRecord->id);

        $recordSet->delete($recordSet->id->is($oldRecord->id));

        $message = sprintf(
            $App->translate('The organization %s has been deleted and all its elements reattached to %s'),
            $oldRecord->name, $newRecord->name
        );

        $this->addMessage($message);

        app_redirect($this->proxy()->display($newRecord->id));
    }



    public function clearSelectedRecords()
    {
        $modelView = $this->modelView(null, 'table');
        $modelView->clearSelectedRows();

        $this->addReloadSelector('.depends-' . $modelView->getId());

        return true;
    }

    public function clearSelectedRecord($id)
    {
        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();

        unset($selectedRows[$id]);

        $modelView->setSelectedRows($selectedRows);

        $this->addReloadSelector('.depends-' . $modelView->getId());

        return true;
    }

    public function showSelectedRecords()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $modelView = $this->modelView(null, 'table');

        $selectedRows = $modelView->getSelectedRows();

        $page = $App->Ui()->Page();
        $page->setTitle($App->translate('Selected elements'));
        $page->setIconFormat(16, 'left');

        $recordSet = $this->getRecordSet();
        $records = $recordSet->select($recordSet->id->in($selectedRows));

        foreach ($records as $record) {
            $page->addItem(
                $W->FlowItems(
                    $W->Link(
                        $record->name,
                        $this->proxy()->display($record->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-40em'),
                    $W->Link(
                        '',
                        $this->proxy()->clearSelectedRecord($record->id)
                    )->setAjaxAction()
                    ->setTitle($App->translate('Remove from selection'))
                    ->addClass('widget-actions')
                    ->setIcon(\Func_Icons::ACTIONS_DIALOG_CANCEL)
                )->setSizePolicy('widget-list-element widget-actions-target')
            );
        }

        $page->addClass('depends-' . $modelView->getId());
        $page->setReloadAction($this->proxy()->showSelectedRecords());
        return $page;
    }


    public function setHighlightedRecords($ids = array())
    {
        $modelView = $this->modelView();
        $modelView->setHighlightedRows($ids);

        $this->addReloadSelector('.depends-' . $modelView->getId() . '-HighlightedRecords');

        return true;
    }

    public function clearHighlightedRecords()
    {
        $modelView = $this->modelView();
        $modelView->clearHighlightedRows();

        $this->addReloadSelector('.depends-' . $modelView->getId() . '-HighlightedRecords');

        return true;
    }

    public function previewHighlightedRecords($view = '', $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $modelView = $this->modelView();
        
        $highlightedRows = $modelView->getHighlightedRows();
        
        $recordSet = $this->getRecordSet();
        $records = $recordSet->select($recordSet->id->in($highlightedRows));
        
        $box = $W->VBoxLayout($itemId);
        $box->addClass('depends-' . $modelView->getId() . '-HighlightedRecords');
        $box->setReloadAction($this->proxy()->previewHighlightedRecords($view, $box->getId()));
        $box->setIconFormat(16, 'left');
        
        $Ui = $App->Ui();
        
        if ($records->count() > 0) {
            $box->addItem(
                $W->Link(
                    '', 
                    $this->proxy()->clearHighlightedRecords()
                )->setAjaxAction()
                ->setIcon(\Func_Icons::ACTIONS_ARROW_RIGHT_DOUBLE)
                ->addClass('app_closeHighlightButton')
                ->setSizePolicy('pull-right')
            );
            
            foreach ($records as $record) {
                $fullFrame = $Ui->OrganizationFullFrame($record);
                $fullFrame->setView("preview");
                $box->addItem($fullFrame);
            }
            $box->addClass('box', 'shadow');
        }
        
        return $box;
    }

    /**
     *
     * @param string $modelViewId
     * @param string $itemId
     * @return \Widget_FlowLayout
     */
    public function selectedItemsToolbar($modelViewId, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $cmp = $this->App()->getComponentByName("Organization");
        $modelView = $this->modelView(null, 'table');
        
        $box = $W->FlowLayout($itemId)->setHorizontalSpacing(1, 'ex');
        $box->addClass('depends-' . $modelViewId . '-selection');
        $box->setReloadAction($this->proxy()->selectedItemsToolbar($modelViewId, $box->getId()));
        $box->setIconFormat(16, 'left');

        $nbSelected = count($modelView->getSelectedRows());
        $links = array();
        
        $box->addItem(
            $W->Html(
                '<b>' . bab_toHtml($cmp->translate('Selection:')) . '</b>'
            )
        );
        $box->addItem(
            $W->Link(
                $nbSelected,
                $this->proxy()->showSelectedRecords()
            )->setTitle($cmp->translate('Show selected organization'))
            ->addClass('badge')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        $box->addItem(
            $links[] = $W->Link(
                '',
                $this->proxy()->clearSelectedRecords()
            )->setIcon(\Func_Icons::ACTIONS_DIALOG_CANCEL)
            ->setTitle($cmp->translate('Clear selection'))
            ->setAjaxAction()
        );
        $box->addItem(
            $W->Html(
                '<b>' . bab_toHtml($cmp->translate('Actions:')) . bab_nbsp() . '</b>'
            )
        );

        $box->addItem(
            $links[] = $W->Link(
                $cmp->translate('Delete'),
                $this->proxy()->deleteConfirmMultipleOrganization()
            )->setIcon(\Func_Icons::OBJECTS_ORGANIZATION)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
        $box->addItem(
            $links[] = $W->Link(
                $cmp->translate('Change parent organization'),
                $this->proxy()->editMultiParentOrganization()
            )->setIcon(\Func_Icons::ACTIONS_DIALOG_CANCEL)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        $box->addClass('widget-toolbar-multiselect', 'alert-info');
            
        if ($nbSelected == 0) {
            foreach($links as $link){
                $link->disable();
            }
        }
        return $box;
    }



    /**
     * Displays a form to edit the organizations' parent organization.
     *
     * @param array	$organizations 	The organization ids.
     *
     * @return \app_Page
     */
    public function editMultiParentOrganization($organizations = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();

        $page = $Ui->Page();
        $page->setTitle(sprintf($App->translate('Change parent organization for %d organizations'), count($selectedRows)));

        $layout = $W->VBoxItems()->setVerticalSpacing(2, 'em');
        $page->addItem($layout);

        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();

        $editor->setName('data');
        $suggestOrganization = $Ui->SuggestOrganization();

        $editor->addItem(
            $editor->labelledField(
                $App->translate('New parent organization'),
                $suggestOrganization->setMinChars(0),
                'parent'
            )
        );

        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->saveMultiParentOrganization());
        $editor->isAjax = $this->isAjaxRequest();

        $layout->addItem($editor);

        return $page;
    }
    
    public function deleteConfirmMultipleOrganization(){
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();
        
        $Ui->includeOrganization();
        
        $page = $Ui->Page();
        $page->setTitle(sprintf($App->translate('Deleted %d organizations'), count($selectedRows)));
        
        $layout = $W->VBoxItems()->setVerticalSpacing(2, 'em');
        $page->addItem($layout);
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();
        $editor->setName('data');

        
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->deleteMultiOrganization());
        $editor->isAjax = $this->isAjaxRequest();
        
        $layout->addItem($editor);
        
        return $page;
    }

    public function deleteMultiOrganization(){
        $App = $this->App();
        
        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();
        $organizationSet = $App->OrganizationSet();
        $organizations = $organizationSet->select(
            $organizationSet->id->in($selectedRows)
        );
        foreach($organizations as $o){
            $o->delete();
        }
        $this->clearSelectedRecords();
        return true;
    }


    /**
     * Saves the organizations' parent organization.
     *
     * @return bool
     */
    public function saveMultiParentOrganization($data = null)
    {
        $App = $this->App();
        $organizationSet = $App->OrganizationSet();

        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();

        $organizations = $organizationSet->select($organizationSet->id->in($selectedRows));

        $nbModifiedOrganizations = 0;
        $nbNonModifiedOrganizations = 0;

        foreach ($organizations as $organization) {

            $organization->parent = $data['parent_ID'];
            $organization->save();

            $nbModifiedOrganizations++;
            unset($selectedRows[$organization->id]);
        }

        if (method_exists($modelView, 'setSelectedRows')) {
            $modelView->setSelectedRows($selectedRows);
        } else {
            $W = bab_Widgets();
            $W->setUserConfiguration($modelView->getId() . '/selectedRows', $selectedRows, 'widgets');
        }

        if ($nbModifiedOrganizations > 0) {
            $this->addMessage(sprintf($App->translate('%d organizations were updated'), $nbModifiedOrganizations));
        }
        if ($nbNonModifiedOrganizations > 0) {
            $this->addError(sprintf($App->translate('%d organizations were not updated'), $nbNonModifiedOrganizations));
        }

        $this->addReloadSelector('.depends-Organization');
        return true;
    }


    /**
     *
     * @param int $id The organization id
     */
    public function addAddress($id = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $Ui = $App->Ui();
        $page = $Ui->Page();

        $page->addClass('App-page-editor');
        $page->setTitle($App->translate('Add an address'));


        $newAddressForm = $W->Form();
        $newAddressForm->setName('link');
        $newAddressForm->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $newAddressForm->addItem(
            $this->typedAddress()
        );

        $newAddressForm->addItem(
            $W->SubmitButton()
            ->setAction($this->proxy()->saveLinkedAddress())
            ->setAjaxAction(null, '')
            ->setLabel($App->translate('Save this address'))
        );

        $newAddressForm->setHiddenValue('link[organization]', $record->id);

        $page->addItem($newAddressForm);

        return $page;
    }

    public function saveLinkedAddress($link = null)
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();
        $addressSet = $App->AddressSet();

        $linkTypes = array();

        if (isset($link['type'])) {
            $linkTypes = explode(',', trim($link['type'], ' ,'));
        }
        
        /* @var $record \app_Link */
        /* @var $organization Organization */
        /* @var $address Address */
        if (isset($link['id'])) {
            $record = $linkSet->get($link['id']);
            $organization = $record->getSource();
            $address = $record->getTarget();

            if (!$organization instanceof Organization) {
                throw new \App_AccessException($App->translate('You are not allowed to update this organization'));
            }

            if (!$address instanceof Address) {
                throw new \App_AccessException($App->translate('You are not allowed to update this address'));
            }
            
            $address->setFormInputValues($link['address']);
            $address->save();
            
            $linkType = array_shift($linkTypes);
            $linkType = trim($linkType);
            $linkType = bab_convertToDatabaseEncoding($linkType);
            $record->type = 'hasAddressType:' . $linkType;
            $record->save();
        } 
        elseif(isset($link['address']) && isset($link['organization'])){
            $linkType = array_shift($linkTypes);
            $linkType = trim($linkType);
            $linkType = bab_convertToDatabaseEncoding($linkType);            
            $address = $addressSet->newRecord();
            $address->setFormInputValues($link['address']);
            $address->save();
            $organizationSet = $App->OrganizationSet();
            $organization = $organizationSet->request($link['organization']);
            $organization->addAddress($address, $linkType);
        }

        return true;
    }

    /**
     *
     * @param string   $type
     * @param string   $address
     * @param bool     $editableAddress
     * @return \Widget_Frame
     */
    public function typedAddress($type = '', $address = null, $editableAddress = true)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $addressComponent = $App->getComponentByName('Address');
        if(!$addressComponent){
            return null;
        }
        
        if (isset($address) && (!$address instanceof Address)) {
            $address = $App->AddressSet()->get($address);
        }

        $addressEditor = $this->addressEditor($address);
        $addressEditor->setName('address');
        if (!$editableAddress) {
            $addressEditor->disable();
        }

        $box = $W->VBoxItems(
            $W->LabelledWidget(
                $App->translate('Type'),
                $W->LineEdit()
                ->setSize(15)
                ->setName('type')
                ->setValue($type)
            ),
            $addressEditor
        );

        $frame = $W->Frame()->setLayout($box);

        return $frame;
    }
    
    /**
     * Creates a frame with fields to enter an address
     *
     * @return \Widget_Frame
     */
    public function addressEditor($address = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        return $Ui->AddressEditor()->setName('adr');
    }



    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $cmp = $App->getComponentByName("Organization");
        $toolbar = parent::toolbar($tableView);
        $toolbar->addItem(
            $W->Link(
                $cmp->translate('Add organization'),
                $this->proxy()->edit()
            )->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        $toolbar->addItem(
            $this->selectedItemsToolbar($tableView->getId())
        );

        return $toolbar;
    }



    /**
     * Returns a page with the record information.
     *
     * @param int	$id
     * @return \app_Page
     */
    public function display($id, $view = '', $itemId = null)
    {
        $App = $this->App();

        $recordSet = $this->getRecordSet();

        $record = $recordSet->request($id);
        if (!$record->isReadable()) {
            throw new \app_AccessException($App->translate('You do not have access to this page'));
        }

        $W = bab_Widgets();
        
        $Ui = $App->Ui();
        $fullFrame = $Ui->OrganizationFullFrame($record);
        $fullFrame->setView($view);

        $actions = $this->getActions($record);

        $mainMenu = $W->Html($this->createMenu($actions));
        $mainMenu->addClass('app-record-nav', 'nav', 'navbar-nav', 'pull-right');
        $mainMenu->setSizePolicy(\Func_Icons::ICON_LEFT_24);

        $page = $App->Ui()->Page();
        if (isset($itemId)) {
            $page->setId($itemId);
        }
        $page->addClass('depends-' . $this->getRecordClassName());

        $page->addToolbar($mainMenu);

        $page->addItem($fullFrame);

        $page->setReloadAction($this->proxy()->display($id, $view, $page->getId()));

        return $page;
    }

    public function displayListForTag($tag = null)
    {
        return ($this->displayList(array('tags' => $tag)));
    }


    public function editStatusHistory($orgaStatus = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $organizationStatusSet = $App->OrganizationStatusSet();
        $organizationStatusSet->organization();

        /* @var $deal OrganizationStatus */
        $orgaStatus = $organizationStatusSet->request($orgaStatus);
        if (!$orgaStatus->organization->isUpdatable()) {
            throw new \app_AccessException($App->translate('You are not allowed to access this page'));
        }

        $page = $App->Ui()->Page();

        $editor = new \app_Editor($App);
        $editor->colon();

        $editor->setName('OrganizationStatusHistory');
        
        $statusSelector = $W->Select();
        $typeStatus = OrganizationStatus::getStatus();
        foreach($typeStatus as $value => $label){
            $statusSelector->addOption(
                $value,
                $App->translate($label)
            );
        }

        if(!bab_isUserAdministrator()){
            $statusSelector->disable();
        }
        
        $editor->addItem(
            $W->FlowItems(
                $editor->labelledField(
                    $App->translate('Status'),
                    $statusSelector,
                    'status'
                ),
                $editor->labelledField(
                    $App->translate('Date'),
                    $datePicker = $W->DatePicker()
                    ->setValue(\BAB_DateTime::now())
                    ->setMandatory(true, $App->translate('You must specified the date')),
                    'date'
                )
            )->setHorizontalSpacing(2, 'em')
        );
        $editor->addItem(
            $editor->labelledField(
                $App->translate('Comment'),
                $comment = $W->TextEdit()
                ->addClass('widget-100pc', 'widget-autoresize'),
                'comment'
            )
        );

        $statusSelector->setValue($orgaStatus->status);
        $datePicker->setValue($orgaStatus->date);
        $comment->setValue($orgaStatus->comment);

        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('OrganizationStatusHistory[id]', $orgaStatus->id);
        $editor->setSaveAction($this->proxy()->saveStatusHistory());
        $editor->isAjax = $this->isAjaxRequest();

        $page->setTitle($App->translate('Edit organization status'));
        $page->addItem($editor);

        return $page;
    }



    public function saveStatusHistory($OrganizationStatusHistory = null)
    {
        $App = $this->App();

        $data = $OrganizationStatusHistory;
        $OrganizationStatusSet = $App->OrganizationStatusSet();
        $OrganizationStatusSet->organization();

        $orgStatusHistory = $OrganizationStatusSet->get($data['id']);
        if (!isset($orgStatusHistory)) {
            throw new \app_Exception($App->translate('Trying to access a status history with a wrong (unknown) id'));
        }

        $data["typeOfChange"] = OrganizationStatus::TYPE_MANUAL;

        $orgStatusHistory->setFormInputValues($data);
        $orgStatusHistory->save();

        $this->addReloadSelector('.depends-status');

        return true;
    }

    /**
     * Saves the deal status.
     *
     * @param array	$deal		['id', 'status', 'comment', 'date']
     * @return bool
     */
    public function saveStatus($org = null,$type = OrganizationStatus::TYPE_MANUAL)
    {
        $App = $this->App();
        $organizationSet = $App->OrganizationSet();
        $currentOrg = $organizationSet->request($org['id']);

        if (!$currentOrg->isUpdatable()) {
            throw new \app_AccessException($App->translate('You are not allowed to access this page'));
        }
        
        if(!empty($org['status'])){
            $organizationStatusSet = $App->OrganizationStatusSet();
            $organizationStatus = $organizationStatusSet->newRecord();
            $organizationStatus->comment = $organizationStatusSet->comment->input($org['comment']);
            $organizationStatus->date = $organizationStatusSet->date->input($org['date']);
            $organizationStatus->organization = $currentOrg->id;
            $organizationStatus->status = $organizationStatusSet->status->input($org['status']);
            $organizationStatus->typeOfChange = $type;
            $organizationStatus->save();
            $this->addReloadSelector('.depends-status');
        }

        return true;
    }


    /**
     * Deletes the specified deal status history.
     * @param int $dealStatusHistory
     */
    public function deleteStatusHistory($OrgaStatusHistory)
    {
        $App = $this->App();

        $orgaStatusSet = $App->OrganizationStatusSet();

        $orgaStatus = $orgaStatusSet->request($OrgaStatusHistory);

        $orgaStatus->delete();

        $this->addReloadSelector('.depends-status');

        return true;
    }


    /**
     * @param int $id       Deal id
     * @return \Widget_VBoxLayout
     */
    public function statusHistory($id, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $organizationStatusSet = $App->OrganizationStatusSet();

        $orgaStatus = $organizationStatusSet->select(
            $organizationStatusSet->organization->is($id)
        )->orderDesc($organizationStatusSet->date)
        ->orderASC($organizationStatusSet->createdOn);


        $box = $W->VBoxLayout($itemId);
        $box->setIconFormat(16, 'left');
        $box->addClass('widget-50em');

        $itemBox = $W->FlowItems(
            $W->Label($App->translate('Type of change'))
            ->setSizePolicy('widget-10pc'),
            $W->Label($App->translate('Status'))
            ->setSizePolicy('widget-20pc')
            ->addClass('widget-strong'),
            $W->Label($App->translate('Date'))
            ->setSizePolicy('widget-20pc')
            ->addClass('widget-strong'),
            $W->Label($App->translate('Comment'))
            ->setSizePolicy('widget-30pc')
            ->addClass('widget-strong'),
            $W->Label($App->translate('By'))
            ->setSizePolicy('widget-10pc')
            ->addClass('widget-strong')
        )->setVerticalAlign('top');
        
        $box->addItem(
            $itemBox->setSizePolicy('widget-list-element')
        );
        
        foreach ($orgaStatus as $orgStat) {
            $userInitials = '';
            $userName = bab_getUserName($orgStat->modifiedBy, true);
            $userNameParts = preg_split("/[\s-]+/", $userName);
            foreach ($userNameParts as $userNamePart) {
                $userInitials .= substr($userNamePart, 0, 1);
            }
            $title = $App->translate("This field was changed automaticly");
            if($orgStat->typeOfChange == OrganizationStatus::TYPE_MANUAL){
                $title = $App->translate("This field was changed manually");
            }
            $itemBox = $W->FlowItems(
                $W->Label($App->translate($orgStat->typeOfChange))->setTitle($title)->setSizePolicy('widget-10pc'),
                $W->FlowItems(
                    $W->Frame()->setCanvasOptions(
                        \Widget_Item::Options()->backgroundColor('#' . $orgStat->olor)
                    )->addClass('app-color-preview'),
                    $W->Label($App->translate($orgStat->status))
                )->setHorizontalSpacing(1, 'em')->setSizePolicy('widget-20pc'),
                $W->Label(bab_shortDate(bab_mktime($orgStat->date), false))->setSizePolicy('widget-20pc'),
                $W->Label(bab_abbr($orgStat->comment, BAB_ABBR_FULL_WORDS, 100))->setSizePolicy('widget-30pc')->addClass('widget-small'),
                $W->Label($userInitials)->setTitle(sprintf($App->translate('Modified by %s on %s'), $userName, bab_shortDate(bab_mktime($orgStat->modifiedOn))))->setSizePolicy('widget-10pc'),
                $W->FlowItems(
                    $W->Link(
                        '',
                        $this->proxy()->editStatusHistory($orgStat->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setTitle($App->translate('Edit'))
                    ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $W->Link(
                        '',
                        $this->proxy()->deleteStatusHistory($orgStat->id)
                    )->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction()
                    ->setTitle($App->translate('Delete'))
                    ->setConfirmationMessage($App->translate('Are you sure you want to delete this status?'))
                )->setSizePolicy('widget-10pc widget-align-right')
                ->addClass('widget-actions')
            )->setVerticalAlign('top')
            ->addClass('widget-actions-target');
            
            $box->addItem(
                $itemBox->setSizePolicy('widget-list-element')
            );
        }
        $itemBox->addClass("widget-strong");
        $box->setReloadAction($this->proxy()->statusHistory($id, $box->getId()));
        $box->addClass('depends-status');

        return $box;
    }

    public function reload($reloadobject = ''){
        if(!empty($reloadobject)){
            $this->addReloadSelector($reloadobject);
        }
        return true;
    }
    
    /**
     * Displays a form to edit the organization status.
     *
     * @param int	$org 	The organization id.
     *
     * @return \app_Page
     */
    public function editStatus($org = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $orgSet = $App->OrganizationSet();

        /* @var $org Organization */
        $org = $orgSet->request($org);
        if (!$org->isUpdatable()) {
            throw new \app_AccessException($App->translate('You are not allowed to access this page'));
        }

        $page = $App->Ui()->Page();
        $page->setTitle($App->translate('Edit organization status'));

        $layout = $W->VBoxItems()->setVerticalSpacing(2, 'em');
        $page->addItem($layout);

        $layout->addItem(
            $W->Section(
                $App->translate('Status history'),
                $this->statusHistory($org->id)
            )
        );

        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();

        $editor->setName('org');

        $typeStatusSelector = $W->Select();
        $typeStatus = OrganizationStatus::getStatus();
        foreach($typeStatus as $value => $label){
            $typeStatusSelector->addOption($value,$App->translate($label));
        }
        $options = $typeStatusSelector->getOptions();

        $statuses = array_keys($options);
        $typeStatusSelector->setOptions(array('' => '') + $options);

        $editor->addItem(
            $W->Section(
                $App->translate('New status'),
                $W->VBoxItems(
                    $W->FlowItems(
                        $statusField = $editor->labelledField(
                            $App->translate('Status'),
                            $typeStatusSelector,
                            'status'
                        )->setSizePolicy('widget-50pc'),
                        $dateField = $editor->labelledField(
                            $App->translate('Date'),
                            $W->DatePicker()
                            ->setMandatory(true, $App->translate('You must specified the date')),
                            'date'
                        )->setSizePolicy('widget-25pc')
                        )->setHorizontalSpacing(2, 'em'),
                    $commentField = $editor->labelledField(
                        $App->translate('Comment'),
                        $W->TextEdit()
                        ->addClass('widget-100pc', 'widget-autoresize'),
                        'comment'
                    )
                )->setVerticalSpacing(1, 'em')
            )
        );
        
        $statusField->setAssociatedDisplayable($dateField, $statuses);
        $statusField->setAssociatedDisplayable($commentField, $statuses);
        
        $editor->setValue('org/date', \BAB_DateTime::now()->getIsoDateTime());
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('org[id]', $org->id);
        $editor->setSaveAction($this->proxy()->saveStatus());
        
        $editor->isAjax = $this->isAjaxRequest();

        $layout->addItem($editor);

        return $page;
    }

    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestParent($child = null, $search = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        bab_requireCredential($App->translate('You must be logged'), 'Basic');

        $suggest = $Ui->SuggestParentOrganization($child);
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }

    public function save($data = null)
    {
        $App = $this->App();

        $set = $this->getRecordSet();
        $set->address();
        if (!empty($data['id'])) {
            $record = $set->get($data['id']);
        } else {
            $record = $set->newRecord();
        }

        $ca = $data["CA"];
        if(isset($ca)){
            $org = $App->OrganizationSet()->request($data["id"]);
            if(ORM_CurrencyField("")->input($ca) != $org->CA){
                $data["CADate"] = ORM_DateTimeField("")->input(\BAB_DateTime::now()->getIsoDateTime());
                $data["CAPerson"] = bab_getUserId();
            }
        }
        $effectif = $data["effectif"];
        if(isset($effectif)){
            $org = $App->OrganizationSet()->request($data["id"]);
            if(ORM_CurrencyField("")->input($effectif) != $org->effectif){
                $data["effectifDate"] = ORM_DateTimeField("")->input(\BAB_DateTime::now()->getIsoDateTime());
                $data["effectifPerson"] = bab_getUserId();
            }
        }
        
        $contactFields = array(
            'commercialReferralContact',
            'responsible'
        );
        $contactSet = $App->ContactSet();
        foreach ($contactFields as $contactField){
            if(isset($data[$contactField]) && substr($data[$contactField], 0, 4) == 'new_'){
                
                $contact = $contactSet->newRecord();
                
                $contact->firstname = '';
                $contact->lastname = '';
                $contact->homePage = 'operation';
                $fullname = explode(' ', urldecode(substr($data[$contactField], 4)));
                if (count($fullname) === 1) {
                    $contact->lastname = $fullname[0];
                } else {
                    $contact->firstname = $fullname[0];
                    array_shift($fullname);
                    $contact->lastname = implode(' ', $fullname);
                }
                
                $contact->save();
                $data[$contactField] = (int)$contact->id;
            }
        } 
        return parent::save($data);
    }

    public function postSave($record, $data)
    {
        $App = $this->App();

        if (isset($data['types'])) {
            $organizationTypeOrganizationSet = $App->OrganizationTypeOrganizationSet();
            $organizationTypeOrganizationSet->delete($organizationTypeOrganizationSet->organization->is($record->id));
            foreach ($data['types'] as $typeOrganizationId) {
                $organizationTypeOrganization = $organizationTypeOrganizationSet->newRecord();
                $organizationTypeOrganization->organizationType = $typeOrganizationId;
                $organizationTypeOrganization->organization= $record->id;
                $organizationTypeOrganization->save();
            }
        }
        
        if($address = $record->address()){
            $linkSet = $App->LinkSet();
            $link = $linkSet->get($linkSet->sourceIs($record)->_AND_($linkSet->targetIs($address)));
            if(!isset($link)){
                $org = $App->OrganizationSet()->get($record->id);//remove joined records
                $org->addAddress($address, '');
            }
        }
        
        if($responsible = $record->responsible()){
            /* @var $responsible Contact */
            /* @var $record Organization */
            /* @var $contactOrganizationSet ContactOrganizationSet */
            $contactOrganizationSet = $App->ContactOrganizationSet();
            $alreadyExists = $contactOrganizationSet->get(
                $contactOrganizationSet->organization->is($record->id)
                ->_AND_($contactOrganizationSet->contact->is($responsible->id))
            );
            if(!$alreadyExists){
                $contactOrganization = $contactOrganizationSet->newRecord();
                $contactOrganization->organization = $record->id;
                $contactOrganization->contact = $responsible->id;
                $contactOrganization->save();
            }
        }

        $record->save();
    }



    /**
     *
     * @param int|array $orgs
     * @param int $threshold
     *
     * @return \app_Page
     */
    public function orgChart($orgs, $threshold = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $organizationSet = $App->OrganizationSet();

        if (!is_array($orgs)) {
            $orgs = array($orgs);
        }

        $orgChartId = 'App_orgChart_' . implode('_', $orgs);

        $nodeIds = array();
        $nbEstablishments = 0;
        foreach ($orgs as $org) {
            $groupOrg = $organizationSet->get($org);
            if (!isset($groupOrg)) {
                continue;
            }
            $ancestors = array();
            while ($groupOrg->parent != 0) {
                $groupOrg = $organizationSet->get($groupOrg->parent);
                if (isset($ancestors[$groupOrg->parent])) {
                    break;
                }
                $ancestors[$groupOrg->parent] = $groupOrg->parent;
            }

            $nodeIds[] = $orgChartId. '.' . $groupOrg->id;

            $accountOrgs = $groupOrg->getChildren();
            foreach ($accountOrgs as $accountOrg) {
                $nodeIds[] = $orgChartId. '.' . $accountOrg->id;

                $establishmentOrgs = $accountOrg->getChildren();
                foreach ($establishmentOrgs as $establishmentOrg) {
                    $nodeIds[] = $orgChartId . '.' . $establishmentOrg->id;
                    $nbEstablishments++;

                    $subEstablishmentOrgs = $establishmentOrg->getChildren();
                    foreach ($subEstablishmentOrgs as $subEstablishmentOrg) {
                        $nodeIds[] = $orgChartId . '.' . $subEstablishmentOrg->id;
                        $nbEstablishments++;
                    }
                }
            }
        }

        $path = 'orgchart/' . $orgChartId . '/';

        $W->setDefaultConfiguration($path . 'openNodes', $nodeIds);
        $W->setDefaultConfiguration($path . 'openMembers', array());
        $W->setDefaultConfiguration($path . 'verticalThreshold', ($nbEstablishments > 8 ? 3 : 4));
        $W->setDefaultConfiguration($path . 'zoomFactor', 1.4);

        $page = $App->Ui()->Page();

        $page->addItem($W->DelayedItem($this->proxy()->orgChartBox($orgs)));

        return $page;
    }

    public function orgChartBox($orgs)
    {
        $App = $this->App();

        $App->Ui()->includeOrganization();
        $orgChart = $this->orgChart($App, $orgs);

        $orgChart->setReloadAction($this->proxy()->orgChartBox($orgs));

        return $orgChart;
    }
    
    /**
     *
     * @param \Widget_OrgchartView $orgChart
     * @param Organization $org
     */
    private function appendOrgChartElement(\Widget_OrgchartView $orgChart, Organization $org)
    {
        $App = $org->App();
        
        $element = $orgChart->createElement($org->id, 'entity', $org->name, '', '');
        
        $element->setItem($this->orgchartElementItem($org));
        
        $orgChart->appendElement($element, $org->parent == 0 ? null : $org->parent);
        
        $element->addAction(
            'close_all_members',
            $App->translate('Hide all contacts from this organization'),
            '',
            '',
            'closeAllMembers',
            array('this'),
            \Func_Icons::ACTIONS_LIST_REMOVE . ' icon'
        );
        $element->addAction(
            'open_all_members',
            $App->translate('Show all contacts from this organization'),
            '',
            '',
            'openAllMembers',
            array('this'),
            \Func_Icons::ACTIONS_LIST_ADD . ' icon'
        );
    }
    /**
     *
     * @param Organization $org
     * @return \Widget_VBoxLayout
     */
    private function orgchartElementItem(Organization $org)
    {
        $W = bab_Widgets();
        $App = $org->App();
        
        $box = $W->VBoxItems(
            $W->Link(
                $org->name,
                $this->proxy()->display($org->id)
            )->addClass('widget-strong')
        );
        
        $address = $org->address();
        if ($address) {
            $box->addItem(
                $W->Label($address->postalCode . ' ' . $address->city)
                ->addClass('widget-small')
            );
        }
        
        $logoPath = $org->getLogoPath();
        if (!empty($logoPath)) {
            $box = $W->FlowItems(
                $W->ImageThumbnail($org->getLogoPath())
                ->addClass('widget-100pc')
                ->setSizePolicy('widget-20pc')
                ->setThumbnailSize(48, 48)
            )->setSizePolicy('widget-80pc')->setHorizontalSpacing(0.5, 'em')->setVerticalAlign('top');
        }
        
        $box = $W->VBoxItems(
            $box,
            $W->VBoxItems(
                $W->DelayedItem($this->proxy()->orgChartEntityMembers($org->id))
                ->setPlaceHolderItem($W->Label($App->translate('Loading...')))
            )->addClass('members')
        );
        
        return $box;
    }

    public function orgChartEntityMembers($id, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $recordSet = $App->OrganizationSet();
        $record = $recordSet->request($id);

        $contactOrganizationSet = $App->ContactOrganizationSet();

        $today = date('Y-m-d');
        $contactOrganizations = $contactOrganizationSet->select(
            $contactOrganizationSet->all(
                $contactOrganizationSet->organization->is($record->id),
                $contactOrganizationSet->isActive($today)
            )
        );
        $contactOrganizations->orderAsc($contactOrganizationSet->organizationRank);

        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $previousPosition = null;

        foreach ($contactOrganizations as $contactOrganization) {
            $contact = $contactOrganization->contact();
            if (!$contact) {
                continue;
            }
            if ($contactOrganization->position !== $previousPosition) {
                $box->addItem(
                    $W->Label($contactOrganization->position)
                        ->addClass('member_role')
                );
                $previousPosition = $contactOrganization->position;
            }
            $box->addItem(
                $W->Link(
                    $contact->getFullName(),
                    $App->Controller()->Contact()->display($contact->id)
                )->addClass('member')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
        }

        $box->setSizePolicy('widget-100pc');

        $box->setReloadAction($this->proxy()->orgChartEntityMembers($record->id, $box->getId()));
        return $box;
    }

    public function addresses($id, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $box = $W->VBoxLayout($itemId);
        $box->setIconFormat(16, 'left');
        
        $mainAddress = $record->address();

        /* @var $addressSet AddressSet */
        $addressSet = $App->AddressSet();
        $addressClassName = $App->AddressClassName();
        $organizationClassName = $App->OrganizationClassName();

        $linkSet = $App->LinkSet();
        $linkSet->hasOne('targetId', $addressClassName);
        $linkSet->targetId();

        // Main address first
        $addressLinks = $linkSet->select(
            $linkSet->sourceClass->is($organizationClassName)
            ->_AND_($linkSet->sourceId->is($record->id))
            ->_AND_($linkSet->targetClass->is($addressClassName))
            ->_AND_($linkSet->targetId->is($mainAddress->id))
        );
        
        

        foreach ($addressLinks as $addressLink) {
            $box->addItem($this->addressLink($record, $addressLink, $mainAddress));
        }

        // Then other addresses
        $addressLinks = $linkSet->select(
            $linkSet->sourceClass->is($organizationClassName)
            ->_AND_($linkSet->sourceId->is($record->id))
            ->_AND_($linkSet->targetClass->is($addressClassName))
            ->_AND_($linkSet->targetId->isNot($mainAddress->id))
        );
        
        foreach ($addressLinks as $addressLink) {
            $box->addItem($this->addressLink($record, $addressLink, $mainAddress));
        }

        $box->setReloadAction($this->proxy()->addresses($record->id, $box->getId()));

        $box->addClass('depends-organization-address');

        return $box;
    }

    private function addressLink($record, $addressLink, $mainAddress)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $controller = $this->proxy();

        list(,$addressType) = explode(':', $addressLink->type);
        $address = $addressLink->targetId;
        $addressBox = $W->VBoxItems(
            $W->Label($addressType)->addClass('badge', 'small'),
            $W->Html(bab_toHtml($address->street, BAB_HTML_BR)),
            $W->FlowItems(
                $W->Label($address->postalCode),
                $W->Label($address->city),
                $W->Label($address->cityComplement)
            )->setHorizontalSpacing(1, 'ex'),
            $W->Label(isset($address) && $address->country() ? $address->country()->getName() : '')
        );

        $buttonsBox = $W->FlowItems();
        if ($record->isUpdatable()) {
            if ($mainAddress->id != $addressLink->targetId->id) {
                $buttonsBox->addItem(
                    $W->Link('', $controller->setMainAddress($addressLink->sourceId, $addressLink->targetId->id))
                    ->setTitle('Mark this address as the main address')
                    ->setAjaxAction(null, '')
                    ->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC)
                    ->addClass('icon', \Func_Icons::ACTIONS_ARROW_UP)
                    );
            }
            $buttonsBox->addItem(
                $W->Link('', $controller->editLinkedAddress($addressLink->id))
                ->setTitle($App->translate('Edit'))
                ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                ->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC)
                ->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
                );
            $buttonsBox->addItem(
                $W->Link('', $controller->deleteLinkedAddress($addressLink->id))
                ->setAjaxAction(null, '')
                ->setTitle($App->translate('Remove'))
                ->setConfirmationMessage($App->translate('Are you sure you want to delete this address?'))
                ->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC)
                ->addClass('icon', \Func_Icons::ACTIONS_LIST_REMOVE)
                );
        }

        return $W->FlowItems(
            $addressBox,
            $buttonsBox->setSizePolicy('pull-right')
            ->addClass('widget-actions')
        )->setSizePolicy('widget-list-element')
        ->addClass('widget-actions-target')
        ->setVerticalAlign('top');
    }

    /**
     * Make the address the main address for the organization.
     *
     */
    public function setMainAddress($organization = null, $address = null, $type = null)
    {
        $App = $this->App();

        $organizationSet = $App->OrganizationSet();
        $organization = $organizationSet->get($organization);

        $organization->address = $address;
        $organization->save();

        $this->addReloadSelector('.depends-organization-address');

        return true;
    }
    public function editLinkedAddress($link)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $Ui = $App->Ui();
        $page = $Ui->Page();

        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Edit address'));


        $addressSet = $App->AddressSet();

        $linkSet = $App->LinkSet();

        $linkSet->hasOne('targetId', get_class($addressSet));

        $linkSet->targetId();

        $linkedAddress = $linkSet->get($link);


        $form = $W->Form();
        $form->setName('link');
        $form->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $form->setHiddenValue('link[id]', $linkedAddress->id);

        $form->addItem(
            $this->typedAddress()
        );

        $form->addItem(
            $W->SubmitButton()
            ->setAction($App->Controller()->Organization()->saveLinkedAddress())
            ->setAjaxAction(null, '')
            ->setLabel($App->translate('Save'))
        );

        $type = substr($linkedAddress->type, strlen('hasAddressType:'));
        $form->setValue(array('link', 'type'), $type);

        $form->setValues($linkedAddress->targetId->getValues(), array('link', 'address'));

        $page->addItem($form);

        return $page;
    }

    /**
     * Removes the association with an address.
     */
    public function deleteLinkedAddress($link)
    {
        $App = $this->App();

        $addressSet = $App->AddressSet();
        $organizationSet = $App->OrganizationSet();

        $linkSet = $App->LinkSet();

        $linkSet->hasOne('targetId', get_class($addressSet));
        $linkSet->targetId();
        $linkSet->hasOne('sourceId', get_class($organizationSet));
        $linkSet->sourceId();

        $linkedAddress = $linkSet->get($link);

        $organization = $linkedAddress->sourceId;

        $linkedAddress->delete();

        $organization->updateMainAddress();

        $this->addReloadSelector('.depends-organization-address');

        return true;
    }



    /**
     * @param int   $id
     * @param int   $width
     * @param int   $height
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function logo($id, $width = 500, $height = 250, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->logo($record->id, $width, $height, $box->getId()));
        $box->addClass('widget-imagepicker-container');
        
        $Ui = $App->ui();
        
        $photoItem = $Ui->OrganizationLogo($record, $width, $height, false, false);
        $photoItem->addClass('img-responsive');
        
        $box->addItem(
            $photoItem
        );

        if ($record->isUpdatable()) {
            $imagesFormItem = $W->ImagePicker();
            $imagesFormItem->addClass('widget-imagepicker');
            $imagesFormItem->oneFileMode();
            $imagesFormItem->hideFiles();

            $imagesFormItem->setAssociatedDropTarget($photoItem);

            $photoUploadPath = $record->getLogoUploadPath();
            if (!is_dir($photoUploadPath->toString())) {
                $photoUploadPath->createDir();
            }
            $imagesFormItem->setFolder($photoUploadPath);
            $imagesFormItem->setAjaxAction($this->proxy()->cancel());

            $box->addItem(
                $imagesFormItem
            );
            $box->addClass('App-image-upload');
        }

        if (isset($itemId)) {
            $box->setId($itemId);
        }

        return $box;
    }

    public function cancel()
    {
        return true;
    }
    /**
     *
     * @param int $id   The organization id
     * @return \app_Page
     */
    public function addContact($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $contactSet = $App->ContactSet();
        $contactSet->address();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $page = $Ui->Page();
        $page->addClass('App-page-editor');

        $page->setTitle($App->translate('Add a contact'));

        $editor = new \app_Editor($App, null, $W->VBoxItems()->setVerticalSpacing(2, 'em'));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');

        $editor->addItem(
            $W->Hidden()->setName('id')->setValue($record->id)
        );

        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Contact'),
                $Ui->SuggestContact(),
                'contact'
            )
        );

        $editor->isAjax = $this->isAjaxRequest();

        $editor->setSaveAction($this->proxy()->saveNewContact());

        $page->addItem($editor);

        return $page;
    }


   /**
    * @param array $data
    * @return boolean
    */
    public function saveNewContact($data = null)
    {
        $App = $this->App();

        $recordSet = $this->getRecordSet();

        if (isset($data['id'])) {
            $record = $recordSet->request($data['id']);
        } else {
            return true;
        }

        if ($data['contact_ID'] === 'new') {
            $contactSet = $App->ContactSet();

            $contact = $contactSet->newRecord();

            $contact->firstname = '';
            $contact->lastname = '';

            $fullname = explode(' ', $data['contact']);
            if (count($fullname) === 1) {
                $contact->lastname = $fullname[0];
            } else {
                $contact->firstname = $fullname[0];
                array_shift($fullname);
                $contact->lastname = implode(' ', $fullname);
            }

            $contact->save();
        } else {
            $contact = $App->getRecordByRef($data['contact_ID']);
        }

        $contactOrganizationSet = $App->ContactOrganizationSet();

        $contactOrganization = $contactOrganizationSet->newRecord();
        $contactOrganization->organization = $record->id;
        $contactOrganization->contact = $contact->id;
        $contactOrganization->save();

        $this->addReloadSelector('.depends-contacts');

        return true;
    }
    
    /**
     * @param string $search
     *
     * @return void
     */
    public function suggestOrganization($search = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $suggest = $Ui->SuggestOrganization();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();
        
        die;
    }
    
    /**
     * Search in all organization based on the name, name2, initials of code
     * 
     * @param string $q
     * @return string (json)
     */
    public function search($allowCreation = 'n', $excluded = array(), $q = null){
        $App = $this->App();
        $set = $this->getRecordSet();
        $W = bab_Widgets();
        $W->includePhpClass('Widget_Select2');
        $collection = new \Widget_Select2OptionsCollection();
        
        session_write_close();
        
//         if(strlen($q)<3){
//             $collection->addOption(new \Widget_Select2Option(0, $App->translate("THe research is too short")));
//             header("Content-type: application/json; charset=utf-8");
//             echo $collection->output();
//             die();
//         }

        $criteria = array();
        if(isset($excluded)){
            $criteria[] = $set->id->notIn($excluded);
        }
        
        $criteria[] = $set->any(
            $set->name->contains($q),
            $set->name2->contains($q),
            $set->initials->contains($q),
            $set->code->contains($q)
        );
        
        $organizations = $set->select(
            $set->all($criteria)
        )->orderAsc($set->name);
            
        $counter = 0;
        foreach($organizations as $organization){
            $counter++;
            $collection->addOption(
                new \Widget_Select2Option($organization->id, $organization->name)
            );
        }
        
        if($allowCreation == 'y' && !empty($q)){
            $collection->addOption(
                new \Widget_Select2Option($q, $q, $App->translate('New organization'))
            );
        }
        
        header("Content-type: application/json; charset=utf-8");
        echo $collection->output();
        die();
    }
}