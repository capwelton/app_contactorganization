<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

use Capwelton\App\ContactOrganization\Set\ContactOrganization;

$App = app_App();
$App->includeRecordController();


/**
 * This controller manages actions that can be performed on contacts-organizations.
 *
 * @method \Func_App App()
 */
class ContactOrganizationController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ContactOrganization'));
    }
    
    /**
     * Returns a box containing the list of contactOrganizations for the specified contact.
     *
     * @param int $contact
     * @param string $itemId
     * @return \Widget_Layout
     */
    public function listForContact($contact, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->listForContact($contact, $box->getId()));
        $box->addClass('depends-ContactOrganization');
        $box->setIconFormat(16, 'left');

        $contactSet = $App->ContactSet();
        $contact = $contactSet->request($contact);

        $box->addItem(
            $W->Link(
                '',
                $this->proxy()->addForContact($contact->id)
            )->addClass('section-button', 'widget-actionbutton')
            ->setOpenMode(\Widget_Link::OPEN_MODAL)
            ->setIcon(\Func_Icons::ACTIONS_LIST_ADD)
        );

        $recordSet = $this->getRecordSet();
        $recordSet->contact();
        $recordSet->organization();

        $contactOrganizations = $recordSet->select(
            $recordSet->contact->is($contact->id)
            ->_AND_($recordSet->organization->deleted->is(0))
        );
        $contactOrganizations->orderAsc($recordSet->contactRank);


        $positionsBox = $W->Form()
            ->setLayout($W->VBoxItems()->sortable($contactOrganizations->count() > 1))
            ->setName('data');

        $box->addItem($positionsBox);

        foreach ($contactOrganizations as $contactOrganization) {

            $emailItem = null;
            $phoneItem = null;

            if ($contactOrganization->email) {
                $emailItem = $recordSet->email->outputWidget($contactOrganization->email)
                    ->addClass('widget-small', 'icon', \Func_Icons::OBJECTS_EMAIL);
            }
            if ($contactPhone = $contactOrganization->getMainPhoneRecord()) {
                $phoneItem = $contactPhone->displayPhone()
                    ->addClass('widget-small');
            }

            $positionItem = $W->HBoxItems(
                $W->Hidden()
                    ->setName(array($contactOrganization->id, 'id'))
                    ->setValue($contactOrganization->id),
                $W->VBoxItems(
                    $W->Link(
                        $contactOrganization->organization->name,
                        $App->Controller()->Organization()->display($contactOrganization->organization->id)
                    )->addClass('widget-strong'),
                    $W->Label(
                        $contactOrganization->position
                    )->addClass('widget-small'),
                    $W->Label(
                        $contactOrganization->type()->name
                    )->addClass('widget-small')
                )->setSizePolicy('widget-50pc')->setIconFormat(16, 'left'),
                $W->VBoxItems(
                    $emailItem,
                    $phoneItem
                )->setSizePolicy('widget-40pc icon-left-10 icon-left icon-10x10'),
                $W->HBoxItems(
                    $W->Link(
                        '',
                        $this->proxy()->edit($contactOrganization->id)
                    )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                    $W->Link(
                        '',
                        $this->proxy()->confirmDelete($contactOrganization->id)
                    )->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction()
                )->setSizePolicy('widget-10pc')
                ->addClass('widget-actions')
            );


            $positionItem->setVerticalAlign('middle');
            $positionItem->addClass('widget-100pc');
            $positionItem->setSizePolicy('widget-list-element widget-actions-target');

            $positionsBox->addItem($positionItem);

            if ($contactOrganization->end != '0000-00-00') {
                if ($contactOrganization->end < date('Y-m-d')) {
                    $positionItem->addClass('crm-deleted');
                }
                if ($contactOrganization->start != '0000-00-00') {
                    $position = sprintf(
                        $App->translate('%s from %s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->start), false),
                        bab_shortDate(bab_mktime($contactOrganization->end), false)
                    );
                } else {
                    $position = sprintf(
                        $App->translate('%s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->end), false)
                    );
                }
                $positionItem->setTitle($position);
            }
            elseif ($contactOrganization->start != '0000-00-00'){
                $position = sprintf(
                    $App->translate('%s since %s'),
                    $contactOrganization->position,
                    bab_shortDate(bab_mktime($contactOrganization->start), false)
                );
                $positionItem->setTitle($position);
            }
        }

        $positionsBox->setAjaxAction($this->proxy()->saveContactRanks(), '', 'change sortupdate');
        
        return $box;
    }


    /**
     * Returns a box containing the list of contactOrganizations for the specified organization.
     *
     * @param int $organization
     * @param string $itemId
     * @return \Widget_Layout
     */
    public function listForOrganization($organization, $limit = null, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->listForOrganization($organization, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        $box->addClass('depends-ContactOrganization');
        $box->addClass('depends-ContactOrganizationPhones');
        $box->setIconFormat(16, 'left');

        if (!isset($limit)) {
            $limit = 5;
        }

        $organizationSet = $App->OrganizationSet();
        $organization = $organizationSet->request($organization);

        $recordSet = $this->getRecordSet();
        $recordSet->contact();
        $recordSet->organization();

        $contactOrganizations = $recordSet->select(
            $recordSet->organization->is($organization->id)
            ->_AND_($recordSet->contact->deleted->is(0))
        );
        $contactOrganizations->orderAsc($recordSet->organizationRank);

        $positionsBox = $W->Form()
            ->setLayout($W->VBoxItems()->sortable($contactOrganizations->count() > 1))
            ->setName('data');
        $positionsBox->addClass("widget-no-close");
        $box->addItem($positionsBox);

        $nbRecords = $contactOrganizations->count();
        $nbItems = 0;
        
        $Ui = $App->Ui();
        
        foreach ($contactOrganizations as $contactOrganization) {
            /* @var $contactOrganization ContactOrganization */
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($App->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->listForOrganization($organization->id, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            if ($contactOrganization->email) {
                $email = $contactOrganization->email;
            } else {
                $email = $contactOrganization->contact->email;
            }

            $phone = $W->Label();
            $contactPhone = $contactOrganization->getMainPhoneRecord();
            if($contactPhone){
                $phone = $contactPhone->displayPhone();
            }
            else{
                $contactPhone = $contactOrganization->contact->getMainPhoneRecord();
                if($contactPhone){
                    $phone = $contactPhone->displayPhone();
                }
            }
            
            $phone->addClass('widget-small');
            
            $coordinateBox = $W->VBoxItems()->setSizePolicy('widget-40pc icon-left-10 icon-left icon-10x10');
            if(!empty($email)){
                $coordinateBox->addItem(
                    $W->Link(
                        $email,
                        'mailto:' . $email
                    )->addClass('widget-small')
                );
            }
            $coordinateBox->addItem($phone);
            
            $position = $contactOrganization->position;
            if (!empty($contactOrganization->service)) {
                $position .=  ' / ' . $contactOrganization->service;
            }

            $positionItem = $W->HBoxItems(
                $W->Hidden()
                    ->setName(array($contactOrganization->id, 'id'))
                    ->setValue($contactOrganization->id),
                $Ui->ContactAvatar($contactOrganization->contact, 28, 28),
                $W->VBoxItems(
                    $W->Link(
                        $contactOrganization->contact->getFullName(),
                        $App->Controller()->Contact()->display($contactOrganization->contact->id)
                    )->addClass('widget-strong'),
                    $W->Label(
                        $position
                    )->addClass('widget-small')
                )->setSizePolicy('widget-50pc'),
                $coordinateBox
            );

            $positionItem->addItem(
                $W->HBoxItems(
                    $W->Link(
                        '',
                        $this->proxy()->edit($contactOrganization->id)
                    )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                    $W->Link(
                        '',
                        $this->proxy()->confirmDelete($contactOrganization->id)
                    )->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction()
                )->setSizePolicy('widget-10pc')
                ->addClass('widget-actions')
            );

            $positionItem->setVerticalAlign('middle');
            $positionItem->addClass('widget-100pc');
            $positionItem->setSizePolicy('widget-list-element widget-actions-target');
            $positionItem->setHorizontalSpacing(4, 'px');

            $positionsBox->addItem($positionItem);


            if ($contactOrganization->end != '0000-00-00') {
                if ($contactOrganization->end < date('Y-m-d')) {
                    $positionItem->addClass('crm-deleted');
                }
                if ($contactOrganization->start != '0000-00-00') {
                    $position = sprintf(
                        $App->translate('%s from %s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->start), false),
                        bab_shortDate(bab_mktime($contactOrganization->end), false)
                    );
                } else {
                    $position = sprintf(
                        $App->translate('%s until %s'),
                        $contactOrganization->position,
                        bab_shortDate(bab_mktime($contactOrganization->end), false)
                    );
                }
                $positionItem->setTitle($position);
            }
        }

        $positionsBox->setAjaxAction($this->proxy()->saveOrganizationRanks(), '', 'change sortupdate');
        return $box;
    }



    public function saveContactRanks($data = null)
    {
        $recordSet = $this->getRecordSet();

        $previousRecord = null;

        foreach (array_keys($data) as $recordId) {
            $record = $recordSet->request($recordId);
            if (isset($previousRecord) && $previousRecord->contactRank > $record->contactRank) {
                $rank = $record->contactRank;
                $record->contactRank = $previousRecord->contactRank;
                $previousRecord->contactRank = $rank;

                $record->save();
                $previousRecord->save();
            }

            $previousRecord = $record;
        }

        $this->addReloadSelector('.depends-ContactOrganization');
        return true;
    }

    public function saveOrganizationRanks($data = null)
    {
        $recordSet = $this->getRecordSet();
        $compteur = 0;
        $arrayKeys = array_keys($data);
        foreach ($arrayKeys as $recordId) {
            $record = $recordSet->request($recordId);
                $compteur++;
                $record->organizationRank = $compteur;
                $record->save();
        }

        $this->addReloadSelector('.depends-ContactOrganization');
        return true;
    }

    /**
     * Displays a page to add a position in an organization for a contact.
     *
     * @param int       $id     The ContactOrganization id
     * @return \app_Page
     */
    public function addForContact($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $page = $Ui->Page();

        $page->setTitle($App->translate('Add a position in an organization'));

        $contactOrganizationSet = $App->ContactOrganizationSet();

        $contactOrganization = $contactOrganizationSet->newRecord();
        $contactOrganization->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
        $contactOrganization->contact = (int)$id;
        $contactOrganization->save();

        $editor = $this->recordEditor();
        $editor->setForContactMode();
        $editor->setRecord($contactOrganization);

        $phoneEditor = $App->Controller()->ContactPhone(false)->listForContactOrganization($contactOrganization->id);
        
        $page->addItem($editor);
        
        $page->addItem(
            $phoneEditor
        );
        
        $page->addClass('widget-no-close');
        return $page;
    }
    /**
     * Displays a page to add a contact to an organization.
     *
     * @param int       $id     The ContactOrganization id
     * @return \app_Page
     */
    public function addForOrganization($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $page = $Ui->Page();

        $page->setTitle($App->translate('Add contact'));
        
        $contactOrganizationSet = $App->ContactOrganizationSet();

        $contactOrganization = $contactOrganizationSet->newRecord();
        $contactOrganization->organization = (int)$id;
        $contactOrganization->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
        $contactOrganization->save();

        $editor = $this->recordEditor();
        $editor->setForOrganizationMode();
        $editor->setRecord($contactOrganization);

        $phoneEditor = $App->Controller()->ContactPhone(false)->listForContactOrganization($contactOrganization->id);
        
        $page->addItem($editor);
        
        $page->addItem(
            $phoneEditor
        );
        
        $page->addClass('widget-no-close');
        return $page;
    }

    /**
     * Displays a page to edit the position of a contact in an organization.
     *
     * @param int       $id     The ContactOrganization id
     * @return \app_Page
     */
    public function edit($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $page = $Ui->Page();

        $page->setTitle($App->translate('Edit contact position in organization'));

        $contactOrganizationSet = $App->ContactOrganizationSet();

        $contactOrganization = $contactOrganizationSet->request($id);

        $editor = $this->recordEditor();
        $editor->setForOrganizationMode();
        $editor->setForContactMode();
        $editor->setRecord($contactOrganization);
        $editor->addClass('widget-no-close');
        
        $phoneEditor = $App->Controller()->ContactPhone(false)->listForContactOrganization($contactOrganization->id); 
        
        $page->addItem($editor);
        
        $page->addItem(
            $phoneEditor
        );
        
        $page->addClass('widget-no-close');
        return $page;
    }

    
    /**
     * Interface to edit only the email part
     * 
     * @param int $id
     * @return \app_Page
     */
    public function editEmail($id)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $Ui = $App->Ui();
        $page = $Ui->Page();
        
        $page->setTitle($App->translate('Edit contact email'));
        
        $contactOrganizationSet = $App->ContactOrganizationSet();
        
        $contactOrganization = $contactOrganizationSet->request($id);
        
        $editor = new \app_Editor($App);
        $editor->setName('data');
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->addClass('widget-no-close');
        
        $editor->addItem(
            $W->Hidden()->setName('id')
        );
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Email'),
                $W->EmailLineEdit(),
                'email'
            )
        );
        
        $editor->setSaveAction($this->proxy()->save());
        $editor->isAjax = $this->isAjaxRequest();
        $page->addItem($editor);
        
        $editor->setValues($contactOrganization->getFormOutputValues(), array('data'));

        
        $page->addClass('widget-no-close');
        return $page;
    }

    /**
     * {@inheritDoc}
     * @see \app_CtrlRecord::save()
     */
    public function save($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        $recordSet = $this->getSaveRecordSet();
        $recordSet->setDefaultCriteria($recordSet->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        $pk = $recordSet->getPrimaryKey();
        $message = null;

        if (!empty($data[$pk])) {
            $record = $recordSet->request($data[$pk]);
            unset($data[$pk]);

            if (!$record->isUpdatable()) {
                throw new \app_AccessException('Access denied');
            }

            $message = $this->getModifedMessage($record);
        } else {

            $record = $recordSet->newRecord();

            if (!$recordSet->isCreatable()) {
                throw new \app_AccessException('Access denied');
            }

            $message = $this->getCreatedMessage();
        }

        if (!isset($record)) {
            throw new \app_SaveException($App->translate('The record does not exists'));
        }

        if (!isset($data)) {
            throw new \app_SaveException($App->translate('Nothing to save'));
        }

        if ($data['contact'] === 'new') {
            $contactSet = $App->ContactSet();
            
            $contact = $contactSet->newRecord();
            
            $contact->firstname = isset($data['contactFirstname']) ? $data['contactFirstname'] : '';
            $contact->lastname = isset($data['contactLastname']) ? $data['contactLastname'] : '';
            
            
            $contact->save();
            $data['contact'] = (int)$contact->id;
        }

        $record->setFormInputValues($data);

        $this->preSave($record, $data);
        if ($record->save()) {
            $this->postSave($record, $data);
            if (is_string($message)) {
                $this->addMessage($message);
            }
            
            $this->addReloadSelector('.depends-' . $this->getRecordClassName());
            return true;
        }

        return true;
    }
    
    protected function preSave($record, $data)
    {
        parent::preSave($record, $data);
        $record->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        $record->save();
    }
    
    public function SuggestContact($recordId, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $set = $App->ContactOrganizationSet();
        $set->setDefaultCriteria(null);
        $record = $set->get($set->id->is($recordId));
        
        if(!isset($itemId)){
            $_SESSION['SuggestContact_values'] = null;
            unset($_SESSION['SuggestContact_values']);
        }
        
        $box = $W->FlowLayout($itemId);
        $box->addClass('depends-SuggestContact');
        $box->addClass('widget-100pc');
        $box->setReloadAction($this->proxy()->SuggestContact($recordId, $box->getId()));
        
        if(!isset($record)){
            return $box;
        }
        
        $box->addItem(
            $W->LabelledWidget(
                $App->translate('Contact'),
                $W->VBoxItems(
                    $select2 = $W->Select2()->setName(isset($itemId) ? array('data', 'contact') : 'contact'),
                    $detailsBox = $W->HBoxItems(
                        $W->LabelledWidget(
                            $App->translate('Firstname'),
                            $firstnameEdit = $W->LineEdit(),
                            isset($itemId) ? array('data', 'contactFirstname') : 'contactFirstname'
                        ),
                        $W->LabelledWidget(
                            $App->translate('Lastname'),
                            $lastnameEdit = $W->LineEdit(),
                            isset($itemId) ? array('data', 'contactLastname') : 'contactLastname'
                        )
                    )
                )
            )->setSizePolicy('widget-100pc')
        );
        
        if(isset($_SESSION['SuggestContact_values']) && isset($_SESSION['SuggestContact_values']['firstname']) && isset($_SESSION['SuggestContact_values']['lastname']) && isset($_SESSION['SuggestContact_values']['id'])){
            $firstnameEdit->setValue($_SESSION['SuggestContact_values']['firstname']);
            $lastnameEdit->setValue($_SESSION['SuggestContact_values']['lastname']);
            $select2->addOption($_SESSION['SuggestContact_values']['id'], $_SESSION['SuggestContact_values']['firstname'] . ' ' . $_SESSION['SuggestContact_values']['lastname']);
        }
        
        $select2->setAssociatedDisplayable($detailsBox, array('new'));
        $select2->setDataSource($this->proxy()->searchContact());
        $select2->setAjaxAction($this->proxy()->contactCreationFromSuggest(), null, 'select2:close');
        
        return $box;
    }
    
    public function contactCreationFromSuggest($data = null)
    {
        if(!isset($data['contact'])){
            return true;
        }
        $contactId = $data['contact'];
        if($contactId == 'new' && isset($_SESSION['SuggestContact_values']['q'])){
            $q = $_SESSION['SuggestContact_values']['q'];
            $parts = explode(' ', $q);
            $firstname = '';
            $lastname = '';
            if(isset($parts[0])){
                $firstname = $parts[0];
            }
            if(isset($parts[1])){
                unset($parts[0]);
                $lastname = implode(' ', $parts);
            }
            $_SESSION['SuggestContact_values'] = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'id' => 'new'
            );
        }
        else{
            $_SESSION['SuggestContact_values'] = array(
                'firstname' => '',
                'lastname' => '',
                'id' => 0
            );
            $set = $this->App()->ContactSet();
            $contact = $set->get($set->id->is($contactId));
            if($contact){
                $_SESSION['SuggestContact_values'] = array(
                    'firstname' => $contact->firstname,
                    'lastname' => $contact->lastname,
                    'id' => $contact->id
                );
            }
        }
        $this->addReloadSelector('.depends-SuggestContact');
        return true;
    }
    
    public function searchContact($q = null)
    {
        $App = $this->APp();
        $set = $App->ContactSet();
        $set->addFields(
            $set->firstname->concat(' ')->concat($set->lastname)->setName('fullname')
        );
        
        $W = bab_Widgets();
        $W->includePhpClass('Widget_Select2');
        
        $collection = new \Widget_Select2OptionsCollection();
        
        $contacts = $set->select(
            $set->any(
                array(
                    $set->firstname->contains($q),
                    $set->lastname->contains($q),
                    $set->fullname->contains($q),
                    $set->fullname->soundsLike($q)
                )
            )
        );
        
        $_SESSION['SuggestContact_values'] = array(
            'q' => $q
        );
        
        session_write_close();
        
        foreach($contacts as $contact){
            $collection->addOption(
                new \Widget_Select2Option($contact->id, $contact->getFullName())
            );
        }
        
        if(!empty($q)){
            $collection->addOption(
                new \Widget_Select2Option('new', $q, $App->translate('New contact'))
            );
        }
        
        header("Content-type: application/json; charset=utf-8");
        echo $collection->output();
        die();
    }
}
