<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

use Capwelton\App\ContactOrganization\Set\ContactPhone;

$App = app_App();
$App->includeRecordController();



/**
 * This controller manages actions that can be performed on contactPhone.
 *
 * @method Func_App         App()
 * @method ContactPhoneSet  getRecordSet()
 */
class ContactPhoneController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ContactPhone'));
    }
    
    /**
     * Returns a box containing the list of contactPhones for the specified contact.
     *
     * @param int $contact
     * @param string $itemId
     * @return \Widget_Layout
     */
    public function listForContact($contact, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $contactSet = $App->ContactSet();
        $contact = $contactSet->request($contact);
        
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->listForContact($contact->id, $box->getId()));
        $box->addClass('depends-'.$this->getRecordClassName());
        $box->setIconFormat(16, 'left');
        
        $box->addItem(
            $W->Link(
                '',
                $this->proxy()->addForContact($contact->id)
            )->addClass('section-button', 'widget-actionbutton')
            ->setOpenMode(\Widget_Link::OPEN_MODAL)
            ->setIcon(\Func_Icons::ACTIONS_LIST_ADD)
        );
        
        $recordSet = $this->getRecordSet();
        $recordSet->contact();
        
        $contactPhones = $recordSet->select(
            $recordSet->contact->is($contact->id)
            ->_AND_($recordSet->contactOrganization->is(0))
        );
        $contactPhones->orderAsc($recordSet->rank);
        
        
        $positionsBox = $W->Form()
        ->setLayout($W->VBoxItems()->sortable($contactPhones->count() > 1))
        ->setName('contactPhoneData')
        ->setIconFormat(16, 'left')
        ->setHiddenValue('tg', $App->controllerTg)
        ->addClass('widget-no-close');
        
        $box->addItem($positionsBox);
        
        foreach ($contactPhones as $contactPhone) {
            /* @var $contactPhone ContactPhone */
            $phoneItem = $recordSet->phone->outputWidget($contactPhone->phone)
                ->addClass('widget-small')
                ->setIcon(\Func_Icons::OBJECTS_PHONE);
            
            $phoneItem = $contactPhone->displayPhone();
            
            $titleItem = $W->Label(
                $contactPhone->getTitle()
            )->addClass('widget-small');
                
            $positionItem = $W->HBoxItems(
                $W->Hidden()
                ->setName(array('contactPhoneId', $contactPhone->id))
                ->setValue($contactPhone->id),
                $phoneItem,
                $titleItem,
                $W->HBoxItems(
                    $W->Link(
                        '',
                        $this->proxy()->edit($contactPhone->id)
                    )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                    $W->Link(
                        '',
                        $this->proxy()->confirmDelete($contactPhone->id)
                    )->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction()
                )->setSizePolicy('widget-10pc')
                ->addClass('widget-actions')
            );
                
            $positionItem->setVerticalAlign('middle');
            $positionItem->addClass('widget-100pc');
            $positionItem->setSizePolicy('widget-list-element widget-actions-target');
            
            $positionsBox->addItem($positionItem);
        }
        
        $positionsBox->setAjaxAction($this->proxy()->savePhoneRanks(), '', 'change sortupdate');
        
        return $box;
    }
    
    /**
     * Returns a box containing the list of contactPhones for the specified contactOrganization.
     *
     * @param int $contact
     * @param string $itemId
     * @return \Widget_Layout
     */
    public function listForContactOrganization($contactOrganization, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $contactOrganizationSet = $App->ContactOrganizationSet();
        $contactOrganizationSet->setDefaultCriteria($contactOrganizationSet->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        $contactOrganization = $contactOrganizationSet->request($contactOrganization);
        
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->listForContactOrganization($contactOrganization->id, $box->getId()));
        $box->addClass('depends-ContactOrganizationPhones');
        $box->setIconFormat(16, 'left');
        
        $box->addItem(
            $W->Section(
                $App->translate('Phones'),
                $sectionContent = $W->VBoxItems()
            )->addClass('box')
        );
        
        $sectionContent->addItem(
            $W->Link(
                '',
                $this->proxy()->addForContactOrganization($contactOrganization->id)
            )->addClass('section-button', 'widget-actionbutton')
            ->setOpenMode(\Widget_Link::OPEN_MODAL)
            ->setIcon(\Func_Icons::ACTIONS_LIST_ADD)
        );
        
        $recordSet = $this->getRecordSet();
        $recordSet->contactOrganization();
        
        $contactPhones = $recordSet->select(
            $recordSet->contactOrganization->is($contactOrganization->id)
        );
        $contactPhones->orderAsc($recordSet->rank);
        
        
        $positionsBox = $W->Form()
        ->setName('contactPhoneData')
        ->setLayout($W->VBoxItems()->sortable($contactPhones->count() > 1))
        ->setIconFormat(16, 'left')
        ->setHiddenValue('tg', $App->controllerTg)
        ->addClass('widget-no-close');
        
        $sectionContent->addItem($positionsBox);
        
        foreach ($contactPhones as $contactPhone) {
            /* @var $contactPhone ContactPhone */

            $phoneItem = $contactPhone->displayPhone(true, true);
            
            $titleItem = null;
            if(!empty($contactPhone->title)){
                $titleItem = $W->Label(
                    $contactPhone->title
                )->addClass('widget-small');
            }
                
            $positionItem = $W->HBoxItems(
                $W->Hidden()
                ->setName(array('contactPhoneId', $contactPhone->id))
                ->setValue($contactPhone->id),
                $phoneItem,
                $titleItem,
                $W->HBoxItems(
                    $W->Link(
                        '',
                        $this->proxy()->edit($contactPhone->id)
                    )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                    $W->Link(
                        '',
                        $this->proxy()->confirmDelete($contactPhone->id)
                    )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction()
                )->setSizePolicy('widget-10pc')
                ->addClass('widget-actions')
            );
                
            $positionItem->setVerticalAlign('middle');
            $positionItem->addClass('widget-100pc');
            $positionItem->setSizePolicy('widget-list-element widget-actions-target');
            
            $positionsBox->addItem($positionItem);
        }
        
        $positionsBox->setAjaxAction($this->proxy()->saveOrganizationPhoneRanks(), '', 'change sortupdate');
        
        return $box;
    }
    
    public function savePhoneRanks($contactPhoneData = null)
    {
        $recordSet = $this->getRecordSet();
        $previousRecord = null;
        $recordIds = isset($contactPhoneData['contactPhoneId']) ? $contactPhoneData['contactPhoneId'] : array();
        
        foreach ($recordIds as $recordId) {
            $record = $recordSet->request($recordId);
            if (isset($previousRecord) && $previousRecord->rank > $record->rank) {
                $rank = $record->rank;
                $record->rank = $previousRecord->rank;
                $previousRecord->rank = $rank;
                
                $record->save();
                $previousRecord->save();
            }
            
            $previousRecord = $record;
        }
        
        $this->addReloadSelector('.depends-'.$this->getRecordClassName());
        return true;
    }
    
    public function saveOrganizationPhoneRanks($contactPhoneData = null)
    {
        $recordSet = $this->getRecordSet();
        $recordIds = isset($contactPhoneData['contactPhoneId']) ? $contactPhoneData['contactPhoneId'] : array();
        
        $rank = 0;
        
        foreach ($recordIds as $recordId) {
            $record = $recordSet->request($recordId);
            $record->rank = $rank;  
            $record->save();
            $rank ++;
        }
        
        $this->addReloadSelector('.depends-ContactOrganizationPhones');
        $this->addReloadSelector('.depends-ContactOrganization');
        return true;
    }
    
    /**
     * Displays a page to add a phone for a contact.
     *
     * @param int       $id     The contact id
     * @return \app_Page
     */
    public function addForContact($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $page = $Ui->Page();
        
        $page->setTitle($App->translate('Add a phone number'));
        
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->newRecord();
        $record->contact = (int)$id;
        $record->rank = $recordSet->select($recordSet->contact->is($id))->count();
        
        $editor = $this->recordEditor();
        $editor->setRecord($record);
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * Displays a page to add a phone for a contactOrganization.
     *
     * @param int       $id     The ContactOrganization id
     * @return \app_Page
     */
    public function addForContactOrganization($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $page = $Ui->Page();
        
        $page->setTitle($App->translate('Add a phone number'));
        
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->newRecord();
        $record->contactOrganization = (int)$id;
        $record->rank = $recordSet->select($recordSet->contactOrganization->is($id))->count();
        
        $editor = $this->recordEditor();
        $editor->setRecord($record);
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function save($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        $recordSet = $this->getSaveRecordSet();
        $pk = $recordSet->getPrimaryKey();
        $message = null;
        
        if (!empty($data[$pk])) {
            $record = $recordSet->request($data[$pk]);
            unset($data[$pk]);
            
            if (!$record->isUpdatable()) {
                throw new \app_AccessException('Access denied');
            }
            
            $message = $this->getModifedMessage($record);
        } else {
            
            $record = $recordSet->newRecord();
            
            if (!$recordSet->isCreatable()) {
                throw new \app_AccessException('Access denied');
            }
            
            $message = $this->getCreatedMessage();
        }
        
        if (!isset($record)) {
            throw new \app_SaveException($App->translate('The record does not exists'));
        }
        
        
        if (!isset($data)) {
            throw new \app_SaveException($App->translate('Nothing to save'));
        }
        
        $record->setFormInputValues($data);
        
        
        $this->preSave($record, $data);
        if ($record->save()) {
            $this->postSave($record, $data);
            if (is_string($message)) {
                $this->addMessage($message);
            }
            
            if($record->contact){
                $this->addReloadSelector('.depends-' . $this->getRecordClassName());
            }
            if($record->contactOrganization){
                $this->addReloadSelector('.depends-ContactOrganizationPhones');
            }
            return true;
        }
        
        return true;
    }
}
