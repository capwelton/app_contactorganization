<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

use Capwelton\App\ContactOrganization\Set\ContactAcquaintanceLevelSet;
use Capwelton\App\ContactOrganization\Set\Contact;
use Capwelton\App\ContactOrganization\Set\ContactAcquaintance;

$App = app_App();
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on contact acquaintances.
 *
 * @method \Func_App    App()
 */
class ContactAcquaintanceController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ContactAcquaintance'));
    }
    
    
    /**
     *
     * @param int $contact
     * @throws \app_Exception
     * @return \app_Page
     */
    public function add($contact)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        if (!isset($contact)) {
            throw new \app_Exception($App->translate('Trying to access a contact with a wrong (unknown) id'));
        }
        
        $page = $Ui->Page();
        $page->addClass('app-page-editor');
        
        $contactSet = $App->ContactSet();
        $contact = $contactSet->request($contact);
        $page->setTitle($App->translate('Add contact acquaintance'), 1);
        
        $set = $this->getRecordSet();
        $record = $set->newRecord();
        $record->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
        $record->contactKnown = $contact->id;
        
        $currentContact = $contactSet->getCurrentContact();
        if($currentContact && $contact->id != $currentContact->id){
            //Set the default contact knowing to the current contact, only if the contact knowing is not the same as the contact known (a contact cannot have an acquaintance with oneself)
            $record->contactKnowing = $currentContact->id;
        }
        
        $record->save();
        
        $editor = $Ui->ContactAcquaintanceEditor($record);
        $editor->isAjax = $this->isAjaxRequest();
        
        $editor->setHiddenValue('contact', $contact->id);
        $editor->setHiddenValue('tg', bab_rp('tg'));
        
        $editor->setSaveAction($this->proxy()->save());
        $editor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     *
     * @param int $id
     * @throws \app_Exception
     * @return \app_Page
     */
    public function edit($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        $page->addClass('app-page-editor');
        
        $set = $this->getRecordSet();
        $record = $set->request($id);
        $page->setTitle($App->translate('Edit contact acquaintance'), 1);
        
        $editor = $Ui->ContactAcquaintanceEditor($record);
        $editor->isAjax = $this->isAjaxRequest();
        
        $editor->setHiddenValue('tg', bab_rp('tg'));
        
        $editor->setSaveAction($this->proxy()->save());
        $editor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * Saves contact acquaintance.
     *
     * @param int	$contact
     * @param int	$level
     * @return bool
     */
    public function save($data = null)
    {
        $App = $this->App();
        
        $set = $this->getRecordSet();
        $set->setDefaultCriteria($set->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT)));
        
        $record = $set->request($data['id']);
        $record->setFormInputValues($data);
        $record->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        
        $this->preSave($record, $data);
        if ($record->save()) {
            $this->postSave($record, $data);
            $this->addMessage($App->translate('The contact acquaintance has been saved'));
            $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        }
        
        return true;
    }
    
    public function listFor($contact, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->listFor($contact, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        
        $contactSet = $App->ContactSet();
        /* @var $contact Contact */
        $contact = $contactSet->get($contact);
        
        if (!isset($contact)) {
            return $box;
        }
        
        $acquaintances = $contact->selectContactsKnowing();
        
        $internals = array();
        $externals = array();
        
        foreach ($acquaintances as $acquaintance) {
            $level = $acquaintance->level();
            $color = $level->color;
            if(strpos($color, '#') === false){
                $color = "#{$color}";
            }
            
            if($acquaintance->contactKnowing()->isInternal()){
                if(!isset($internals[$level->rank])){
                    $internals[$level->rank] = $W->Section(
                        $level->getRecordTitle(),
                        $W->VBoxItems(),
                        7
                    )->setFoldable(true)->addClass('contactAcquaintanceLevelSection')->addAttribute('style', "color:{$color};border-color:{$color};");
                }
                
                $levelSection = $internals[$level->rank];
            }
            else{
                if(!isset($externals[$level->rank])){
                    $externals[$level->rank] = $W->Section(
                        $level->getRecordTitle(),
                        $W->VBoxItems(),
                        7
                    )->setFoldable(true)->addClass('contactAcquaintanceLevelSection')->addAttribute('style', "color:{$color};border-color:{$color};");
                }
                
                $levelSection = $externals[$level->rank];
            }
            
            $listElement = $this->listItem($acquaintance);
            $levelSection->addItem($listElement);
        }
        
        if(count($internals) > 0){
            $box->addItem(
                $W->Label($App->translate('Internal'))
            );
            
            foreach ($internals as $internal){
                $box->addItem($internal);
            }
        }
        
        if(count($externals) > 0){
            $box->addItem(
                $W->Label($App->translate('External'))
            );
            
            foreach ($externals as $external){
                $box->addItem($external);
            }
        }
        
        return $box;
    }
    
    /**
     * Returns a \Widget_VBoxLayout containing minimal informations about the Acquaintance
     * @param int $id The Note id
     * @return \Capwelton\App\Note\Ui\NoteItem
     */
    public function listItem($id)
    {
        $App = $this->App();
        
        if ($id instanceof ContactAcquaintance) {
            $record = $id;
        } else {
            $record = $this->getRecordSet()->request($id);
        }
        
        return $App->Ui()->ContactAcquaintanceItem($record);
    }
}
