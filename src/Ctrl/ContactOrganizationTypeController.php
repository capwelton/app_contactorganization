<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

$App = app_App();
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on contact organization types.
 *
 * @method \Func_App    App()
 */
class ContactOrganizationTypeController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ContactOrganizationType'));
    }
    
    /**
     * Editor contact organization type form
     *
     * @param int | null $contactOrganizationType
     * @return \app_Page
     */
    public function edit($contactOrganizationType = null, $errors = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $page = $Ui->Page();

        $contactOrganizationTypeSet = $App->ContactOrganizationTypeSet();

        $page->addClass('app-page-editor');

        if (isset($contactOrganizationType)) {
            $page->setTitle($App->translate('Edit contact organization type'));
            if (is_array($contactOrganizationType)) {

            } else if (isset($contactOrganizationType)) {
                $contactOrganizationType = $contactOrganizationTypeSet->request($contactOrganizationType);
            }
        } else {
            $page->setTitle($App->translate('Create a new contact organization type'));
        }
        
        $editor = $Ui->ContactOrganizationTypeEditor($contactOrganizationType);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('contactOrganizationType');

        $page->addItem($editor);

        return $page;
    }
    
    /**
     * @param array $contactOrganizationType
     * @return boolean
     */
    public function save($contactOrganizationType = null)
    {
        $App = $this->App();
        
        $data = $contactOrganizationType;
        $contactOrganizationTypeSet = $App->ContactOrganizationTypeSet();
        
        if (isset($data['id'])) {
            $contactOrganizationType = $contactOrganizationTypeSet->request($data['id']);
        } else {
            $contactOrganizationType = $contactOrganizationTypeSet->newRecord();
        }
        
        $contactOrganizationType->setFormInputValues($data);
        
        $contactOrganizationType->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-ContactOrganizationTypeController_modelView');
        
        return true;
    }
    
    
    
    public function delete($contactOrganizationType)
    {
        $App = $this->App();
        
        $set = $App->ContactOrganizationTypeSet();
        
        $contactOrganizationType = $set->request($contactOrganizationType);
        
        $set->delete($set->id->is($contactOrganizationType->id));
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-ContactOrganizationTypeController_modelView');
        
        return true;
    }

    /**
     * Does nothing and returns to the previous page.
     *
     *
     * @return \Widget_Action
     */
    public function cancel()
    {
        return true;
    }
    
    
    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $toolbar = parent::toolbar($tableView);
        $toolbar->addItem(
            $W->Link(
                $App->translate('Add type'),
                $this->proxy()->edit()
            )->addClass('icon',  \Func_Icons::ACTIONS_LIST_ADD)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        
        return $toolbar;
    }
}
