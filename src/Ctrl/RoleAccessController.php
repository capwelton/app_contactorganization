<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

$App = app_App();
$App->includeRecordController();


/**
 * This controller manages actions that can be performed on role accesses.
 *
 * @method \Func_App App()
 */
class RoleAccessController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('RoleAccess'));
    }
    
    /**
     *
     * @param string $object            The record classname (eg. 'Contact')
     * @param string $itemId
     * @return \Widget_TableView
     */
    public function listForObject($object, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $roleSet = $App->RoleSet();
        $roles = $roleSet->select();

        $allRoles = array(
            0 => $App->translate('_defaultRole')
        );
        foreach ($roles as $role) {
            $allRoles[$role->id] = $role->name();
        }

        $recordSet = $this->getRecordSet();
        $recordSet->role();

        $objectRecordSet = $App->getRecordSetByRef($object);

        $restrictableActions = $objectRecordSet->getRestrictableActions();

        $tableview = $W->TableView();
        if (isset($itemId)) {
            $tableview->setId($itemId);
        }

        $row = 0;
        $col = 0;

        $tableview->getLayout()->addHeaderSection($tableview->getId() . 'header', null, 'widget-table-header');
        $tableview->setCurrentSection($tableview->getId() . 'header');


        $tableview->addItem(
            $W->Label($App->translate('Role') . ' \ ' . $App->translate('Access right')),
            $row,
            $col
        );

        $tableview->addColumnClass($col, 'widget-20pc');

        $col++;

        foreach ($restrictableActions as $restrictableActionKey => $restrictableAction) {
            $tableview->addItem(
                $W->Label($App->translate($restrictableAction['name']))
                    ->setTitle($App->translate($restrictableAction['description'])),
                $row,
                $col
            );
            $tableview->addColumnClass($col, 'widget-10pc');
            $col++;
        }

        $tableview->getLayout()->addSection($tableview->getId() . 'body', null, 'widget-table-body');
        $tableview->setCurrentSection($tableview->getId() . 'body');

        $row++;

        foreach ($allRoles as $roleId => $roleName) {
            $col = 0;
            $tableview->getLayout()->addItem(
                $W->Label($roleName),
                $row,
                $col
            );

            $col++;

            foreach ($restrictableActions as $restrictableActionKey => $restrictableAction) {

                $criterions = $restrictableAction['conditions'];

                $select = $W->Select();
                $select->setName(array('roleAccess', $object, $roleId, $restrictableActionKey));
                $select->setAjaxAction($this->proxy()->set(), '', 'change');

                if ($roleId != 0) {
                    $record = $recordSet->get(
                        $recordSet->all(
                            $recordSet->object->is($object),
                            $recordSet->action->is($restrictableActionKey),
                            $recordSet->role->is(0)
                        )
                    );

                    if ($record) {
                        $select->addOption('', '[' . $App->translate($criterions[$record->criterion]) . ']');
                    } else {
                        $select->addOption('', '');
                    }
                }

                foreach ($criterions as $criterion => $description) {
                    $select->addOption($criterion, $App->translate($description));
                }

                $record = $recordSet->get(
                    $recordSet->all(
                        $recordSet->object->is($object),
                        $recordSet->action->is($restrictableActionKey),
                        $recordSet->role->is($roleId)
                    )
                );
                if ($record) {
                    $select->setValue($record->criterion);
                }
                $tableview->addItem($select, $row, $col);

                $col++;
            }

            $row++;
        }

        $tableview->setFixedHeader();
        $tableview->addClass('condensed');

        $tableview->addClass('depends-RoleAccess-object' . $object);
        $tableview->setReloadAction($this->proxy()->listForObject($object, $tableview->getId()));

        return $tableview;
    }


    public function edit()
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();

        $page = $Ui->Page();
        $page->setTitle($App->translate('Edit access rights'));

        $objects = [
            'Organization',
            'Contact'
        ];


        foreach ($objects as $object) {
            $objectRecordSet = $App->getRecordSetByRef($object);

            $accessSection = $W->Section(
                $App->translate($objectRecordSet->getDescription()),
                $W->DelayedItem($this->proxy()->listForObject($object))
            )->setFoldable(true, true);

            $page->addItem($accessSection);
        }

        return $page;
    }

    /**
     *
     * @param string[][][] $roleAccess
     * @return boolean
     */
    public function set($roleAccess = null)
    {
        $recordSet = $this->getRecordSet();

        foreach ($roleAccess as $object => $access) {
            foreach ($access as $roleId => $actionCriterion) {
                foreach ($actionCriterion as $action => $criterion) {
                    $recordSet->delete(
                        $recordSet->all(
                            $recordSet->object->is($object),
                            $recordSet->action->is($action),
                            $recordSet->role->is($roleId)
                        )
                    );
                    $record = $recordSet->newRecord();
                    $record->setFormInputValues(
                        array(
                            'object' => $object,
                            'action' => $action,
                            'role' => $roleId,
                            'criterion' => $criterion
                        )
                    );
                    $record->save();
                }
            }
        }

        $this->addReloadSelector('.depends-RoleAccess-object' . $object);
        return true;
    }
}
