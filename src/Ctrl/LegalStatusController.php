<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

use Capwelton\App\ContactOrganization\Set\LegalStatusSet;

$App = app_App();
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on legal statuses
 *
 * @method \Func_App    App()
 */
class LegalStatusController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('LegalStatus'));
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	LegalStatusSet	$organizationTypeSet
     * @param	array			$filter
     * @return \ORM_Criteria
     */
    protected function getFilterCriteria(LegalStatusSet $set, $filter)
    {
        // Initial conditions are base on read access rights.
        $conditions = new \ORM_TrueCriterion();

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_(
                $set->name->contains($filter['name'])
            );
        }
        if (isset($filter['code']) && !empty($filter['code'])) {
            $conditions = $conditions->_AND_($set->code->contains($filter['code']));
        }

        return $conditions;
    }
    
    protected function toolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        $W = bab_Widgets();
        $toolbar->addItem(
            $W->Link(
                $this->App()->translate('Add legal status'),
                $this->proxy()->edit()
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    /**
     * Editor legal status form
     *
     * @param int | null $legalStatus
     * @return \app_Page
     */
    public function edit($legalStatus = null, $errors = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $page = $Ui->Page();

        $set = $App->LegalStatusSet();

        $page->addClass('app-page-editor');

        if (isset($legalStatus)) {
            $page->setTitle($App->translate('Edit legal status'));
            $legalStatus = $set->request($legalStatus);
        } else {
            $page->setTitle($App->translate('Create a new legal status'));
        }
        
        $editor = $Ui->LegalStatusEditor($legalStatus);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('data');

        $page->addItem($editor);

        return $page;
    }
    
    /**
     * @param array $data
     * @return boolean
     */
    public function save($data = null)
    {
        $App = $this->App();
        
        $set = $App->LegalStatusSet();

        if (isset($data['id'])) {
            $record = $set->request($data['id']);
        } else {
            $record = $set->newRecord();
        }
        
        $record->setFormInputValues($data);

        $record->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-LegalStatusController_modelView');

        return true;
    }



    public function delete($id)
    {
        $App = $this->App();

        $set = $App->LegalStatusSet();

        $record = $set->request($id);

        $set->delete($set->id->is($record->id));
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-LegalStatusController_modelView');

        return true;
    }

    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}
