<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

use Capwelton\App\ContactOrganization\Set\OrganizationTypeSet;

$App = app_App();
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on organization types.
 *
 * @method \Func_App    App()
 */
class OrganizationTypeController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('OrganizationType'));
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	OrganizationTypeSet	$organizationTypeSet
     * @param	array			$filter
     * @return \ORM_Criteria
     */
    protected function getFilterCriteria(OrganizationTypeSet $organizationTypeSet, $filter)
    {
        // Initial conditions are base on read access rights.
        $conditions = new \ORM_TrueCriterion();

        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_(
                $organizationTypeSet->name->contains($filter['name'])
            );
        }
        if (isset($filter['description']) && !empty($filter['description'])) {
            $conditions = $conditions->_AND_($organizationTypeSet->description->contains($filter['description']));
        }

        return $conditions;
    }
    
    protected function toolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        $W = bab_Widgets();
        $toolbar->addItem(
            $W->Link(
                $this->App()->translate('Add organization type'),
                $this->proxy()->edit()
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    /**
     * Editor organization type form
     *
     * @param int | null $organizationType
     * @return \app_Page
     */
    public function edit($organizationType = null, $errors = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $page = $Ui->Page();

        $organizationTypeSet = $App->OrganizationTypeSet();

        $page->addClass('app-page-editor');

        if (isset($organizationType)) {
            $page->setTitle($App->translate('Edit organization type'));
            if (is_array($organizationType)) {

            } else if (isset($organizationType)) {
                $organizationType = $organizationTypeSet->request($organizationType);
            }
        } else {
            $page->setTitle($App->translate('Create a new organization type'));
        }
        
        $editor = $Ui->OrganizationTypeEditor($organizationType);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('organizationType');

        $page->addItem($editor);

        return $page;
    }
    
    /**
     * @param array $organizationType
     * @return boolean
     */
    public function save($organizationType = null)
    {
        $App = $this->App();

        $data = $organizationType;
        $organizationTypeSet = $App->OrganizationTypeSet();

        if (isset($data['id'])) {
            $organizationType = $organizationTypeSet->request($data['id']);
        } else {
            $organizationType = $organizationTypeSet->newRecord();
        }
        
        $organizationType->setFormInputValues($data);

        $organizationType->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-OrganizationTypeController_modelView');

        return true;
    }



    public function delete($organizationType)
    {
        $App = $this->App();

        $set = $App->OrganizationTypeSet();

        $organizationType = $set->request($organizationType);

        $set->delete($set->id->is($organizationType->id));
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-OrganizationTypeController_modelView');

        return true;
    }

    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}
