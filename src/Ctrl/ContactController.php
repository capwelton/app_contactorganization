<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\Ctrl;

use Capwelton\App\ContactOrganization\Set\Contact;
use Capwelton\App\ContactOrganization\Set\ContactSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Contact');

/**
 * This controller manages actions that can be performed on contacts.
 */
class ContactController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Contact'));
    }
    
    /**
     *
     * @return string[]
     */
    public function getAvailableDisplayFields()
    {
        $App = $this->App();
        $availableFields = array(
            'fullname' => array(
                'name' => 'fullname',
                'description' => 'Name'
            ),
            'roles' => array(
                'name' => 'roles',
                'description' => 'Roles'
            ),
            'organizationsAndPositions' => array(
                'name' => 'organizationsAndPositions',
                'description' => 'Organizations and positions'
            ),
            'photo' => array(
                'name' => 'photo',
                'description' => 'Photo'
            ),
            'acquaintances' => array(
                'name' => 'acquaintances',
                'description' => 'Who knows this contact'
            ),
            'contactPhones' => array(
                'name' => 'contactPhones',
                'description' => 'Personal phone numbers'
            )
        );
        
        if(bab_isUserAdministrator()){
            $availableFields['userAccount'] = array(
                'name' => 'userAccount',
                'description' => 'User account'
            );
        }
        
        if($attachmentC = $App->getComponentByName('Attachment')){
            $availableFields['attachments'] = array(
                'name' => 'attachments',
                'description' => $attachmentC->translate('Attached files')
            );
        }
        if($tagC = $App->getComponentByName('Tag')){
            $availableFields['tags'] = array(
                'name' => 'tags',
                'description' => $tagC->translate('Tags')
            );
        }
        if($noteC = $App->getComponentByName('Note')){
            $availableFields['notes'] = array(
                'name' => 'notes',
                'description' => $noteC->translate('Notes')
            );
        }
        if($teamC = $App->getComponentByName('Team')){
            $availableFields['teams'] = array(
                'name' => 'teams',
                'description' => $teamC->translate('Teams')
            );
        }
        if($taskC = $App->getComponentByName('TASK')){
            $availableFields['pendingTasks'] = array(
                'name' => 'pendingTasks',
                'description' => $taskC->translate('Pending tasks')
            );
            $availableFields['completedTasks'] = array(
                'name' => 'completedTasks',
                'description' => $taskC->translate('Completed tasks')
            );
        }
        
        $availableFields = array_merge($availableFields, parent::getAvailableDisplayFields());
        
        return $availableFields;
    }
    
    /**
     * @param Contact $record
     * @return array
     */
    protected function getActions(Contact $record)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $ctrl = $this->proxy();
        $actions = array();
        $moreActions = array();
        $documentsActions = array();
        
        if($currentContact = $App->ContactSet()->getCurrentContact())
        {
            $action = $W->Link(
                $W->Label($App->translate('Send VCard')),
                $this->proxy()->sendVcard($record->id)
            )->setOpenMode(\Widget_Link::OPEN_DIALOG);
            $action->setTitle($App->translate('Send VCard'));
            $action->setIcon(\Func_Icons::OBJECTS_CONTACT);
            $actions['vcard'] = $action;
        }
        
        if ($record->isDeletable()) {
            $moreActions['delete'] =  $W->Link(
                $App->translate('Delete'),
                $ctrl->confirmDelete($record->id)
            )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG);
        }
        
        
        if (bab_isUserAdministrator()) {
            $customContainerCtrl = $App->Controller()->CustomContainer();
            $views = $record->getViews();
            $className = $record->getClassName();
            
            foreach ($views as $view) {
                $action = $customContainerCtrl->editContainers($className, $view);
                $action->setTitle(sprintf($App->translate('Edit view layout %s'), $view));
                $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
                $moreActions['editSections_' . $view] = $action;
            }
            
            $action = $customContainerCtrl->importContainersConfirm($className, $view);
            $action->setTitle($App->translate('Import view layout'));
            $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
            $moreActions['importSectionsConfirm'] = $action;
        }
        
        
        $actions['documents'] = array(
            'icon' => \Func_Icons::APPS_FILE_MANAGER,
            'title' => 'Documents',
            'items' => array(
                $documentsActions
            )
        );
        $actions['more'] = array(
            'icon' => 'actions-context-menu'/*Func_Icons::ACTIONS_CONTEXT_MENU*/,
            'title' => 'More',
            'items' => array(
                $moreActions
            )
        );
        return $actions;
    }
    
    /**
     * Returns a page with the record information.
     *
     * @param int	$id
     * @return \app_Page
     */
    public function display($id, $view = '', $itemId = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->request($id);
        
        if (!$record->isReadable()) {
            throw new \app_AccessException($App->translate('You do not have access to this page'));
        }
        
        $W = bab_Widgets();
        
        $fullFrame = $Ui->ContactFullFrame($record);
        $fullFrame->setView($view);
        
        $actions = $this->getActions($record);
        
        $mainMenu = $W->Html($this->createMenu($actions));
        $mainMenu->addClass('app-record-nav', 'nav', 'navbar-nav', 'pull-right');
        $mainMenu->setSizePolicy(\Func_Icons::ICON_LEFT_24);
        
        $page = $Ui->Page();
        if (isset($itemId)) {
            $page->setId($itemId);
        }
        $page->addClass('depends-' . $this->getRecordClassName());
        
        $page->addToolbar($mainMenu);
        
        $page->addItem($fullFrame);
        
        $page->setReloadAction($this->proxy()->display($id), $page->getId());
        
        return $page;
    }
    
    public function displayListForTag($tag = null)
    {
        return ($this->displayList(array('tags' => $tag)));
    }
    
    /**
     *
     * @param int $id       The contact id
     * @throws \app_AccessException
     * @return \app_Page
     */
    public function editUserAccount($id)
    {
        if (!bab_isUserAdministrator()) {
            throw new \app_AccessException();
        }
        
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->request($id);
        
        $page = $Ui->Page();
        
        
        $editor = new \app_Editor($App, null, $W->VBoxItems()->setVerticalSpacing(1, 'em'));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem($W->Hidden()->setName('id')->setValue($record->id));
        
        if (isset($record->user) && $record->user > 0) {
            
            $page->setTitle($App->translate('Edit user account'));
            
            $editor->addItem(
                $W->LabelledWidget(
                    $App->translate('User'),
                    $W->UserPicker(),
                    'user'
                )
            );
            
        } else {
            
            $page->setTitle($App->translate('Create user account'));
            
            $editor->addItem(
                $W->LabelledWidget(
                    $App->translate('Nickname'),
                    $W->LineEdit()->setAutoComplete('new-password'),
                    'nickname'
                )
            );
            
            $editor->addItem(
                $W->LabelledWidget(
                    $App->translate('Password'),
                    $W->LineEdit()->obfuscate()->setAutoComplete('new-password'),
                    'password'
                )
            );
            
            $editor->addItem(
                $W->LabelledWidget(
                    $App->translate('Password (verification)'),
                    $W->LineEdit()->obfuscate()->setAutoComplete('new-password'),
                    'password2'
                )
            );
            
            $editor->addItem(
                $W->LabelledWidget(
                    $App->translate('Send email notification'),
                    $W->CheckBox(),
                    'notify'
                )
            );
            
            $editor->addItem(
                $W->LabelledWidget(
                    $App->translate('Force password change at the next login'),
                    $W->CheckBox(),
                    'forcePasswordChange'
                )
            );
        }
        
        $editor->setSaveAction($this->proxy()->saveUserAccount());
        
        $editor->isAjax = $this->isAjaxRequest();
        
        $page->addItem($editor);
        
        return $page;
    }    
    
    /**
     * @param array $data
     */
    public function saveUserAccount($data = null)
    {
        $App = $this->App();
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->request($data['id']);
        
        if ($data['password'] !== $data['password2']) {
            $this->addError($App->translate('The passwords do not match'));
            return true;
        }
        
        $notify = ORM_BoolField('')->input($data['notify']);
        
        $userId = $record->createUser($data['nickname'], $data['password'], $notify, true);
        
        if ($userId) {
            $record->user = $userId;
            $record->save();
            
            $forcePasswordChange = ORM_BoolField('')->input($data['forcePasswordChange']);
            if ($forcePasswordChange) {
                $error = '';
                bab_updateUserById($userId, array('force_pwd_change' => true), $error);
            }
            
            $this->addMessage($App->translate('The user account has been created'));
        }
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        
        return true;
    }
    
    /**
     *
     * @param int $id               The contact id
     * @throws \app_AccessException
     * @return \app_Page
     */
    public function editUserPassword($id)
    {
        if (!bab_isUserAdministrator()) {
            throw new \app_AccessException();
        }
        
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->request($id);
        
        if (!isset($record->user) && $record->user > 0) {
            throw new \app_AccessException();
        }
        
        $page = $Ui->Page();
        
        $page->setTitle($App->translate('Edit user password'));
        
        $editor = new \app_Editor($App, null, $W->VBoxItems()->setVerticalSpacing(1, 'em'));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem($W->Hidden()->setName('id')->setValue($record->id));
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Password'),
                $W->LineEdit()->obfuscate()->setAutoComplete('new-password'),
                'password'
            )
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Password (verification)'),
                $W->LineEdit()->obfuscate()->setAutoComplete('new-password'),
                'password2'
            )
        );
        
        $editor->setSaveAction($this->proxy()->saveUserPassword());
        
        $editor->isAjax = $this->isAjaxRequest();
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * @param array $data
     */
    public function saveUserPassword($data = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new \app_AccessException();
        }
        
        $App = $this->App();
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->request($data['id']);
        
        if ($data['password'] !== $data['password2']) {
            $this->addError($App->translate('The passwords do not match'));
            return true;
        }
        
        $record->updatePassword($data['password']);
        
        $this->addMessage($App->translate('The new password has been saved'));
        
        return true;
    }
    
    public function editUserRoles($id)
    {
        if (!bab_isUserAdministrator()) {
            throw new \app_AccessException();
        }
        
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->request($id);
        
        if (!$record->isGrantableAccess()) {
            throw new \app_AccessException();
        }
        
        $page = $Ui->Page();
        
        $page->setTitle($App->translate('Edit user roles'));
        $roleSet = $App->RoleSet();
        
        $editor = new \app_Editor($App, null, $W->VBoxItems()->setVerticalSpacing(1, 'em'));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem($W->Hidden()->setName('id')->setValue($record->id));
        
        $contactRoleSet = $App->ContactRoleSet();
        
        $contactRoles = $contactRoleSet->select($contactRoleSet->contact->is($record->id));
        $contactRoleIds = array();
        foreach ($contactRoles as $contactRole) {
            $contactRoleIds[$contactRole->role] = $contactRole->role;
        }
        
        $roles = $roleSet->select();
        foreach ($roles as $role) {
            $editor->addItem(
                $W->LabelledWidget(
                    $role->name,
                    $W->CheckBox()->setValue(isset($contactRoleIds[$role->id])),
                    array('roles', $role->id)
                )
            );
        }
        
        $editor->setSaveAction($this->proxy()->saveUserRoles());
        
        $editor->isAjax = $this->isAjaxRequest();
        
        $page->addItem($editor);
        
        return $page;
    }
    
    
    /**
     * @param array $data
     */
    public function saveUserRoles($data = null)
    {
        $App = $this->App();
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->request($data['id']);
        
        $contactRoleSet = $App->ContactRoleSet();
        
        $contactRoleSet->delete($contactRoleSet->contact->is($record->id));
        foreach ($data['roles'] as $roleId => $selected) {
            if (!$selected) {
                continue;
            }
            $contactRole = $contactRoleSet->newRecord();
            $contactRole->contact = $record->id;
            $contactRole->role = $roleId;
            $contactRole->save();
        }
        
        $this->addReloadSelector('.depends-contactRole');
        return true;
    }
    
    /**
     *
     */
    public function renameRandomly()
    {
        $App = $this->App();
        $exeption = array(39,47,40,38,31);
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $Pop = \bab_functionality::get('Populator');
        if (!($Pop instanceof \bab_functionality)) {
            return 0;
        }
        
        $contactSet = $App->ContactSet();
        $contacts = $contactSet->select(
            $contactSet->id->notIn($exeption)
            );
        $count = 0;
        foreach ($contacts as $contact){
            $this->addMessage(sprintf($App->translate('Contact %s has been renamed'), $contact->getFullName()));
            $count++;
            $contact->renameRandomly();
        }
        $this->addMessage(sprintf($App->translate('%d contact has been modified', '%d contacts have been modified', $count), $count));
        
    }
    
    /**
     * @param int   $id
     * @param int   $width
     * @param int   $height
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function photo($id, $width = 100, $height = 100, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);
        
        $photoItem = $App->Ui()->ContactAvatar($record, $width, $height, false, false);
        $photoItem->addClass('img-responsive');
        
        $box = $W->VBoxItems(
            $photoItem
        );
        
        if ($record->isUpdatable()) {
            $imagesFormItem = $W->ImagePicker();
            $imagesFormItem->oneFileMode();
            $imagesFormItem->hideFiles();
            
            $imagesFormItem->setAssociatedDropTarget($photoItem);
            
            $photoUploadPath = $record->getPhotoUploadPath();
            if (!is_dir($photoUploadPath->toString())) {
                $photoUploadPath->createDir();
            }
            $imagesFormItem->setFolder($photoUploadPath);
            $imagesFormItem->setAjaxAction($this->proxy()->updatePhoto());
            
            $box->addItem(
                $imagesFormItem
            );
            $box->addClass('crm-image-upload');
        }
        
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $box->setReloadAction($this->proxy()->photo($record->id, $width, $height, $box->getId()));
        
        return $box;
    }
    
    /**
     * @return boolean
     */
    public function updatePhoto()
    {
        return true;
    }
    
    public function postSave(Contact $contact, $data)
    {
        $contact->manageUserAccount($data);
        $contact->updateUser();
        return true;
    }
    
    
    /**
     * Displays an information page about elements linked to an organization
     * before deleting it, and propose the user to confirm or cancel
     * the organization deletion.
     *
     * @param int $id
     *
     * @return \app_Page
     */
    public function confirmDelete($id = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($recordSet->id->is($id));
        
        if (!isset($record) || $record->deleted == \app_TraceableRecord::DELETED_STATUS_DELETED) {
            throw new \app_Exception($App->translate('Trying to delete a contact with a wrong (unknown) id'));
        }
        
        $page = $App->Ui()->Page();
        $page->setMainPanelLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
        $page->addClass('crm-page-editor');
        
        $page->setTitle($App->translate('Delete contact'));
        
        $form = new \app_Editor($App);
        $form->setHiddenValue('tg', $App->controllerTg);
        
        $relatedRecords = $record->getRelatedRecords();
        
        if (count($relatedRecords) > 0) {
            
            $relatedRecordSection = $W->Section(
                $App->translate('Several elements are related to this contact'),
                $W->VBoxItems()->setVerticalSpacing(1, 'em')
            );
            $page->addItem($relatedRecordSection);
            
            $confirmedAction = $this->proxy()->delete($record->id);
            $parameters = $confirmedAction->getParameters();
            foreach ($parameters as $key => $value) {
                $form->setHiddenValue($key, $value);
            }
            $form->addButton(
                $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Delete the contact and all associated elements'))
            );
            
            $replaceAction = $this->proxy()->searchReplacement($record->id);
            $parameters = $confirmedAction->getParameters();
            foreach ($parameters as $key => $value) {
                $form->setHiddenValue($key, $value);
            }
            $form->addButton(
                $W->SubmitButton()
                ->setAjaxAction($replaceAction)
                ->setLabel($App->translate('Replace with an existing contact...'))
            );
            
        } else {
            
            $confirmedAction = $this->proxy()->delete($record->id);
            $parameters = $confirmedAction->getParameters();
            foreach ($parameters as $key => $value) {
                $form->setHiddenValue($key, $value);
            }
            
            $form->addButton(
                $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Delete permanently'))
            );
            
            $page->addItem($W->Html(bab_toHtml($App->translate('This contact is not associated with any CRM element.')))->addClass('alert'));
        }
        
        $page->addItem($form);
        
        return $page;
    }
    
    /**
     *
     * @param int $id
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function replacementMatches($id, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        $recordSet = $this->getRecordSet();
        
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction($this->proxy()->replacementMatches($id, $box->getId()));
        $box->addClass('depends-replacementKeyword');
        
        $keyword = $_SESSION['contactReplacementMatches_' . $id];
        $keywords = array();
        foreach (explode(' ', $keyword) as $k) {
            if (mb_strlen($k) > 3) {
                $keywords[] = $k;
            }
        }
        
        if (count($keywords) > 0) {
            $similarElementsSection = $W->Section(
                $App->translate('Similar contacts'),
                $W->VBoxLayout()
                ->setVerticalSpacing(1, 'em'),
                3
            );
            $box->addItem($similarElementsSection);
            
            $criteria = $recordSet->none();
            foreach ($keywords as $k) {
                $criteria = $criteria->_OR_($recordSet->lastname->contains($k));
            }
            
            $matchingRecords = $recordSet->select(
                $recordSet->id->isNot($id)
                ->_AND_($criteria)
            );
            
            foreach ($matchingRecords as $matchingRecord) {
                $similarElementsSection->addItem(
                    $W->FlowItems(
                        $Ui->ContactCardFrame($matchingRecord)
                        ->addClass('widget-30em'),
                        $W->Link(
                            $App->translate('Replace with this contact'),
                            $this->proxy()->replace($id, $matchingRecord->id)
                        )->addClass('widget-actionbutton')
                        ->setAjaxAction()
                    )
                    ->setHorizontalSpacing(2, 'em')
                    ->setVerticalAlign('middle')
                    ->setSizePolicy('widget-list-element')
                );
            }
        }
        
        return $box;
    }
    
    /**
     *
     * @param int $id
     * @param string $keyword
     * @return bool
     */
    public function setReplacementKeyword($id = null, $keyword = null)
    {
        $_SESSION['contactReplacementMatches_' . $id] = $keyword;
        
        $this->addReloadSelector('.depends-replacementKeyword');
        
        return true;
    }
    
    /**
     *
     * @param int $id
     *
     * @return \Widget_Action
     */
    public function searchReplacement($id = null, $keyword = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($recordSet->id->is($id), true);
        
        if (!isset($record) || $record->deleted == \app_TraceableRecord::DELETED_STATUS_DELETED) {
            throw new \app_Exception($App->translate('Trying to replace an contact with a wrong (unknown) id'));
        }
        
        if (!isset($keyword)) {
            $keyword = $record->lastname();
        }
        
        $this->setReplacementKeyword($record->id, $keyword);
        
        $page = $Ui->Page();
        
        $page->setTitle($App->translate('Search replacement for contact'));
        
        $page->addItem(
            $W->LabelledWidget(
                $App->translate('Contact to replace'),
                $Ui->ContactCardFrame($record)
            )
        );
        
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout());
        $editor->colon();
        
        $editor->addItem($editor->labelledField($App->translate('Keywords'), $W->LineEdit()->setSize(80), 'keyword'));
        $editor->setSaveAction($this->proxy()->setReplacementKeyword(), $App->translate('Search'));
        $editor->setReadOnly();
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('id', $record->id);
        $editor->setValue('keyword', $keyword);
        $editor->isAjax = true;
        $page->addItem($editor);
        
        $page->addItem(
            $this->replacementMatches($record->id)
        );
        
        return $page;
    }
    
    /**
     *
     * @param int $oldId
     * @param int $newId
     * @throws \app_Exception
     * @return boolean
     */
    public function replace($oldId, $newId)
    {
        $App = $this->App();
        
        $recordSet = $this->getRecordSet();
        $oldRecord = $recordSet->request($oldId);
        $newRecord = $recordSet->request($newId);
        
        if (!$oldRecord->isUpdatable() || !$newRecord->isUpdatable()) {
            throw new \app_Exception($App->translate('You do not have access to this action'));
        }
        
        $oldRecord->replaceWith($newRecord->id);
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     *
     * @param array	$message
     * @return bool
     */
    public function cancel($message = null)
    {
        if(isset($message)){
            $this->addMessage($message);
        }
        return true;
    }
    
    /**
     * @param string $search
     *
     * @return void
     */
    public function suggestContact($q = null, $relatedOrganizations = null, $excludedContacts = array())
    {
        $App = $this->App();
        /* @var $set ContactSet */
        $set = $App->ContactSet();
        
        bab_Widgets()->includePhpClass('Widget_Select2');
        $collection = new \Widget_Select2OptionsCollection();
        
        $criteria = array();
        
        if(!empty($excludedContacts)){
            $criteria[] = $set->id->notIn($excludedContacts);
        }
        
        $contacts = array();
        if(!empty($q)){
            $subCriteria = array();
            $subCriteria[] = $set->firstname->contains($q);
            $subCriteria[] = $set->lastname->contains($q);
            
            $criteria[] = $set->any($subCriteria);
            
            $contacts = $set->select($set->all($criteria));
        }
        else{
            $collection->createOption(
                0,
                bab_nbsp()
            );
        }
        session_write_close();
        
        foreach ($contacts as $contact){
            $orgName = null;
            $mainOrg = $contact->getMainContactOrganization();
            if($mainOrg){
                $mainOrg = $mainOrg->getMainOrganization();
                if($mainOrg){
                    $orgName = $mainOrg->getName();
                }
            }
            $collection->createOption(
                $contact->id,
                $contact->getFullName(),
                $orgName
            );
        }
        
        if(!empty($q)){
            $collection->createOption(
                'new_'.urldecode($q),
                $q,
                '',
                $App->translate('Create new contact')
            );
        }
       
        
        header('Content-Type: application/json');
        die($collection->output());
    }
    
    /**
     * Download a vCard
     * @param int $contactId The contact id to get the vcard from
     * @return void
     */
    public function sendVcard($contactId = null)
    {
        $App = $this->App();
        $contactSet = $App->ContactSet();
        
        $record = $contactSet->request($contactId);
        
        $mail = \bab_mail();
        
        if(!$mail){
            $this->addToast($App->translate('The mail functionnality has not been instanciated'), '', 'error');
            return true;
        }
        
        $emailTitle = $record->getFullName();
        
        $emailMessage = sprintf($App->translate('Please, find attached the vcard of %s.'), $record->getFullName());
        
        $mail->mailSubject($emailTitle);
        
        $htmlMessage = $mail->mailTemplate(\bab_toHtml($emailMessage, \BAB_HTML_BR));
        
        $mail->mailBody($htmlMessage, 'html');
        $mail->mailAltBody($emailMessage);
        $mail->clearAllRecipients();
        
        $vCard = $record->vCard()->toString();
        
        $recipient = $contactSet->getCurrentContact();
        $mainEmail = '';
        if($recipient){
            $mainEmail = $recipient->getMainEmail();
        }
        
        if (!$recipient || empty($mainEmail)) {
            $this->addToast($App->translate('No email address has been found on your contact sheet'), '', 'warn');
            return true;
        }
        
        $mail->mailTo($recipient->getMainEmail());
        $filename = \bab_removeDiacritics(\bab_convertStringFromDatabase($record->lastname.'_'.$record->firstname, 'ISO-8859-15'));
        $filename = str_replace(' ', '_', $filename);
        $filename .= '.vcf';
        
        $mail->mailStringAttach($vCard, $filename, 'text/x-vCard');
        
        /* @var $Spooler \Func_Mailspooler */
        if ($Spooler) {
            $Spooler->save($mail);
        }
        $mail->send();
        
        $this->addToast(sprintf($App->translate('The vcard of %s has been sent to your email address.'), $record->getFullName()));
        
        return true;
    }
}