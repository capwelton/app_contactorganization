<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\ContactOrganization\ComponentDefinition;

/**
 * RecordSets
 */
use Capwelton\App\ContactOrganization\Set\ContactAcquaintanceLevelSet;
use Capwelton\App\ContactOrganization\Set\ContactSet;
use Capwelton\App\ContactOrganization\Set\ContactAcquaintanceSet;
use Capwelton\App\ContactOrganization\Set\ContactOrganizationSet;
use Capwelton\App\ContactOrganization\Set\ContactOrganizationTypeSet;
use Capwelton\App\ContactOrganization\Set\ContactPhoneSet;
use Capwelton\App\ContactOrganization\Set\ContactRoleSet;
use Capwelton\App\ContactOrganization\Set\OrganizationSet;
use Capwelton\App\ContactOrganization\Set\OrganizationStatusSet;
use Capwelton\App\ContactOrganization\Set\OrganizationTypeSet;
use Capwelton\App\ContactOrganization\Set\OrganizationTypeOrganizationSet;
use Capwelton\App\ContactOrganization\Set\RoleSet;
use Capwelton\App\ContactOrganization\Set\RoleAccessSet;

/**
 * Controllers
 */
use Capwelton\App\ContactOrganization\Ctrl\ContactAcquaintanceLevelController;
use Capwelton\App\ContactOrganization\Ctrl\ContactController;
use Capwelton\App\ContactOrganization\Ctrl\ContactOrganizationController;
use Capwelton\App\ContactOrganization\Ctrl\ContactOrganizationTypeController;
use Capwelton\App\ContactOrganization\Ctrl\ContactPhoneController;
use Capwelton\App\ContactOrganization\Ctrl\OrganizationController;
use Capwelton\App\ContactOrganization\Ctrl\OrganizationTypeController;
use Capwelton\App\ContactOrganization\Ctrl\RoleAccessController;
use Capwelton\App\ContactOrganization\Ctrl\RoleController;

/**
 * Uis
 */
use Capwelton\App\ContactOrganization\Ui\ContactUi;
use Capwelton\App\ContactOrganization\Ui\ContactOrganizationUi;
use Capwelton\App\ContactOrganization\Ui\ContactOrganizationTypeUi;
use Capwelton\App\ContactOrganization\Ui\ContactPhoneUi;
use Capwelton\App\ContactOrganization\Ui\OrganizationUi;
use Capwelton\App\ContactOrganization\Ui\OrganizationTypeUi;
use Capwelton\App\ContactOrganization\Ui\RoleAccessUi;
use Capwelton\App\ContactOrganization\Ui\RoleUi;
use Capwelton\App\ContactOrganization\Set\ContactAcquaintance;
use Capwelton\App\ContactOrganization\Ui\ContactAcquaintanceLevelUi;
use Capwelton\App\ContactOrganization\Ctrl\ContactAcquaintanceController;
use Capwelton\App\ContactOrganization\Ui\ContactAcquaintanceUi;
use Capwelton\App\ContactOrganization\Set\LegalStatusSet;
use Capwelton\App\ContactOrganization\Ctrl\LegalStatusController;
use Capwelton\App\ContactOrganization\Ui\LegalStatusUi;

class ComponentDefinition implements \app_ComponentDefinition
{   
    public function getDefinition()
    {
        return 'Manages contacts, organizations and roles';
    }
    
    public function getComponents(\Func_App $App)
    {
        $contactComponent = $App->createComponent(ContactSet::class, ContactController::class, ContactUi::class);
        $contactAcquaintanceLevelComponent = $App->createComponent(ContactAcquaintanceLevelSet::class, ContactAcquaintanceLevelController::class, ContactAcquaintanceLevelUi::class);
        $contactAcquaintanceComponent = $App->createComponent(ContactAcquaintanceSet::class, ContactAcquaintanceController::class, ContactAcquaintanceUi::class);
        $contactOrganizationComponent = $App->createComponent(ContactOrganizationSet::class, ContactOrganizationController::class, ContactOrganizationUi::class);
        $contactOrganizationTypeComponent = $App->createComponent(ContactOrganizationTypeSet::class, ContactOrganizationTypeController::class, ContactOrganizationTypeUi::class);
        $contactPhoneComponent = $App->createComponent(ContactPhoneSet::class, ContactPhoneController::class, ContactPhoneUi::class);
        $contactRoleComponent = $App->createComponent(ContactRoleSet::class, null, null);
        $organizationComponent = $App->createComponent(OrganizationSet::class, OrganizationController::class, OrganizationUi::class);
        $organizationStatusComponent = $App->createComponent(OrganizationStatusSet::class, null, null);
        $organizationTypeComponent = $App->createComponent(OrganizationTypeSet::class, OrganizationTypeController::class, OrganizationTypeUi::class);
        $organizationTypeOrganizationComponent = $App->createComponent(OrganizationTypeOrganizationSet::class, null, null);
        $roleAccessComponent = $App->createComponent(RoleAccessSet::class, RoleAccessController::class, RoleAccessUi::class);
        $roleComponent = $App->createComponent(RoleSet::class, RoleController::class, RoleUi::class);
        $legalStatusComponent = $App->createComponent(LegalStatusSet::class, LegalStatusController::class, LegalStatusUi::class);
        
        return array(
            'CONTACT' => $contactComponent,
            'CONTACTACQUAINTANCELEVEL' => $contactAcquaintanceLevelComponent,
            'CONTACTACQUAINTANCE' => $contactAcquaintanceComponent,
            'CONTACTORGANIZATION' => $contactOrganizationComponent,
            'CONTACTORGANIZATIONTYPE' => $contactOrganizationTypeComponent,
            'CONTACTPHONE' => $contactPhoneComponent,
            'CONTACTROLE' => $contactRoleComponent,
            'ORGANIZATION' => $organizationComponent,
            'ORGANIZATIONSTATUS' => $organizationStatusComponent,
            'ORGANIZATIONTYPE' => $organizationTypeComponent,
            'ORGANIZATIONTYPEORGANIZATION' => $organizationTypeOrganizationComponent,
            'ROLEACCESS' => $roleAccessComponent,
            'ROLE' => $roleComponent,
            'LEGALSTATUS' => $legalStatusComponent
        );
    }

    public function getLangPath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/appcontactorganization/src/langfiles/';
    }
    
    public function getStylePath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/appcontactorganization/src/styles/';
    }
    
    public function getScriptPath(\Func_App $App)
    {
        return null;
    }
    
    public function getConfiguration(\Func_App $App)
    {
        $W = bab_Widgets();
        $component = $App->getComponentByName('Organization');
        return array(
            array(
                'sectionName' => 'Main',
                'sectionContent' => array(
                    $W->Link(
                        $W->Icon($component->translate('Organization types'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->OrganizationType()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Legal statuses'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->LegalStatus()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Contact types in organizations'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->ContactOrganizationType()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Contact acquaintance levels'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->ContactAcquaintanceLevel()->displayList()
                    )
                )
            )
        );
    }
}