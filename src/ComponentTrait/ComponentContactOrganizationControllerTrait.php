<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\ComponentTrait;

use Capwelton\App\ContactOrganization\Ctrl\ContactController;
use Capwelton\App\ContactOrganization\Ctrl\ContactOrganizationController;
use Capwelton\App\ContactOrganization\Ctrl\ContactOrganizationTypeController;
use Capwelton\App\ContactOrganization\Ctrl\ContactPhoneController;
use Capwelton\App\ContactOrganization\Ctrl\OrganizationController;
use Capwelton\App\ContactOrganization\Ctrl\OrganizationTypeController;


/**
 * @method ContactController                    Contact($proxy)
 * @method ContactOrganizationController        ContactOrganization($proxy)
 * @method ContactOrganizationTypeController    ContactOrganizationType($proxy)
 * @method ContactPhoneController               ContactPhone($proxy)
 * @method OrganizationController               Organization($proxy)
 * @method OrganizationTypeController           OrganizationType($proxy)
 */
trait ComponentContactOrganizationControllerTrait
{
    
}