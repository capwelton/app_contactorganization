<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\ContactOrganization\ComponentTrait;

use Capwelton\App\ContactOrganization\Set\ContactSet;
use Capwelton\App\ContactOrganization\Set\ContactAcquaintanceSet;
use Capwelton\App\ContactOrganization\Set\ContactOrganizationSet;
use Capwelton\App\ContactOrganization\Set\ContactOrganizationTypeSet;
use Capwelton\App\ContactOrganization\Set\ContactPhoneSet;
use Capwelton\App\ContactOrganization\Set\ContactRoleSet;
use Capwelton\App\ContactOrganization\Set\OrganizationSet;
use Capwelton\App\ContactOrganization\Set\OrganizationStatusSet;
use Capwelton\App\ContactOrganization\Set\OrganizationTypeOrganizationSet;
use Capwelton\App\ContactOrganization\Set\OrganizationTypeSet;
use Capwelton\App\ContactOrganization\Set\RoleAccessSet;
use Capwelton\App\ContactOrganization\Set\RoleSet;


/**
 * @method ContactAcquaintanceSet           ContactAcquaintanceSet()
 * @method ContactOrganizationSet           ContactOrganizationSet();
 * @method ContactOrganizationTypeSet       ContactOrganizationTypeSet()
 * @method ContactPhoneSet                  ContactPhoneSet()
 * @method ContactRoleSet                   ContactRoleSet()
 * @method ContactSet                       ContactSet()
 * @method OrganizationSet                  OrganizationSet()
 * @method OrganizationStatusSet            OrganizationStatusSet();
 * @method OrganizationTypeOrganizationSet  OrganizationTypeOrganizationSet()
 * @method OrganizationTypeSet              OrganizationTypeSet()
 * @method RoleAccessSet                    RoleAccessSet()
 * @method RoleSet                          RoleSet()
 */
trait ComponentContactOrganizationAppTrait
{
    
}